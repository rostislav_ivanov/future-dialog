﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Globalization;
using System.IO;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Runtime.Serialization.Json;
using System.Threading.Tasks;
using Windows.ApplicationModel.Background;
using Windows.Devices.Geolocation;
using Windows.Security.Cryptography;
using Windows.Security.Cryptography.Core;
using Windows.Storage;
using Windows.Storage.Streams;
using Windows.System.Profile;
using Windows.UI.Notifications;
using Windows.System.UserProfile;
using Windows.Devices.Geolocation.Geofencing;

namespace TheBoard.BackgroundAgent
{
    public sealed class SendNewLocation : IBackgroundTask
    {
        //public static string ServerUrl = "https://stage-app.futuredialog.fi/";
        private static string ServerUrl = "https://app.futuredialog.fi/";
        private static string ApiDomain = ServerUrl + "api/v2/";
        private const string UrlUpdateCustomerData = "customer";
        private HttpClient _httpClient;

        async void IBackgroundTask.Run(IBackgroundTaskInstance taskInstance)
        {
            try
            {

                var deferral = taskInstance.GetDeferral(); //ToDo
                _httpClient = new HttpClient() { BaseAddress = new Uri(ApiDomain) };
                var headers = _httpClient.DefaultRequestHeaders;

                //DEV Espoo:             yOAt7IS3zSWj6HYI5vi4dan1Hwz5PcYH
                //LIVE Espoo:            d0GjMPg7Bo6nyqPREM7bSyo-G_cPdHl2
                //headers.Add("x-board-appid", "dc3s9kLXYWRCFOVOeZyXSNiwYow2P9Y-");
                //headers.Add("x-board-appid", "aQpF_IGaIoMkPf9sXQr4BInEFp_lqXJ6");  //Holola
                //headers.Add("x-board-appid", "rIP5TnHnGvRr2yl7Q4QE0OCtXoj0TmKA");  //Holola Release
                //headers.Add("x-board-appid", "Sw1iQb8AJrv7oh0JxAZ4tdHcvYWGYu13");  //Tamperere3

                //Medialab:              df9lFPDzz6MD448oBnfb4wS8YEKV5ZeJ

                //DEV Yleisurheilu SUL:  yf2fQ5xBnOf7vUOt1tdMdVB3LD_nPciR
                //LIVE Yleisurheilu SUL: dc3s9kLXYWRCFOVOeZyXSNiwYow2P9Y-

                //DEV Lahti(Porukka):    bL7PmoBrmdmJUTmOgX4RTq3e8VjEcZrR
                //LIVE Lahti(Porukka):   17adc2fb962b468417eee74ef8c49e5a

                //DEV Tampere:           qbcbyjJ4FmgAoDKowGw1cQCMK6cmofqZ
                //LIVE Tampere:          rNn03Kf4SR2GU5A9Kn1l7rHevoXrfJqI

                //DEV Laurea:            sfJZs3k5O6tUM3Sw0jkmNfU2E7iyvGx-
                //LIVE Laurea:           BryvRRIXugPP6QGWaoNoOObBl7YhNnL9

                //DEV HalooKouvola:      2xR7QYUO3MhDu2ghxHKuV1fX_U4cTfpP
                //LIVE HalooKouvola:     g_v3M2DutNa542k4bkCGRfx6EMIs7kbo

                //DEV Puro Joensuu:      7FM0eBrgPFDiLjuXvESXD78YePV-9B00
                //LIVE Puro Joensuu:     FRxsv02_GJwI7WfxXSnpnQ_P6k73Wdy0

                //DEV Tredu:             J_glW_pLOPGGMtnvDyIx7EnM6sRcwP-6
                //LIVE Tredu:            excHYSUARq7YUIqlPizd_9JpyeYn5xQ6

                //DEV Rinki:             BYKDJnOHgOR2V_rt2-OBvyqQ6WP3Inop
                //LIVE Rinki:            ft82c7uuAjHzTT3Y4G72RZcqapRGhd4n

                //DEV #Tampere3:         t0e6JfH1n9-8mpA0cp-6Vltkv3y_VMoS
                //LIVE #Tampere3:        XmeqdxYepxrfCxJlxN-mnmCX5xq8BiG_

                //DEV Kaiku Liminka:     jyP2dDimQaG9mFgpu_AYaNSm89wP3y14
                //LIVE Kaiku Liminka:    UY1p2QIPsMmHNUtIn_G1qnGc8Y8ei7dd

                //DEV MunJäke:           RYUfaYWPuf8yGweoYrRGfr_xHV0bkRbR
                //LIVE MunJäke:          bYD_K1t5yw5iRUOK1f2iVW4sCriK6cYR

                //Current application
                headers.Add("x-board-appid", "bYD_K1t5yw5iRUOK1f2iVW4sCriK6cYR");

                IReadOnlyList<GeofenceStateChangeReport> reports = null;

                reports = GeofenceMonitor.Current.ReadReports();

                if (reports.Count == 0) return;

                var location = reports[reports.Count - 1].Geoposition.Coordinate.Point;

                await GetCustomerDataAsync(location.Position.Latitude.ToString(), location.Position.Longitude.ToString());

            }
            catch (Exception e)
            {
                if (Debugger.IsAttached) Debugger.Break();
                return;
            }
        }

        public KeyValuePair<string, string>? LoadAccessTokenForLogin()
        {
            var name = ApplicationData.Current.LocalSettings.Values["accessTokenName"];
            var value = ApplicationData.Current.LocalSettings.Values["accessTokenValue"];

            if (name == null || value == null) return null;

            return new KeyValuePair<string, string>((string)name, (string)value);
        }

        private async Task<ResponseCustomer> GetCustomerDataAsync(string latitude, string longitude)
        {
            KeyValuePair<string, string>? accessTokenPair = LoadAccessTokenForLogin();

            if (accessTokenPair == null) return null;

            Func<HttpContent> getContentFunc = () => new FormUrlEncodedContent(new[] {
                (KeyValuePair<string, string>)accessTokenPair,
                new KeyValuePair<string, string>("latitude", latitude),
                new KeyValuePair<string, string>("longitude", longitude),
            });

            var result = await SendRequestAsync(UrlUpdateCustomerData,
                HttpMethod.Post, getContentFunc);

            return result;
        }

        private async Task<ResponseCustomer> SendRequestAsync(string url, HttpMethod httpMethod,
            Func<HttpContent> getContentFunc = null)
        {
            ResponseCustomer result;
            var attempt = 1;

            while (true)
            {
                try
                {
                    var requestMessage = new HttpRequestMessage(httpMethod,
                        string.Format("{0}{1}", ApiDomain, url));

                    if (getContentFunc != null)
                    {
                        var httpContent = getContentFunc();
                        if (httpContent != null) requestMessage.Content = httpContent;
                    }

                    var responseMessage = await _httpClient.SendAsync(requestMessage,
                        HttpCompletionOption.ResponseContentRead);

                    var serializer = new DataContractJsonSerializer(typeof(ResponseCustomer));
                    using (var responseStream = await responseMessage.Content.ReadAsStreamAsync())
                    {
                        if (responseStream.Length == 0) responseMessage.EnsureSuccessStatusCode();
                        result = (ResponseCustomer)serializer.ReadObject(responseStream);
                    }

                    break;
                }
                catch (Exception ex)
                {
                    if (Debugger.IsAttached) Debugger.Break();
                    result = new ResponseCustomer { Error = ex.ToString(), IsSuccess = false };
                    break;
                } 
            }

            return result;
        }
    }
}
