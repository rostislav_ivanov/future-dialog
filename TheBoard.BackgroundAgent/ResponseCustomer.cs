﻿using System.Runtime.Serialization;

namespace TheBoard.BackgroundAgent
{
    [DataContract]
    public sealed class ResponseCustomer
    {
        [DataMember(Name = "data")] public CustomerData Data { get; set; }
        [DataMember(Name = "success")] public bool IsSuccess { get; set; }
        [DataMember(Name = "error")] public string Error { get; set; }
    }
}
