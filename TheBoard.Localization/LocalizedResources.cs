﻿using Windows.ApplicationModel.Resources;

namespace TheBoard.Localization
{
    public static class LocalizedResources
    {
        private static ResourceLoader _resourceLoader;
        private static ResourceLoader _xUidResourceLoader;

        public static string GetString(string resourceName)
        {
            if (resourceName == "settings_location_description_" || resourceName == "first_info_card_header_" ||
                resourceName == "first_info_card_text_" || resourceName == "settings_notification_description_")
            {
                //Current application
                ResourceLoader _resourceLoaderurrentApp = ResourceLoader.GetForViewIndependentUse("TheBoard.Localization/ResourcesMunJäke");

                return _resourceLoaderurrentApp.GetString(resourceName);
            }

            if (_resourceLoader == null)
                _resourceLoader = ResourceLoader.GetForViewIndependentUse("TheBoard.Localization/Resources");
            return _resourceLoader.GetString(resourceName);
        }

        public static string GetLocalString(string resourceName)
        {
            if (_xUidResourceLoader == null)
                _xUidResourceLoader = ResourceLoader.GetForViewIndependentUse("Resources");
            return _xUidResourceLoader.GetString(resourceName);
        }
    }
}
