﻿using MetroLab.Common;
using MetroLab.SampleApp.ViewModel;
using MetroLab.SampleApp.ViewModel.Pages;
using Windows.UI.Xaml.Controls;

namespace MetroLab.SampleApp
{
    public sealed partial class SectionPage
    {
        public SectionPage()
        {
            InitializeComponent();
        }

        /// <summary>
        /// Invoked when an item is clicked.
        /// </summary>
        /// <param name="sender">The GridView displaying the item clicked.</param>
        /// <param name="e">Event data that describes the item clicked.</param>
        private void ItemView_ItemClick(object sender, ItemClickEventArgs e)
        {
            var itemId = ((SampleDataItem)e.ClickedItem).UniqueId;
            var nextViewModel = new ItemPageViewModel {ItemId = itemId};
            AppViewModel.Current.NavigateToViewModel(nextViewModel);
        }
    }
}