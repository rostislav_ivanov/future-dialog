﻿using System;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Navigation;
using MetroLab.Common;
using MetroLab.SampleApp.ViewModel;
using MetroLab.SampleApp.ViewModel.Pages;


namespace MetroLab.SampleApp
{
    /// <summary>
    /// A page that displays a grouped collection of items.
    /// </summary>
    public sealed partial class HubPage
    {
        public HubPage()
        {
            InitializeComponent();
        }
        
        /// <summary>
        /// Invoked when a HubSection header is clicked.
        /// </summary>
        /// <param name="sender">The Hub that contains the HubSection whose header was clicked.</param>
        /// <param name="e">Event data that describes how the click was initiated.</param>
        void Hub_SectionHeaderClick(object sender, HubSectionHeaderClickEventArgs e)
        {
            HubSection section = e.Section;
            var group = section.DataContext;
            var nextViewModel = new SectionPageViewModel { SectionId = ((SampleDataGroup)group).UniqueId };
            AppViewModel.Current.NavigateToViewModel(nextViewModel);
        }

        /// <summary>
        /// Invoked when an item within a section is clicked.
        /// </summary>
        /// <param name="sender">The GridView or ListView
        /// displaying the item clicked.</param>
        /// <param name="e">Event data that describes the item clicked.</param>
        void ItemView_ItemClick(object sender, ItemClickEventArgs e)
        {
            var nextViewModel = new ItemPageViewModel{ItemId = ((SampleDataItem)e.ClickedItem).UniqueId};
            AppViewModel.Current.NavigateToViewModel(nextViewModel);
        }
    }
}
