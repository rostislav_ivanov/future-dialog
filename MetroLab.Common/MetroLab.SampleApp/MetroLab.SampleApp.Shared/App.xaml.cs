﻿using MetroLab.Common;
using MetroLab.SampleApp.ViewModel;
using MetroLab.SampleApp.ViewModel.Pages;
using System;
using System.Diagnostics;
using System.Runtime.Serialization;
using System.Threading.Tasks;
using Windows.ApplicationModel;
using Windows.ApplicationModel.Activation;
using Windows.UI.Core;
using Windows.UI.Popups;
using Windows.UI.Xaml;

namespace MetroLab.SampleApp.SampleApp
{
    /// <summary>
    /// Provides application-specific behavior to supplement the default Application class.
    /// </summary>
    sealed partial class App
    {
        private static readonly Type HomePageViewModelType = typeof(HubPageViewModel);
        private static readonly Type StartViewModelType = typeof(HubPageViewModel);    

        private MvvmFrame _mvvmFrame;

        [DataContract]
        private class AppStateSnapshot
        {
            [DataMember]
            public IPageViewModel[] PageViewModels { get; set; }
        }

        private class DataContractViewModelSerializer : IPageViewModelStackSerializer
        {
            private readonly DataContractSerializerSettings _settings;

            public DataContractViewModelSerializer()
            {
                var settings = new DataContractSerializerSettings
                {
                    KnownTypes = new[]
                    {
                        typeof (BindableBase), typeof (BaseViewModel), typeof (DataLoadViewModel),
                        typeof (SimplePageViewModel), typeof (ContentCacheDemoPageViewModel),
                        typeof (HubPageViewModel), typeof (ItemPageViewModel), typeof (SectionPageViewModel)},
                    SerializeReadOnlyTypes = false,
                };
                _settings = settings;
            }

            public async Task SerializeAsync(IPageViewModel[] pageViewModels, System.IO.Stream streamForWrite)
            {
                var appStateSnapshot = new AppStateSnapshot
                {
                    PageViewModels = pageViewModels
                };
                var serializer = new DataContractSerializer(typeof(AppStateSnapshot), _settings);
                serializer.WriteObject(streamForWrite, appStateSnapshot);
                await streamForWrite.FlushAsync();
            }

            public async Task<IPageViewModel[]> DeserializeAsync(System.IO.Stream streamForRead)
            {
                var serializer = new DataContractSerializer(typeof(AppStateSnapshot), _settings);
                var appStateSnapshot = (AppStateSnapshot)serializer.ReadObject(streamForRead);
                return appStateSnapshot.PageViewModels;
            }
        }

        public App()
        {
            this.InitializeComponent();
            UnhandledException += CurrentUnhandledExceptionAsync;
            Suspending += OnSuspendingAsync;

#if WINDOWS_PHONE_APP
            Windows.Phone.UI.Input.HardwareButtons.BackPressed += (s, e) =>
            {
                AppViewModel.Current.GoBack();
                e.Handled = true;
            };
#endif
        }

        public Action OnNewLogEntry { get; set; }

        private static MvvmFrame CreateMainFrame()
        {
            var result = new MvvmFrame(new DataContractViewModelSerializer()) { LoadDataAtBackgroundThread = true };
#if WINDOWS_APP
            result.AddSupportedPageViewModel(typeof(ContentCacheDemoPageViewModel), typeof(ContentCacheDemoPage));
#endif
            result.AddSupportedPageViewModel(typeof(HubPageViewModel), typeof(HubPage));
            result.AddSupportedPageViewModel(typeof(ItemPageViewModel), typeof(ItemPage));
            result.AddSupportedPageViewModel(typeof(SectionPageViewModel), typeof(SectionPage));
            return result;
        }

        /// <summary>
        /// Invoked when the application is launched normally by the end user.  Other entry points
        /// will be used when the application is launched to open a specific file, to display
        /// search results, and so forth.
        /// </summary>
        /// <param name="args">Details about the launch request and process.</param>
        protected override void OnLaunched(LaunchActivatedEventArgs args)
        {
            if (args.PreviousExecutionState != ApplicationExecutionState.Running)
            {
                bool wasTerminated = (args.PreviousExecutionState == ApplicationExecutionState.Terminated);

                var extendedSplash = new ExtendedSplash(args.SplashScreen);
                Window.Current.Content = extendedSplash;

                args.SplashScreen.Dismissed += (sender, o) =>
                    extendedSplash.Dispatcher.RunAsync(CoreDispatcherPriority.Low, async () =>
                    {
                        if (!wasTerminated)
                        {
                            await ContentCache.Current.RemoveOutdatedFilesAsync(DateTime.Now - TimeSpan.FromDays(30));
                            await ContentCacheTemp.Current.ClearCachedFiles();
                        }

                        await ContentCache.Current.Initialize();
                        await ContentCacheTemp.Current.Initialize();

                        await InitializeTask;
                        OnLaunchedInnerAsync(args);
                    });
            }
            else
            {
                OnLaunchedInnerAsync(args);
            }
        }

        protected override void OnActivated(IActivatedEventArgs args)
        {
            base.OnActivated(args);
            if (args.Kind == ActivationKind.Protocol)
            {
                OnLaunchedInnerAsync(args);
            }
        }

        private async void OnLaunchedInnerAsync(IActivatedEventArgs args)
        {
            Window.Current.Content = _mvvmFrame;
            Window.Current.Activate();

            AppViewModel.Current.HomePageViewModelType = HomePageViewModelType;// Set startup Page

            Func<bool> navigateToPinnedPage = () =>
            {
                try
                {
                    var launchActivatedEventArgs = args as LaunchActivatedEventArgs;
                    if (launchActivatedEventArgs != null && !String.IsNullOrEmpty(launchActivatedEventArgs.Arguments))
                    {
                        LinksAgent.Current.OpenLink(launchActivatedEventArgs.Arguments);
                        return true;
                    }

                    var protocolArgs = args as ProtocolActivatedEventArgs;
                    if (protocolArgs != null && protocolArgs.Uri != null)
                    {
                        LinksAgent.Current.OpenLink(protocolArgs.Uri.OriginalString);
                        return true;
                    }

                    return false;
                }
                catch (Exception)
                {
                    if (Debugger.IsAttached) Debugger.Break();
                    return false;
                }
            };

            if ((args.PreviousExecutionState == ApplicationExecutionState.Terminated))
            {
                try
                {
                    await _mvvmFrame.LoadFromStorageAsync(true); //restore
                    navigateToPinnedPage();
                }
                catch (Exception e)
                {
                    //errors in deserializing
#if DEBUG
                    if (Debugger.IsAttached) Debugger.Break();
#endif

                    AppViewModel.Current.NavigateToViewModel((IPageViewModel)Activator.CreateInstance(StartViewModelType));
                }
            }
            else if (args.PreviousExecutionState == ApplicationExecutionState.Running)
            {
                if (!navigateToPinnedPage()) Window.Current.Activate();
            }
            else
            {
                if (!navigateToPinnedPage())
                    AppViewModel.Current.NavigateToViewModel((IPageViewModel)Activator.CreateInstance(StartViewModelType));
            }
        }

        private Task _initializeTask;

        private Task InitializeTask
        {
            get { return _initializeTask ?? (_initializeTask = InitializeAsync()); }
        }

        private async Task InitializeAsync()
        {
            try
            {
                if (!Resources.ContainsKey("AppViewModel"))
                    Resources.Add("AppViewModel", AppViewModel.Current);
                
                //ToDo:
                //await DebugMenuPageViewModel.InitCurrentLog();
                _mvvmFrame = CreateMainFrame();
                AppViewModel.Current.MvvmFrame = _mvvmFrame;

                //ToDo:
                //BackgroundAgentsHelper.RegisterUpdateApplicationTileAgent();

                await ContentCache.Current.Initialize();
            }
            catch (Exception)
            {
                if (Debugger.IsAttached) Debugger.Break();
                throw;
            }
        }

        private void CurrentUnhandledExceptionAsync(object sender, UnhandledExceptionEventArgs e)
        {
            e.Handled = false;
#if DEBUG
            if (Debugger.IsAttached) Debugger.Break();
            ShowErrorMessage(e.Exception != null ? e.Exception.StackTrace : String.Empty, e.Message);
#else 
            ShowErrorMessage(CommonLocalizedResources.GetLocalizedString("msg_GeneralUnhandledException"),
                CommonLocalizedResources.GetLocalizedString("txt_Error"));
#endif
        }

        private async void ShowErrorMessage(string content, string title)
        {
            var md = new MessageDialog(content ?? string.Empty, title ?? string.Empty);
            md.Commands.Add(new UICommand("Ok"));
            await md.ShowAsync();
            Current.Exit();
        }
        
        /// <summary>
        /// Invoked when application execution is being suspended.  Application state is saved
        /// without knowing whether the application will be terminated or resumed with the contents
        /// of memory still intact.
        /// </summary>
        /// <param name="sender">The source of the suspend request.</param>
        /// <param name="e">Details about the suspend request.</param>
        private async void OnSuspendingAsync(object sender, SuspendingEventArgs e)
        {
            var deferral = e.SuspendingOperation.GetDeferral();
            await AppViewModel.Current.MvvmFrame.SaveToStorageAsync();
            //ToDo:
            //await DebugMenuPageViewModel.SaveDebugMenuLog();
            deferral.Complete();
        }
    }
}
