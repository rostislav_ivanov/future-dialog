﻿using System;
using Windows.Graphics.Display;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Navigation;
using MetroLab.Common;
using MetroLab.SampleApp.ViewModel;
using MetroLab.SampleApp.ViewModel.Pages;

namespace MetroLab.SampleApp
{
    public sealed partial class HubPage
    {
        public HubPage()
        {
            this.InitializeComponent();

            // Hub is only supported in Portrait orientation
            DisplayInformation.AutoRotationPreferences = DisplayOrientations.Portrait;

            this.NavigationCacheMode = NavigationCacheMode.Required;
        }

        /// <summary>
        /// Shows the details of a clicked group in the <see cref="SectionPage"/>.
        /// </summary>
        /// <param name="sender">The source of the click event.</param>
        /// <param name="e">Details about the click event.</param>
        private void GroupSection_ItemClick(object sender, ItemClickEventArgs e)
        {
            var groupId = ((SampleDataGroup)e.ClickedItem).UniqueId;
            var nextViewModel = new SectionPageViewModel{SectionId = groupId};
            AppViewModel.Current.NavigateToViewModel(nextViewModel);
        }

        /// <summary>
        /// Shows the details of an item clicked on in the <see cref="ItemPage"/>
        /// </summary>
        /// <param name="sender">The source of the click event.</param>
        /// <param name="e">Defaults about the click event.</param>
        private void ItemView_ItemClick(object sender, ItemClickEventArgs e)
        {
            var itemId = ((SampleDataItem)e.ClickedItem).UniqueId;
            var nextViewModel = new ItemPageViewModel { ItemId = itemId };
            AppViewModel.Current.NavigateToViewModel(nextViewModel);
        }
    }
}