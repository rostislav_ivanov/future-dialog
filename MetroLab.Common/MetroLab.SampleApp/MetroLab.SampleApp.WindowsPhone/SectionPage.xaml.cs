﻿using System;
using Windows.ApplicationModel.Resources;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Navigation;
using MetroLab.Common;
using MetroLab.SampleApp.ViewModel;
using MetroLab.SampleApp.ViewModel.Pages;

namespace MetroLab.SampleApp
{
    public sealed partial class SectionPage
    {
        public SectionPage()
        {
            InitializeComponent();
        }

        /// <summary>
        /// Shows the details of an item clicked on in the <see cref="ItemPage"/>
        /// </summary>
        /// <param name="sender">The GridView displaying the item clicked.</param>
        /// <param name="e">Event data that describes the item clicked.</param>
        private void ItemView_ItemClick(object sender, ItemClickEventArgs e)
        {
            var itemId = ((SampleDataItem)e.ClickedItem).UniqueId;
            var nextViewModel = new ItemPageViewModel {ItemId = itemId};
            AppViewModel.Current.NavigateToViewModel(nextViewModel);
        }
    }
}