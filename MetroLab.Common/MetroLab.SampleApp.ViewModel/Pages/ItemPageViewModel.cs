﻿using System.Collections.Generic;
using System.Threading.Tasks;
using MetroLab.Common;

namespace MetroLab.SampleApp.ViewModel.Pages
{
    public class ItemPageViewModel : SimplePageViewModel
    {
        public string ItemId { get; set; }

        private SampleDataItem _item;

        public SampleDataItem Item
        {
            get { return _item; }
            set { SetProperty(ref _item, value); }
        }

        private List<string> _onlinePhotos;

        public List<string> OnlinePhotos
        {
            get { return _onlinePhotos; }
            set { SetProperty(ref _onlinePhotos, value); }
        }
        
        public override async Task InitializeAsync()
        {
            Item = await SampleDataSource.GetItemAsync(ItemId);
            OnlinePhotos = new List<string>
            {
                "http://www.hdwallpapersfullhd.net/wp-content/uploads/2014/05/Beautiful-Natural-Scene-Nature-Wallpapers-610x343.jpg",
                "http://www.hdwallpapersfullhd.net/wp-content/uploads/2014/05/Nature-Wallpaper-610x343.jpg",
                "http://www.hdwallpapersfullhd.net/wp-content/uploads/2014/05/Wide-Nature-Wallpaper-610x343.jpg",
                "http://www.hdwallpapersfullhd.net/wp-content/uploads/2014/05/Pictures-of-Beautiful-Nature-Wallpapers-610x343.jpg",
                "http://www.hdwallpapersfullhd.net/wp-content/uploads/2014/05/Nature-Wallpapers-Beautiful-Sunset-610x343.jpg",
                "http://www.hdwallpapersfullhd.net/wp-content/uploads/2014/05/Photography-Nature-Wallpaper-610x343.jpg"
            };
        }
    }
}
