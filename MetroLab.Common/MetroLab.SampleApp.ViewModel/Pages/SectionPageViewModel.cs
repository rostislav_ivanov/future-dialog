﻿using System.Collections.ObjectModel;
using System.Threading.Tasks;
using MetroLab.Common;

namespace MetroLab.SampleApp.ViewModel.Pages
{
    public class SectionPageViewModel : SimplePageViewModel
    {
        public string SectionId { get; set; }


        private SampleDataGroup _group;

        public SampleDataGroup Group
        {
            get { return _group; }
            set { SetProperty(ref _group, value); }
        }

        private ObservableCollection<SampleDataItem> _items;

        public ObservableCollection<SampleDataItem> Items
        {
            get { return _items; }
            set { SetProperty(ref _items, value); }
        }

        public override async Task InitializeAsync()
        {
            var group = await SampleDataSource.GetGroupAsync(SectionId);
            Group = group;
            Items = group.Items;
        }
    }
}
