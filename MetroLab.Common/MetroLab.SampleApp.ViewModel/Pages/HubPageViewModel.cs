﻿using System.Collections;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Threading.Tasks;
using MetroLab.Common;

namespace MetroLab.SampleApp.ViewModel.Pages
{
    public class HubPageViewModel : SimplePageViewModel
    {
        private SampleDataGroup _section3Items;

        public SampleDataGroup Section3Items
        {
            get { return _section3Items; }
            set { SetProperty(ref _section3Items, value); }
        }
        
        private static SampleDataSource _sampleDataSource = new SampleDataSource();

        private IEnumerable<SampleDataGroup> _groups;
        public IEnumerable<SampleDataGroup> Groups
        {
            get { return _groups; }
            set { SetProperty(ref _groups, value); }
        }

        public override async Task InitializeAsync()
        {
            Groups = await SampleDataSource.GetGroupsAsync();
            var sampleDataGroup = await SampleDataSource.GetGroupAsync("Group-4");
            Section3Items = sampleDataGroup;
        }
    }
}
