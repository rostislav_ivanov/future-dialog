﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MetroLab.Common;

namespace MetroLab.SampleApp.ViewModel.Pages
{
    public class ContentCacheDemoPageViewModel : SimplePageViewModel
    {
        private List<string> _onlinePhotos;

        public List<string> OnlinePhotos
        {
            get { return _onlinePhotos; }
            set { SetProperty(ref _onlinePhotos, value); }
        }
        
        private List<string> _embeddedPhotos;

        public List<string> EmbeddedPhotos
        {
            get { return _embeddedPhotos; }
            set { SetProperty(ref _embeddedPhotos, value); }
        }

        public override async Task InitializeAsync()
        {
            OnlinePhotos = new List<string>
            {
                "http://www.hdwallpapersfullhd.net/wp-content/uploads/2014/05/Beautiful-Natural-Scene-Nature-Wallpapers-610x343.jpg",
                "http://www.hdwallpapersfullhd.net/wp-content/uploads/2014/05/Nature-Wallpaper-610x343.jpg",
                "http://www.hdwallpapersfullhd.net/wp-content/uploads/2014/05/Wide-Nature-Wallpaper-610x343.jpg",
                "http://www.hdwallpapersfullhd.net/wp-content/uploads/2014/05/Pictures-of-Beautiful-Nature-Wallpapers-610x343.jpg",
                "http://www.hdwallpapersfullhd.net/wp-content/uploads/2014/05/Nature-Wallpapers-Beautiful-Sunset-610x343.jpg",
                "http://www.hdwallpapersfullhd.net/wp-content/uploads/2014/05/Photography-Nature-Wallpaper-610x343.jpg"
            };

            EmbeddedPhotos = new List<string>
            {
                "Pictures\\001-610x343.jpg",
                "Pictures\\002-610x343.jpg",
                "Pictures\\003-610x343.jpg",
                "Pictures\\004-610x343.jpg",
                "Pictures\\005-610x343.jpg",
                "Pictures\\006-610x343.jpg",
                "Pictures\\007-610x343.jpg",
                "Pictures\\008-610x343.jpg"
            };
        }
    }
}
