﻿using System;
using System.Collections.Generic;

namespace MetroLab.Common
{
    public class Cache<T>
    {
        private const int MaxPagesInCacheCount = 3;

        public int Limit { get; private set; }

        public Cache()
        {
            Limit = MaxPagesInCacheCount;
        }

        public Cache(int limit)
        {
            if(limit < 0) throw new ArgumentOutOfRangeException("limit");
            Limit = limit;
        }

        private List<T> _items = new List<T>();

        public void Remove(T item)
        {
            _items.Remove(item);
        }

        public void Add(T item)
        {
            _items.Remove(item);
            if (Limit > 0)
                _items.Add(item);
            if (_items.Count > Limit)
                _items.RemoveAt(0);
        }

        public void Clear()
        {
            _items.Clear();
        }
    }
}
