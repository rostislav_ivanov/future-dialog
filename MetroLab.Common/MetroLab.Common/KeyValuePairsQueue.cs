﻿using System;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using Windows.System.Threading;

namespace MetroLab.Common
{
    public delegate Task ProcessItemHandler<TKey, TValue>(object sender, ProcessItemEventArgs<TKey, TValue> args);

    public class ProcessItemEventArgs<TKey, TValue>
    {
        public TKey Key { get; private set; }
        public TValue Value { get; private set; }

        internal ProcessItemEventArgs(TKey key, TValue value)
        {
            Key = key;
            Value = value;
        }
    }
    
    public class KeyValuePairsQueue<TKey, TValue>
    {
        public event ProcessItemHandler<TKey, TValue> ProcessItem;

        private async Task OnProcessItem(ProcessItemEventArgs<TKey, TValue> args)
        {
            ProcessItemHandler<TKey, TValue> handler = ProcessItem;
            if (handler != null) await handler(this, args);
        }

        public void StartWorker(int threadCount = 1)
        {
            if (threadCount < 1)
                throw new ArgumentOutOfRangeException("threadCount", "threadCount must be greater than 0");
            for (int i = 0; i < threadCount; i++)
                ThreadPool.RunAsync(operation => Worker());
        }

        private readonly object _locker = new object(); 
        private readonly object _pulsar = new object(); 
        
        private readonly Dictionary<TKey, TValue> _values = new Dictionary<TKey, TValue>();
        private Dictionary<TKey, TValue> Values
        {
            get { return _values; }
        }

        public int Count
        {
            get
            {
                lock (_locker)
                    return _queue.Count;
            }
        }

        private bool ContainsKey(TKey key)
        {
            lock(_locker)
            {
                return Values.ContainsKey(key);
            }
        }

        public TValue this[TKey key]
        {
            get
            {
                lock (_locker)
                    return Values[key];
            }
        }

        private readonly List<TKey> _queue = new List<TKey>();
        private List<TKey> Queue
        {
            get { return _queue; }
        }

        /// <summary>
        /// Add the object to the queue
        /// </summary>
        /// <param name="key"></param>
        /// <param name="value"></param>
        public void Enqueue(TKey key, TValue value)
        {
            lock(_locker)
            {
                if(Values.ContainsKey(key))
                {
                    Values[key] = value;
                }
                else
                {
                    Queue.Add(key);
                    Values.Add(key, value);
                    lock(_pulsar)
                        Monitor.PulseAll(_pulsar);
                }
            }
        }

        public KeyValuePair<TKey, TValue> Dequeue()
        {
            lock (_locker)
            {
                var key = Queue[0];
                Queue.RemoveAt(0);
                var value = Values[key];
                Values.Remove(key);
                return new KeyValuePair<TKey, TValue>(key, value);
            }
        }

        /// <summary>
        /// Remove object from queue
        /// </summary>
        /// <param name="key"></param>
        /// <returns></returns>
        public bool Remove(TKey key)
        {
            lock (_locker)
            {
                if (Values.ContainsKey(key))
                {
                    Values.Remove(key);
                    Queue.Remove(key);
                    return true;
                }
            }
            return false;
        }

        private async void Worker()
        {
            while (true)
            {
                bool isAnyToProcess = false;
                TKey key = default(TKey);
                TValue value = default(TValue);
                lock (_locker)
                {
                    if (Queue.Count > 0)
                    {
                        isAnyToProcess = true;
                        key = Queue[0];
                        Queue.RemoveAt(0);
                        value = Values[key];
                        Values.Remove(key);
                    }
                }

                if (isAnyToProcess)
                {
                    await OnProcessItem(new ProcessItemEventArgs<TKey, TValue>(key, value));
                    continue;
                }

                lock (_pulsar)
                    Monitor.Wait(_pulsar, 1000);
            }
        }
    }
}
