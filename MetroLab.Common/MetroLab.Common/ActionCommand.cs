﻿using System;
using System.Windows.Input;

namespace MetroLab.Common
{
    public class ActionCommand : BindableBase, ICommand
    {
        private bool _isCanExecute = true;
        public bool IsCanExecute
        {
            get { return _isCanExecute; }
            set
            {
                if (_isCanExecute == value) return;
                SetProperty(ref _isCanExecute, value);                
                if (CanExecuteChanged != null)
                    CanExecuteChanged(this, null);
            }
        }

        public event Action<object> ExecuteAction;

        public bool CanExecute(object parameter) { return IsCanExecute; }

        public event EventHandler CanExecuteChanged;
        public void Execute(object parameter)
        {
            if (ExecuteAction != null)
            {
                ExecuteAction(parameter);
            }
        }
    }
}
