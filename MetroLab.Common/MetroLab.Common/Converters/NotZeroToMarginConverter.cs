﻿using System;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Data;

namespace MetroLab.Common.Converters
{
  public  class NotZeroToMarginConverter:IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, string language)
        {
            return ((int)value)>0.0?new Thickness(0,0,0,20):new Thickness(0,0,0,0);
        }

        public object ConvertBack(object value, Type targetType, object parameter, string language)
        {
            throw new NotSupportedException();
        }
    }
}
