namespace MetroLab.Common
{
    public interface IServerSubstitution
    {
        string TransformUrl(string sourceUrl);
    }
}