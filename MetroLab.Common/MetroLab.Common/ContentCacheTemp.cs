﻿using System;
using System.Diagnostics;
using System.Threading.Tasks;
using Windows.Storage;
using Windows.UI.Xaml;

namespace MetroLab.Common
{
    public class ContentCacheTemp : ContentCache
    {
        protected override string BinaryCacheFolderName { get { return "BinaryCacheTemp"; } }
        protected override int ThreadCount { get { return 1; } }

        private static volatile ContentCacheTemp _current;
        public static new ContentCacheTemp Current
        {
            get { return _current ?? (_current = new ContentCacheTemp()); }
        }

        public static string GetSourceUrlTemp(DependencyObject frameworkControl) { return (string)frameworkControl.GetValue(SourceUrlTempProperty); }
        public static void SetSourceUrlTemp(DependencyObject frameworkControl, string value) { frameworkControl.SetValue(SourceUrlTempProperty, value); }

        public static readonly DependencyProperty SourceUrlTempProperty = DependencyProperty.RegisterAttached(
            "SourceUrlTemp", typeof(string), typeof(ContentCache), new PropertyMetadata(null, Current.OnSourceUriPropertyChanged));
        
        
        private ContentCacheTemp() { }

        public async Task ClearCachedFiles()
        {
            try
            {
                await ApplicationData.Current.LocalFolder.CreateFolderAsync(
                    BinaryCacheFolderName, CreationCollisionOption.ReplaceExisting);
            }
            catch (Exception e)
            {
                if (Debugger.IsAttached)
                    Debugger.Break();
            }
        }
    }
}
