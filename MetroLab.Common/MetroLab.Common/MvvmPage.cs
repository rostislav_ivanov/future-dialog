﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Threading.Tasks;
using Windows.System;
using Windows.System.Threading;
using Windows.UI.Core;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Media.Animation;
using Windows.UI.Xaml.Navigation;
using MetroLab.Common.Transitions;

namespace MetroLab.Common
{
    [Windows.Foundation.Metadata.WebHostHidden]
    public class MvvmPage : Page, ILoadedOrUpdatedSuccessfullyListener
    {
        private List<Control> _layoutAwareControls;
        
        public bool LoadDataAtBackgroundThread { get; internal set; }
        
        public static readonly DependencyProperty NavigationTransitionProperty = DependencyProperty.Register("NavigationTransition", typeof(MetroLabNavigationTransitionInfo), typeof(MvvmPage), null);

        public MetroLabNavigationTransitionInfo NavigationTransition
        {
            get { return (MetroLabNavigationTransitionInfo)GetValue(NavigationTransitionProperty); }
            set { SetValue(NavigationTransitionProperty, value); }
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="MvvmPage"/> class.
        /// </summary>
        protected MvvmPage()
        {
            if (Windows.ApplicationModel.DesignMode.DesignModeEnabled) return;
            NavigationCacheMode = NavigationCacheMode.Enabled;
            Transitions = null;
            // When this page is part of the visual tree make two changes:
            // 1) Map application view state to visual state for the page
            // 2) Handle keyboard and mouse navigation requests
            Loaded += OnLoaded;

            // Undo the same changes when the page is no longer visible
            Unloaded += OnUnloaded;
        }

#if DEBUG
        ~MvvmPage()
        {
            Debug.WriteLine("{0} destructed", GetType().Name);
        }
#endif

        private void OnLoaded(object sender, RoutedEventArgs args)
        {
            StartLayoutUpdates(sender, args);

            var currentWindow = Window.Current;
            var bounds = currentWindow.Bounds;
            // Keyboard and mouse navigation only apply when occupying the entire window
            if (ActualHeight == bounds.Height && ActualWidth == bounds.Width)
            {
                var coreWindow = currentWindow.CoreWindow;
                // Listen to the window directly so focus isn't required
                coreWindow.Dispatcher.AcceleratorKeyActivated += CoreDispatcherAcceleratorKeyActivated;
                coreWindow.PointerPressed += CoreWindowPointerPressed;
            }
        }

        private void OnUnloaded(object sender, RoutedEventArgs args)
        {
            StopLayoutUpdates(sender, args);
            var coreWindow = Window.Current.CoreWindow;
            coreWindow.Dispatcher.AcceleratorKeyActivated -= CoreDispatcherAcceleratorKeyActivated;
            coreWindow.PointerPressed -= CoreWindowPointerPressed;
        }

        private IPageViewModel _viewModel;
        public IPageViewModel ViewModel
        {
            get { return _viewModel; }
            set
            {
                if (!Equals(_viewModel, value))
                {
                    var dataLoadViewModel = _viewModel as IDataLoadViewModel;
                    if (dataLoadViewModel != null)
                        dataLoadViewModel.RemoveLoadedOrUpdatedSuccessfullyListener(this);
                    
                    _viewModel = value;
                    
                    dataLoadViewModel = value as IDataLoadViewModel;
                    if (dataLoadViewModel != null)
                        dataLoadViewModel.AddLoadedOrUpdatedSuccessfullyListener(this);
                    
                    DataContext = value;
                }
            }
        }

        public void OnLoadedOrUpdatedSuccessfully(object sender, EventArgs args)
        {
            OnViewModelLoadedOrUpdatedSuccessfully();
        }
        
        public void OnRemoved()
        {
            var viewModel = ViewModel;
            if (viewModel != null)
            {
                viewModel.UnSubscribeFromEvents();
                var dataLoadViewModel = _viewModel as IDataLoadViewModel;
                if (dataLoadViewModel != null)
                    dataLoadViewModel.RemoveLoadedOrUpdatedSuccessfullyListener(this);
            }
            
            _viewModel = null;
        }
        
        protected virtual void OnViewModelLoadedOrUpdatedSuccessfully() { }

        #region Navigation support

        /// <summary>
        /// Invoked as an event handler to navigate backward in the navigation stack
        /// associated with this page's <see cref="Frame"/>.
        /// </summary>
        /// <param name="sender">Instance that triggered the event.</param>
        /// <param name="e">Event data describing the conditions that led to the
        /// event.</param>
        protected virtual void GoBack(object sender, RoutedEventArgs e)
        {
            if (AppViewModel.Current.CanGoBack) AppViewModel.Current.GoBack();
        }

        /// <summary>
        /// Invoked as an event handler to navigate forward in the navigation stack
        /// associated with this page's <see cref="Frame"/>.
        /// </summary>
        /// <param name="sender">Instance that triggered the event.</param>
        /// <param name="e">Event data describing the conditions that led to the
        /// event.</param>
        protected virtual void GoForward(object sender, RoutedEventArgs e) { }

        /// <summary>
        /// Invoked on every keystroke, including system keys such as Alt key combinations, when
        /// this page is active and occupies the entire window.  Used to detect keyboard navigation
        /// between pages even when the page itself doesn't have focus.
        /// </summary>
        /// <param name="sender">Instance that triggered the event.</param>
        /// <param name="args">Event data describing the conditions that led to the event.</param>
        private void CoreDispatcherAcceleratorKeyActivated(CoreDispatcher sender, AcceleratorKeyEventArgs args)
        {
            var virtualKey = args.VirtualKey;

            // Only investigate further when Left, Right, or the dedicated Previous or Next keys
            // are pressed
            if ((args.EventType == CoreAcceleratorKeyEventType.SystemKeyDown ||
                args.EventType == CoreAcceleratorKeyEventType.KeyDown) &&
                (virtualKey == VirtualKey.Left || virtualKey == VirtualKey.Right ||
                (int)virtualKey == 166 || (int)virtualKey == 167))
            {
                var coreWindow = Window.Current.CoreWindow;
                var downState = CoreVirtualKeyStates.Down;
                bool menuKey = (coreWindow.GetKeyState(VirtualKey.Menu) & downState) == downState;
                bool controlKey = (coreWindow.GetKeyState(VirtualKey.Control) & downState) == downState;
                bool shiftKey = (coreWindow.GetKeyState(VirtualKey.Shift) & downState) == downState;
                bool noModifiers = !menuKey && !controlKey && !shiftKey;
                bool onlyAlt = menuKey && !controlKey && !shiftKey;

                if (((int)virtualKey == 166 && noModifiers) ||
                    (virtualKey == VirtualKey.Left && onlyAlt))
                {
                    // When the previous key or Alt+Left are pressed navigate back
                    args.Handled = true;
                    GoBack(this, new RoutedEventArgs());
                }
                else if (((int)virtualKey == 167 && noModifiers) ||
                    (virtualKey == VirtualKey.Right && onlyAlt))
                {
                    // When the next key or Alt+Right are pressed navigate forward
                    args.Handled = true;
                    GoForward(this, new RoutedEventArgs());
                }
            }
        }

        /// <summary>
        /// Invoked on every mouse click, touch screen tap, or equivalent interaction when this
        /// page is active and occupies the entire window.  Used to detect browser-style next and
        /// previous mouse button clicks to navigate between pages.
        /// </summary>
        /// <param name="sender">Instance that triggered the event.</param>
        /// <param name="args">Event data describing the conditions that led to the event.</param>
        private void CoreWindowPointerPressed(CoreWindow sender, PointerEventArgs args)
        {
            var properties = args.CurrentPoint.Properties;

            // Ignore button chords with the left, right, and middle buttons
            if (properties.IsLeftButtonPressed || properties.IsRightButtonPressed ||
                properties.IsMiddleButtonPressed) return;

            // If back or foward are pressed (but not both) navigate appropriately
            bool backPressed = properties.IsXButton1Pressed;
            bool forwardPressed = properties.IsXButton2Pressed;
            if (backPressed ^ forwardPressed)
            {
                args.Handled = true;
                if (backPressed) GoBack(this, new RoutedEventArgs());
                if (forwardPressed) GoForward(this, new RoutedEventArgs());
            }
        }

        #endregion

        #region Visual state switching

        /// <summary>
        /// Invoked as an event handler, typically on the <see cref="FrameworkElement.Loaded"/>
        /// event of a <see cref="Control"/> within the page, to indicate that the sender should
        /// start receiving visual state management changes that correspond to application view
        /// state changes.
        /// </summary>
        /// <param name="sender">Instance of <see cref="Control"/> that supports visual state
        /// management corresponding to view states.</param>
        /// <param name="e">Event data that describes how the request was made.</param>
        /// <remarks>The current view state will immediately be used to set the corresponding
        /// visual state when layout updates are requested.  A corresponding
        /// <see cref="FrameworkElement.Unloaded"/> event handler connected to
        /// <see cref="StopLayoutUpdates"/> is strongly encouraged.  Instances of
        /// <see cref="MvvmPage"/> automatically invoke these handlers in their Loaded and
        /// Unloaded events.</remarks>
        private void StartLayoutUpdates(object sender, RoutedEventArgs e)
        {
            var control = sender as Control;
            if (control == null) return;
            if (_layoutAwareControls == null)
            {
                // Start listening to view state changes when there are controls interested in updates
                Window.Current.SizeChanged += WindowSizeChanged;
                _layoutAwareControls = new List<Control>();
            }
            _layoutAwareControls.Add(control);

            // Set the initial visual state of the control
        }

        private void WindowSizeChanged(object sender, WindowSizeChangedEventArgs e)
        {
            //ToDo: InvalidateVisualState();
        }

        /// <summary>
        /// Invoked as an event handler, typically on the <see cref="FrameworkElement.Unloaded"/>
        /// event of a <see cref="Control"/>, to indicate that the sender should start receiving
        /// visual state management changes that correspond to application view state changes.
        /// </summary>
        /// <param name="sender">Instance of <see cref="Control"/> that supports visual state
        /// management corresponding to view states.</param>
        /// <param name="e">Event data that describes how the request was made.</param>
        /// <remarks>The current view state will immediately be used to set the corresponding
        /// visual state when layout updates are requested.</remarks>
        /// <seealso cref="StartLayoutUpdates"/>
        private void StopLayoutUpdates(object sender, RoutedEventArgs e)
        {
            var control = sender as Control;
            if (control == null || _layoutAwareControls == null) return;
            _layoutAwareControls.Remove(control);
            if (_layoutAwareControls.Count == 0)
            {
                // Stop listening to view state changes when no controls are interested in updates
                _layoutAwareControls = null;
                Window.Current.SizeChanged -= WindowSizeChanged;
            }
        }

        private bool _firstNavigation = true;
        
        protected virtual void OnNavigatedTo()
        {
            if (_firstNavigation)
            {
                _firstNavigation = false;
                return;
            }
            var viewModel = ViewModel;
            var dataLoadViewModel = viewModel as IDataLoadViewModel;
            if (dataLoadViewModel != null && dataLoadViewModel.UpdatingOnNavigationEnabled)
            {
                if (LoadDataAtBackgroundThread)
                    ThreadPool.RunAsync(o => dataLoadViewModel.UpdateAsync());
                else dataLoadViewModel.UpdateAsync();
            }
        }

        protected virtual bool OnNavigatingFrom(NavigationType type) { return true; }

        public void RaiseNavigatedTo()
        {
            OnNavigatedTo();
        }
        
        public bool RaiseNavigatingFrom(NavigationType type)
        {
            return OnNavigatingFrom(type);
        }
        
        #endregion
    }
}
