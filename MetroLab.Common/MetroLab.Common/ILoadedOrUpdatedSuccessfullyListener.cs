﻿using System;

namespace MetroLab.Common
{
    public interface ILoadedOrUpdatedSuccessfullyListener
    {
        void OnLoadedOrUpdatedSuccessfully(object sender, EventArgs args);
    }
}