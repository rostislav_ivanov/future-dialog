using System;

namespace MetroLab.Common
{
    public class ServerSubstitution : IServerSubstitution
    {
        private readonly string _oldServerBasePath;
        
        private readonly string _newServerBasePath;

        public ServerSubstitution(string oldServerBasePath, string newServerBasePath)
        {
            if (string.IsNullOrEmpty(oldServerBasePath))
                throw new ArgumentException("oldServerBasePath");
            if (string.IsNullOrEmpty(newServerBasePath))
                throw new ArgumentException("newServerBasePath");

            _oldServerBasePath = oldServerBasePath;
            _newServerBasePath = newServerBasePath;
        }

        public string TransformUrl(string sourceUrl)
        {
            if (sourceUrl == null) return null;
            if (sourceUrl.StartsWith(_oldServerBasePath))
                return _newServerBasePath + sourceUrl.Substring(_oldServerBasePath.Length);
            return sourceUrl;
        }
    }
}