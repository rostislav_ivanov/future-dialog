﻿using System.Runtime.Serialization;
using System.Threading.Tasks;
using Windows.ApplicationModel.DataTransfer;

namespace MetroLab.Common
{
    [DataContract]
    public class SimplePageViewModel : BaseViewModel, IPageViewModel
    {
        protected internal virtual async Task<bool> LoadAsync() { return true; }

        public virtual async Task InitializeAsync() { }

        public virtual async Task UpdateAsync() { }

        [IgnoreDataMember] public IPrintHelper PrintHelper { get; set; }

        public virtual async Task RequestData(DataTransferManager manager, DataRequestedEventArgs args) { }
        
        public virtual async Task OnSerializingToStorageAsync() { }

        public virtual async Task OnSerializedToStorageAsync() { }

        public virtual async Task OnDeserializedFromStorageAsync() { }

        public virtual async Task OnNavigatingAsync() { }

        public virtual void UnSubscribeFromEvents() { }

    }
}
