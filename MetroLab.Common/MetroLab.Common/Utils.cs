﻿using System;
using System.IO;
using System.Threading.Tasks;
using Windows.Security.Cryptography;
using Windows.Security.Cryptography.Core;
using Windows.Storage;
using Windows.Storage.Streams;

namespace MetroLab.Common
{
    public static class Utils
    {
        public static string CalculateMd5(string str)
        {
            var alg = HashAlgorithmProvider.OpenAlgorithm("MD5");
            var buff = CryptographicBuffer.ConvertStringToBinary(str, BinaryStringEncoding.Utf8);
            var hashed = alg.HashData(buff);
            return CryptographicBuffer.EncodeToHexString(hashed);
        }

        public static async Task<string> CalculateMd5Async(StorageFile file)
        {
            using (var fileStream = await file.OpenStreamForReadAsync())
            using (var inputStream = fileStream.AsInputStream())
                return await CalculateMd5Async(inputStream);
        }

        public static async Task<string> CalculateMd5Async(IInputStream inputStream)
        {
            var alg = HashAlgorithmProvider.OpenAlgorithm(HashAlgorithmNames.Md5);

            const uint capacity = 65536;
            var buffer = new Windows.Storage.Streams.Buffer(capacity);
            var hash = alg.CreateHash();

            while (true)
            {
                await inputStream.ReadAsync(buffer, capacity, InputStreamOptions.None);
                if (buffer.Length > 0)
                    hash.Append(buffer);
                else break;
            }

            return CryptographicBuffer.EncodeToHexString(hash.GetValueAndReset());
        }
    }
}
