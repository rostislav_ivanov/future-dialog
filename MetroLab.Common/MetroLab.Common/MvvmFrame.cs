﻿using System.Diagnostics;
using Windows.System.Threading;
using Windows.UI.Xaml.Media.Animation;
using MetroLab.Common.Converters;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.Serialization;
using System.Threading.Tasks;
using Windows.Storage;
using Windows.UI.Core;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Data;
using MetroLab.Common.Transitions;

namespace MetroLab.Common
{
    public interface INavigationItem : IDisposable
    {
        Page GetPage();

        WeakReference<Page> GetPageReference();

        IPageViewModel PageViewModel { get; }
    }

    public interface IPageViewModelStackSerializer
    {
        Task SerializeAsync(IPageViewModel[] pageViewModels, Stream streamForWrite);

        Task<IPageViewModel[]> DeserializeAsync(Stream streamForRead);
    }

#if WINDOWS_PHONE_APP
    public class MvvmFrame : Frame
#else
    public class MvvmFrame : ContentControl, INavigate
#endif
    {
        private const int DefaultMaxPagesInCacheCount = 5;

        private Cache<Page> _pagesCache = new Cache<Page>(DefaultMaxPagesInCacheCount);
        private Cache<Page> PagesCache
        {
            get { return _pagesCache; }
        }

        public int MaxPagesInCacheCount
        {
            get { return _pagesCache.Limit; }
            set { _pagesCache = new Cache<Page>(value); }
        }

        private bool _loadDataAtBackgroundThread = true;

        public bool LoadDataAtBackgroundThread
        {
            get { return _loadDataAtBackgroundThread; }
            set { _loadDataAtBackgroundThread = value; }
        }

        private class MvvmNavigationItem : INavigationItem
        {
            private readonly MvvmFrame _mvvmFrame;

            private WeakReference<MvvmPage> _pageReference;

            protected WeakReference<MvvmPage> PageReference
            {
                get { return _pageReference; }
            }

            private readonly IPageViewModel _pageViewModel;
            public IPageViewModel PageViewModel
            {
                get { return _pageViewModel; }
            }

            public MvvmNavigationItem(IPageViewModel pageViewModel, MvvmFrame mvvmFrame)
            {
                if (mvvmFrame == null) throw new ArgumentNullException("mvvmFrame");
                if (pageViewModel == null) throw new ArgumentNullException("pageViewModel");

                _mvvmFrame = mvvmFrame;
                _pageViewModel = pageViewModel;
            }

            protected internal void InitializePage(MvvmPage page)
            {
                page.ViewModel = _pageViewModel;
            }

            public Page GetPage()
            {
                lock (this)
                {
                    MvvmPage page;
                    if ((_pageReference != null) && _pageReference.TryGetTarget(out page))
                        return page;
                    var pageType = _mvvmFrame.GetPageType(_pageViewModel.GetType());

#if WINDOWS_PHONE_APP
                    if (!_mvvmFrame.Navigate(pageType))
                        if (Debugger.IsAttached) Debugger.Break();
                    page = (MvvmPage)_mvvmFrame.Content;
#else
                    page = (MvvmPage)Activator.CreateInstance(pageType);
#endif

                    page.LoadDataAtBackgroundThread = _mvvmFrame.LoadDataAtBackgroundThread;
                    InitializePage(page);
                    if (_pageReference == null)
                        _pageReference = new WeakReference<MvvmPage>(page);
                    else
                        _pageReference.SetTarget(page);
                    return page;
                }
            }

            public WeakReference<Page> GetPageReference()
            {
                lock (this)
                {
                    MvvmPage page;
                    if ((_pageReference != null) && _pageReference.TryGetTarget(out page) && (page != null))
                        return new WeakReference<Page>(page);
                    return null;
                }
            }

            public void UpdateView()
            {
                lock (this)
                {
                    _pageReference = null;
                }
            }

            public void Dispose()
            {
                MvvmPage page;
                if (_pageReference != null && _pageReference.TryGetTarget(out page))
                {
                    _mvvmFrame.PagesCache.Remove(page);
                    page.OnRemoved();
                }
            }

            public override string ToString()
            {
                return string.Format("MvvmNavigationItem: {0}", _pageViewModel);
            }
        }

        private class PageNavigationItem : INavigationItem
        {
            private readonly Page _page;

            public PageNavigationItem(Page page)
            {
                if (page == null) throw new ArgumentNullException("page");
                _page = page;
            }

            public Page GetPage()
            {
                return _page;
            }

            public WeakReference<Page> GetPageReference()
            {
                return new WeakReference<Page>(GetPage());
            }

            public IPageViewModel PageViewModel { get { return null; } }

            public void Dispose() { }
        }

        private readonly Stack<INavigationItem> _navigationStack = new Stack<INavigationItem>();
        private Stack<INavigationItem> NavigationStack { get { return _navigationStack; } }

        public INavigationItem CurrentNavigationItem { get { return NavigationStack.Peek(); } }

        private readonly IPageViewModelStackSerializer _serializer;

        private readonly Dictionary<Type, Type> _viewModelsAndPagesMapping = new Dictionary<Type, Type>();

        public MvvmFrame(IPageViewModelStackSerializer serializer)
        {
            if (serializer == null) throw new ArgumentNullException("serializer");

            _serializer = serializer;
            HorizontalContentAlignment = HorizontalAlignment.Stretch;
            VerticalContentAlignment = VerticalAlignment.Stretch;
        }

        public void AddSupportedPageViewModel(Type viewModelType, Type pageType)
        {
            if (viewModelType == null) throw new NullReferenceException("viewModelType");
            if (pageType == null) throw new NullReferenceException("pageType");

            try
            {
                _viewModelsAndPagesMapping.Add(viewModelType, pageType);
            }
            catch (ArgumentException e) //An element with the same key already exists in the Dictionary<TKey, TValue>.
            {
                var errorMessage = string.Format("ViewModel of type {0} alredy mapped", viewModelType.Name);
                throw new InvalidOperationException(errorMessage, e);
            }
        }

        private Type GetPageType(Type viewModelType)
        {
            Type result;
            if (_viewModelsAndPagesMapping.TryGetValue(viewModelType, out result))
                return result;
            var message = string.Format("There are no appropriate View for ViewModel {0}", viewModelType.FullName);
            throw new InvalidOperationException(message);
        }

        private void ShowItem(INavigationItem navigationItem)
        {
            if (navigationItem == null) throw new ArgumentNullException("navigationItem");
            var pageToShow = navigationItem.GetPage();
            PagesCache.Add(pageToShow);
            pageToShow.HorizontalAlignment = HorizontalAlignment.Stretch;
            pageToShow.VerticalAlignment = VerticalAlignment.Stretch;
            Content = pageToShow;
            Window.Current.Activate();
            if (pageToShow is MvvmPage)
                (pageToShow as MvvmPage).RaiseNavigatedTo();
        }

        public bool CanGoBack
        {
            get { return NavigationStack.Count > 1; }
        }

        public async Task<IPageViewModel> GoBack()
        {
            var previousPageViewModel = NavigationStack.Peek().PageViewModel;
            if (!RemoveLastEntryFromNavigationStack()) return null;

            if (NavigationStack.Count == 0)
            {
                Application.Current.Exit();
                return null;
            }

#if WINDOWS_PHONE_APP
            base.GoBack();
            var page = Content as MvvmPage;
            if (page != null && page.ViewModel == null)
                ((MvvmNavigationItem) NavigationStack.Peek()).InitializePage(page);

            return page == null ? null : page.ViewModel;
#else
            var oldPage = Content as MvvmPage;

            Func<IPageViewModel> finishNavigation = () =>
            {
                var nextNavigationItem = NavigationStack.Peek();
                var nextPage = (MvvmPage)nextNavigationItem.GetPage();
                ShowItem(nextNavigationItem);

                Debug.Assert(nextPage != null, "nextPage != null");
                var transitionInfo = nextPage.NavigationTransition;
                if (transitionInfo != null)
                    transitionInfo.RunGoInBackwardTransition(
                        new NavigationTransitionArgs
                        {
                            TargetPage = nextPage,
                            NextPageViewModel = nextNavigationItem.PageViewModel,
                            PreviousPageViewModel = previousPageViewModel
                        });
                
                nextPage.IsHitTestVisible = true;
                var pageViewModel = nextNavigationItem.PageViewModel;
                if (LoadDataAtBackgroundThread)
                    ThreadPool.RunAsync(o => pageViewModel.InitializeAsync());
                else pageViewModel.InitializeAsync();
                return pageViewModel;
            };

            if (oldPage == null || oldPage.NavigationTransition == null) 
                return finishNavigation();
            
            var task = new TaskCompletionSource<IPageViewModel>();
            oldPage.NavigationTransition.RunGoAwayBackwardTransition(new NavigationTransitionArgs{TargetPage = oldPage, OnCompletedAction = () => task.TrySetResult(finishNavigation())});
            return await task.Task;
#endif
        }

        public async Task Navigate(IPageViewModel pageViewModel)
        {
#if WINDOWS_PHONE_APP
            var currentPage = Content as MvvmPage;
            if (currentPage != null)
            {
                currentPage.RaiseNavigatingFrom(NavigationType.New);
            }

            var nextNavigationItem = new MvvmNavigationItem(pageViewModel, this);
            NavigationStack.Push(nextNavigationItem);

            base.Navigate(GetPageType(pageViewModel.GetType()));
            var page = (MvvmPage)Content;
            Debug.Assert(page != null);
            nextNavigationItem.InitializePage(page);
            page.RaiseNavigatedTo();

            if (LoadDataAtBackgroundThread)
                ThreadPool.RunAsync(o => pageViewModel.InitializeAsync());
            else pageViewModel.InitializeAsync();
#else
            TaskCompletionSource<bool> taskCompletionSource = null;
            Action finishNavigation = () =>
            {
                var nextNavigationItem = new MvvmNavigationItem(pageViewModel, this);
                NavigationStack.Push(nextNavigationItem);

                var nextPage = (MvvmPage)nextNavigationItem.GetPage();
                ShowItem(nextNavigationItem);
                Debug.Assert(nextPage != null, "nextPage != null");
                var nextTransitionInfo = nextPage.NavigationTransition;
                if (NavigationStack.Count > 1 && nextTransitionInfo != null)
                    nextTransitionInfo.RunGoInForwardTransition(new NavigationTransitionArgs {TargetPage = nextPage, NextPageViewModel = pageViewModel});
                
                if (taskCompletionSource != null) taskCompletionSource.TrySetResult(true);

                if (LoadDataAtBackgroundThread)
                    ThreadPool.RunAsync(o => pageViewModel.InitializeAsync());
                else pageViewModel.InitializeAsync();                
            };

            var currentPage = Content as MvvmPage;
            if (currentPage != null)
            {
                currentPage.IsHitTestVisible = false;
                var transitionInfo = currentPage.NavigationTransition;
                if (transitionInfo != null)
                {
                    if (!currentPage.RaiseNavigatingFrom(NavigationType.New)) return;

                    taskCompletionSource = new TaskCompletionSource<bool>();
                    transitionInfo.RunGoAwayForwardTransition(new NavigationTransitionArgs
                    {
                        TargetPage = currentPage,
                        NextPageViewModel = pageViewModel,
                        OnCompletedAction = finishNavigation
                    });
                    
                    await taskCompletionSource.Task;
                    return;
                }

                if (!currentPage.RaiseNavigatingFrom(NavigationType.New)) return;
            }

            finishNavigation();
#endif
        }
        
#if !WINDOWS_PHONE_APP
        public bool Navigate(Type sourcePageType)
        {
            lock (this)
            {
                var page = (Page)Activator.CreateInstance(sourcePageType);
                var nextNavigationItem = new PageNavigationItem(page);
                NavigationStack.Push(nextNavigationItem);
                ShowItem(nextNavigationItem);
                return true;
            }
        }
#endif

        const string SessionStateFileName = "_sessionState";

        [DataContract]
        public class AppStateSnapshot
        {
            [DataMember]
            public IPageViewModel[] PageViewModels { get; set; }
        }

        public async Task SaveToStorageAsync()
        {
            var viewModels = NavigationStack.OfType<MvvmNavigationItem>().Select(x => x.PageViewModel).ToList();
            var sessionItems = viewModels.ToArray();
            var sessionData = new MemoryStream();

            await _serializer.SerializeAsync(sessionItems, sessionData);

            // Get an output stream for the SessionState file and write the state asynchronously
            StorageFile file = await ApplicationData.Current.RoamingFolder.CreateFileAsync(SessionStateFileName, CreationCollisionOption.ReplaceExisting);
            using (Stream fileStream = await file.OpenStreamForWriteAsync())
            {
                sessionData.Seek(0, SeekOrigin.Begin);
                await sessionData.CopyToAsync(fileStream);
                await fileStream.FlushAsync();
            }
        }

        public async Task LoadFromStorageAsync(bool showImmediately = true)
        {
            // Get the input stream for the SessionState file

            var file = await ApplicationData.Current.RoamingFolder
                .CreateFileAsync(SessionStateFileName, CreationCollisionOption.OpenIfExists);
            using (var inStream = await file.OpenSequentialReadAsync())
            {
                // Deserialize the Session State
                IPageViewModel[] sessionState = await _serializer.DeserializeAsync(inStream.AsStreamForRead());

                foreach (var pageViewModel in sessionState)
                {
                    await pageViewModel.OnDeserializedFromStorageAsync();
                }

                foreach (var pageViewModel in sessionState.Reverse())
                {
                    var navigationItem = new MvvmNavigationItem(pageViewModel, this);
                    NavigationStack.Push(navigationItem);
                }
            }

            var peekNavigationItem = NavigationStack.Peek();
            if (showImmediately)
                ShowItem(peekNavigationItem);
            {
                var pageViewModel = peekNavigationItem.PageViewModel;
                if (LoadDataAtBackgroundThread)
                    ThreadPool.RunAsync(o => pageViewModel.InitializeAsync());
                else pageViewModel.InitializeAsync();
            }
        }

        public void ClearNavigationStack()
        {
            foreach (var navigationItem in NavigationStack)
                navigationItem.Dispose();
            NavigationStack.Clear();
        }

        public bool RemoveLastEntryFromNavigationStack()
        {
            if (NavigationStack.Count <= 0)
                throw new InvalidOperationException(CommonLocalizedResources.GetLocalizedString("exceptionMsg_CantGoBack"));

            var currentPage = Content as MvvmPage;
            if (currentPage != null)
            {
                if (!currentPage.RaiseNavigatingFrom(NavigationType.Back)) return false;
            }

            var pageReference = CurrentNavigationItem.GetPageReference();
            if (pageReference != null)
            {
                Page page;
                if (pageReference.TryGetTarget(out page))
                    PagesCache.Remove(page);
            }

            NavigationStack.Pop().Dispose();
            return true;
        }

        public void UpdateView()
        {
            PagesCache.Clear();
            foreach (var navigationItem in NavigationStack)
            {
                var mvvmNavigationItem = navigationItem as MvvmNavigationItem;
                if (mvvmNavigationItem != null)
                {
                    mvvmNavigationItem.PageViewModel.UpdateAsync();
                    mvvmNavigationItem.UpdateView();
                }
            }
            ShowItem(NavigationStack.Peek());
        }
    }
}
