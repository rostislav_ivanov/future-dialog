﻿using System.Threading.Tasks;
using Windows.ApplicationModel.DataTransfer;

namespace MetroLab.Common
{
    public interface IPageViewModel
    {
        Task InitializeAsync();

        Task UpdateAsync();

        Task RequestData(DataTransferManager manager, DataRequestedEventArgs args);
        
        Task OnSerializingToStorageAsync();

        Task OnSerializedToStorageAsync();

        Task OnDeserializedFromStorageAsync();

        Task OnNavigatingAsync();

        void UnSubscribeFromEvents();

        IPrintHelper PrintHelper { get; set; }
    }
}
