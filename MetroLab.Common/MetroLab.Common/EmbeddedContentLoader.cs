﻿using Windows.UI.Xaml.Media.Imaging;
using MetroLog;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Threading.Tasks;
using Windows.ApplicationModel;
using Windows.UI.Core;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Media.Animation;

namespace MetroLab.Common
{
    public class EmbeddedContentLoader : ContentLoader
    {
        private static volatile EmbeddedContentLoader _current;

        public static EmbeddedContentLoader Current
        {
            get { return _current ?? (_current = new EmbeddedContentLoader()); }
        }

        //<ui element, file path>
        private KeyValuePairsQueue<object, string> _elementsQueue = new KeyValuePairsQueue<object, string>();

        private readonly object InitializeLocker = new object();

        private volatile Task _initializeTask;
        public Task Initialize()
        {
            if (_initializeTask == null)
                lock (InitializeLocker)
                {
                    if (_initializeTask == null)
                        return (_initializeTask = InitializeInner());
                }
            return _initializeTask;
        }

        private async Task InitializeInner()
        {
            _elementsQueue = new KeyValuePairsQueue<object, string>();
            _elementsQueue.ProcessItem += ElementsQueue_ProcessItem;
            _elementsQueue.StartWorker();
        }

        public async Task<bool> SetCachedSource(object element, string fileRelativePath)
        {
            if (fileRelativePath == null)
                throw new ArgumentNullException("fileRelativePath");

            var task = new TaskCompletionSource<bool>();
            var localUri = Package.Current.InstalledLocation.Path + '\\' + fileRelativePath.Replace('/', '\\');
            Action<object> action = async target =>
            {

                try
                {
                    var helper = GetHelper(element);
                    await helper.SetSourceAsync(new Uri(localUri, UriKind.RelativeOrAbsolute));
                    task.TrySetResult(true);
                }
                catch (FileNotFoundException)
                {
                    task.TrySetResult(false);
                }
                catch (Exception e)
                {
#if DEBUG
                    if (Debugger.IsAttached)
                        Debugger.Break();
                    //press F5
#endif
                    task.TrySetResult(false);
                }
            };
            var frameworkElement = element as FrameworkElement;
            if (frameworkElement != null)
            {
                await frameworkElement.Dispatcher.RunAsync(CoreDispatcherPriority.Low, () => action(element));
            }
            else
            {
                action(element);
            }

            return await task.Task;
        }

        async Task ElementsQueue_ProcessItem(object sender, ProcessItemEventArgs<object, string> args)
        {
            try
            {
                var element = args.Key;
                var relativePath = args.Value;
                if (String.IsNullOrEmpty(relativePath)) return;

                await SetCachedSource(element, relativePath);
            }
            catch (Exception e)
            {
                if (Debugger.IsAttached)
                    Debugger.Break();
            }
        }
        
        public static string GetSourceUrl(DependencyObject frameworkControl) { return (string)frameworkControl.GetValue(SourceUrlProperty); }
        public static void SetSourceUrl(DependencyObject frameworkControl, string value) { frameworkControl.SetValue(SourceUrlProperty, value); }

        public static readonly DependencyProperty SourceUrlProperty = DependencyProperty.RegisterAttached(
            "SourceUrl", typeof(string), typeof(EmbeddedContentLoader), new PropertyMetadata(null, Current.OnSourceUriPropertyChanged));

        protected async void OnSourceUriPropertyChanged(DependencyObject o, DependencyPropertyChangedEventArgs e)
        {
            var newServerPath = (string) e.NewValue;

            var element = (FrameworkElement)o; 
            if (GetRemoveOldContentWhenLoading(element))
            {
                var elementCacheHelper = GetHelper(element);
                elementCacheHelper.RemoveContent();
            }
            element.Unloaded -= ElementOnUnloaded;
            element.Unloaded += ElementOnUnloaded;

            await RemoveAllQueuedElements();
            await RemoveElement(element);
            _elementsQueue.Enqueue(element, newServerPath);
            Log.Info("Ссылка {0} добавлена в очередь на скачивание (сработало SourceUriPropertyChanged)", newServerPath);
        }

        protected void ElementOnUnloaded(object sender, RoutedEventArgs routedEventArgs)
        {
            var element = ((FrameworkElement)sender);
            element.Loaded += ElementOnLoaded;
            element.Unloaded -= ElementOnUnloaded;
            lock (PendingUnloadedElementsQueueLocker)
                PendingUnloadedElementsQueue.Add(element);
        }

        private async void ElementOnLoaded(object sender, RoutedEventArgs routedEventArgs)
        {
            var element = ((FrameworkElement)sender);
            element.Loaded -= ElementOnLoaded;
            var helper = GetHelper(element);
            if (!(await helper.IsSourceEmptyAsync())) return;
            await RemoveAllQueuedElements();
            await RemoveElement(element);
            var sourceUrl = GetSourceUrl(element);
            _elementsQueue.Enqueue(element, sourceUrl);

            Log.Info("Ссылка {0} добавлена в очередь на скачивание (ElementOnLoaded called)", sourceUrl);
        }

        private readonly object PendingUnloadedElementsQueueLocker = new object();
        private List<object> _pendingUnloadedElementsQueue = new List<object>();
        private List<object> PendingUnloadedElementsQueue
        {
            get { return _pendingUnloadedElementsQueue; }
        }

        protected async Task RemoveAllQueuedElements()
        {
            List<object> elements;
            lock (PendingUnloadedElementsQueueLocker)
            {
                elements = PendingUnloadedElementsQueue;
                _pendingUnloadedElementsQueue = new List<object>();
            }
            if (elements == null) return;
            foreach (var element in elements)
            {
                await RemoveElement(element);
            }
        }

        private async Task RemoveElement(object element)
        {
            await Initialize();
            _elementsQueue.Remove(element);
        }
    }
}
