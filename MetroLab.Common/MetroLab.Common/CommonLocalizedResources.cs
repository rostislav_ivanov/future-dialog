﻿using Windows.ApplicationModel.Resources;

namespace MetroLab.Common
{
    public static class CommonLocalizedResources
    {
        private static ResourceLoader _resourceLoader;

        public static string GetLocalizedString(string resourceName)
        {
            if (_resourceLoader == null)
                _resourceLoader = ResourceLoader.GetForViewIndependentUse("MetroLab.Common/Resources");
            
            return _resourceLoader.GetString(resourceName);
        }
    }
}
