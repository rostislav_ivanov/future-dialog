﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Collections.Specialized;
using System.ComponentModel;
using System.Linq;

namespace MetroLab.Common
{
    public class FilteredCollection<T> : INotifyPropertyChanged
    {
        private ObservableCollection<T> _result; 
        public ObservableCollection<T> Result
        {
            get { return _result; }
            set { _result = value; OnPropertyChanged("Result"); }
        } 

        public IList<T> SourceCollection { get; private set; }
        
        private Func<T, bool> _filter;

        public Func<T, bool> Filter
        {
            get { return _filter; }
            set
            {
                _filter = value;
                UpdateFilter();
            }
        }

        public void UpdateFilter()
        {
            var nextResult = new ObservableCollection<T>(SourceCollection.Where(Filter));
            if (Result == null) Result = nextResult;

            int prevItemPosition = -1;
            foreach (var item in nextResult)
            {
                var itemPosition = Result.IndexOf(item);
                if (itemPosition < 0)
                {
                    prevItemPosition++;
                    if (prevItemPosition < Result.Count)
                        Result.Insert(prevItemPosition, item);
                    else
                        Result.Add(item);
                }
                else
                {
                    prevItemPosition++;
                    for (int i = prevItemPosition; i < itemPosition; i++)
                        Result.RemoveAt(prevItemPosition);
                }
            }

            while (Result.Count > prevItemPosition + 1 )
                Result.RemoveAt(Result.Count - 1);
        }

        public FilteredCollection(IList<T> sourceCollection)
        {
            SourceCollection = sourceCollection;
            var observableCollection = sourceCollection as INotifyCollectionChanged;
            if (observableCollection != null)
                observableCollection.CollectionChanged += SourceCollectionOnCollectionChanged;
        }

        private void SourceCollectionOnCollectionChanged(object sender, NotifyCollectionChangedEventArgs args)
        {
            UpdateFilter();
        }
        
        public event PropertyChangedEventHandler PropertyChanged;

        protected void OnPropertyChanged(string propertyName)
        {
            if (PropertyChanged != null)
                PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
        }
    }
}
