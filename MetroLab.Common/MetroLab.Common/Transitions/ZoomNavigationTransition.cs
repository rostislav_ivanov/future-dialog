﻿using System;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Media.Animation;

namespace MetroLab.Common.Transitions
{
    public class ZoomNavigationTransition : MetroLabNavigationTransitionInfo
    {
        private const double ScaleMagnitude = 0.85;

        private Storyboard RunTransition(NavigationTransitionArgs args, EasingFunctionBase easingFunction,
            double startOpacity, double endOpacity,
            double startScale, double endScale)
        {
            var targetUiElement = args.TargetPage.Content;
            targetUiElement.CacheMode = new BitmapCache();
            var windowsBounds = Window.Current.CoreWindow.Bounds;
            targetUiElement.RenderTransform = new CompositeTransform { CenterX = windowsBounds.Width / 2, CenterY = windowsBounds.Height / 2, ScaleX = startScale, ScaleY = startScale};
            targetUiElement.Opacity = startOpacity;

            var storyboard = new Storyboard { Duration = TimeSpan.FromMilliseconds(230) };
            Storyboard.SetTarget(storyboard, targetUiElement);


            var scaleXAnimation = new DoubleAnimation
            {
                To = endScale,
                Duration = new Duration(TimeSpan.FromMilliseconds(220)),
                AutoReverse = false,
                EnableDependentAnimation = true,
                FillBehavior = FillBehavior.HoldEnd,
                EasingFunction = new QuadraticEase { EasingMode = EasingMode.EaseOut }
            };
            Storyboard.SetTargetProperty(scaleXAnimation, "(UIElement.RenderTransform).(CompositeTransform.ScaleX)");
            storyboard.Children.Add(scaleXAnimation);

            var scaleYAnimation = new DoubleAnimation
            {
                To = endScale,
                Duration = new Duration(TimeSpan.FromMilliseconds(220)),
                AutoReverse = false,
                EnableDependentAnimation = true,
                FillBehavior = FillBehavior.HoldEnd,
                EasingFunction = new QuadraticEase { EasingMode = EasingMode.EaseOut }
            };
            Storyboard.SetTargetProperty(scaleYAnimation, "(UIElement.RenderTransform).(CompositeTransform.ScaleY)");
            storyboard.Children.Add(scaleYAnimation);

            var opacityAnimation = new DoubleAnimation
            {
                To = endOpacity,
                Duration = new Duration(TimeSpan.FromMilliseconds(220)),
                AutoReverse = false,
                EnableDependentAnimation = true,
                FillBehavior = FillBehavior.HoldEnd,
                EasingFunction = new QuadraticEase { EasingMode = EasingMode.EaseOut }
            };
            Storyboard.SetTargetProperty(opacityAnimation, "Opacity");
            storyboard.Children.Add(opacityAnimation);

            EventHandler<object> completed = null;
            completed = (sender, o) =>
            {
                targetUiElement.CacheMode = null;
                var typedSender = (Storyboard)sender;
                targetUiElement.Opacity = endOpacity;
                var renderTransform = targetUiElement.RenderTransform as CompositeTransform;
                if (renderTransform != null)
                {
                    renderTransform.ScaleX = endScale;
                    renderTransform.ScaleY = endScale;
                }

                if (args.OnCompletedAction != null) args.OnCompletedAction();
                typedSender.Completed -= completed;
                typedSender.Stop();
            };

            storyboard.Completed += completed;
            storyboard.Begin();
            return storyboard;
        }

        public override Storyboard RunGoInForwardTransition(NavigationTransitionArgs args)
        {
            return RunTransition(args,
                new QuadraticEase { EasingMode = EasingMode.EaseOut },
                startOpacity: 0, endOpacity: 1,
                startScale: ScaleMagnitude, endScale: 1.0);
        }

        public override Storyboard RunGoAwayBackwardTransition(NavigationTransitionArgs args)
        {
            return RunTransition(args,
                new QuadraticEase { EasingMode = EasingMode.EaseIn },
                startOpacity: 1, endOpacity: 0,
                startScale: 1.0, endScale: ScaleMagnitude);
        }

        public override Storyboard RunGoInBackwardTransition(NavigationTransitionArgs args)
        {
            return RunTransition(args,
                new QuadraticEase { EasingMode = EasingMode.EaseOut },
                startOpacity: 0, endOpacity: 1,
                startScale: ScaleMagnitude, endScale: 1.0);
        }

        public override Storyboard RunGoAwayForwardTransition(NavigationTransitionArgs args)
        {
            return RunTransition(args,
                new QuadraticEase { EasingMode = EasingMode.EaseIn },
                startOpacity: 1, endOpacity: 0,
                startScale: 1.0, endScale: ScaleMagnitude);
        }
    }
}