﻿using System;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Media.Animation;

namespace MetroLab.Common.Transitions
{
    public class SlideNavigationTransition : MetroLabNavigationTransitionInfo
    {
        private const double TranslateMagnitude = 120;

        private Storyboard RunTransition(MvvmPage page, EasingFunctionBase easingFunction,
            double startOpacity, double endOpacity, 
            double startY, double endY, 
            Action onCompleted = null)
        {
            var targetUiElement = page.Content;
            targetUiElement.RenderTransform = new TranslateTransform{Y = startY};
            targetUiElement.Opacity = startOpacity;

            var storyboard = new Storyboard { Duration = TimeSpan.FromMilliseconds(230) };
            Storyboard.SetTarget(storyboard, targetUiElement);

            var translateYAnimation = new DoubleAnimation
            {
                From = startY,
                To = endY,
                Duration = new Duration(TimeSpan.FromMilliseconds(220)),
                AutoReverse = false,
                EnableDependentAnimation = true,
                FillBehavior = FillBehavior.HoldEnd,
                EasingFunction = easingFunction
            };
            Storyboard.SetTargetProperty(translateYAnimation, "(UIElement.RenderTransform).(TranslateTransform.Y)");
            storyboard.Children.Add(translateYAnimation);

            var opacityAnimation = new DoubleAnimation
            {
                From = startOpacity,
                To = endOpacity,
                Duration = new Duration(TimeSpan.FromMilliseconds(220)),
                AutoReverse = false,
                EnableDependentAnimation = true,
                FillBehavior = FillBehavior.HoldEnd,
                EasingFunction = new QuadraticEase { EasingMode = EasingMode.EaseIn }
            };
            Storyboard.SetTargetProperty(opacityAnimation, "Opacity");
            storyboard.Children.Add(opacityAnimation);

            EventHandler<object> completed = null;
            completed = (sender, o) =>
            {
                var typedSender = (Storyboard)sender;
                targetUiElement.Opacity = endOpacity;
                var translateTransform = (TranslateTransform) targetUiElement.RenderTransform;
                translateTransform.Y = endY;

                if (onCompleted != null) onCompleted();

                typedSender.Completed -= completed;
                typedSender.Stop();
            };

            storyboard.Completed += completed;
            storyboard.Begin();
            return storyboard;
        }

        public override Storyboard RunGoInForwardTransition(NavigationTransitionArgs args)
        {
            return RunTransition(args.TargetPage,
                new QuadraticEase { EasingMode = EasingMode.EaseOut },
                startOpacity: 0, endOpacity: 1,
                startY: -TranslateMagnitude, endY: 0,
                onCompleted: args.OnCompletedAction);
        }

        public override Storyboard RunGoAwayBackwardTransition(NavigationTransitionArgs args)
        {
            return RunTransition(args.TargetPage,
                new QuadraticEase { EasingMode = EasingMode.EaseIn },
                startOpacity: 1, endOpacity: 0,
                startY: 0, endY: -TranslateMagnitude,
                onCompleted: args.OnCompletedAction);
        }

        public override Storyboard RunGoInBackwardTransition(NavigationTransitionArgs args)
        {
            return RunTransition(args.TargetPage,
                new QuadraticEase { EasingMode = EasingMode.EaseOut },
                startOpacity: 0, endOpacity: 1,
                startY: TranslateMagnitude, endY: 0,
                onCompleted: args.OnCompletedAction);
        }

        public override Storyboard RunGoAwayForwardTransition(NavigationTransitionArgs args)
        {
            return RunTransition(args.TargetPage,
                new QuadraticEase { EasingMode = EasingMode.EaseIn },
                startOpacity: 1, endOpacity: 0,
                startY: 0, endY: TranslateMagnitude,
                onCompleted: args.OnCompletedAction);
        }
    }
}