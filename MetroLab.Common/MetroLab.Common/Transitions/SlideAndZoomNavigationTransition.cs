﻿using System;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Media.Animation;

namespace MetroLab.Common.Transitions
{
    public class SlideAndZoomNavigationTransition : MetroLabNavigationTransitionInfo
    {
        public override Storyboard RunGoInForwardTransition(NavigationTransitionArgs args)
        {
            var targetUiElement = args.TargetPage.Content;
            targetUiElement.CacheMode = new BitmapCache();
            var windowsBounds = Window.Current.CoreWindow.Bounds;
            targetUiElement.RenderTransform = new CompositeTransform { CenterX = windowsBounds.Width / 2, CenterY = windowsBounds.Height / 2, ScaleX = 0.85, ScaleY = 0.85 };
            targetUiElement.Opacity = 0;

            var storyboard = new Storyboard { Duration = TimeSpan.FromMilliseconds(230) };
            Storyboard.SetTarget(storyboard, targetUiElement);
            
            var scaleXAnimation = new DoubleAnimation
            {
                To = 1,
                Duration = new Duration(TimeSpan.FromMilliseconds(220)),
                AutoReverse = false,
                EnableDependentAnimation = true,
                FillBehavior = FillBehavior.HoldEnd,
                EasingFunction = new QuadraticEase {EasingMode = EasingMode.EaseOut}
            };
            Storyboard.SetTargetProperty(scaleXAnimation, "(UIElement.RenderTransform).(CompositeTransform.ScaleX)");
            storyboard.Children.Add(scaleXAnimation);
                        
            var scaleYAnimation = new DoubleAnimation
            {
                To = 1,
                Duration = new Duration(TimeSpan.FromMilliseconds(220)),
                AutoReverse = false,
                EnableDependentAnimation = true,
                FillBehavior = FillBehavior.HoldEnd,
                EasingFunction = new QuadraticEase { EasingMode = EasingMode.EaseOut }
            };
            Storyboard.SetTargetProperty(scaleYAnimation, "(UIElement.RenderTransform).(CompositeTransform.ScaleY)");
            storyboard.Children.Add(scaleYAnimation);
            
            var opacityAnimation = new DoubleAnimation
            {
                To = 1,
                Duration = new Duration(TimeSpan.FromMilliseconds(220)),
                AutoReverse = false,
                EnableDependentAnimation = true,
                FillBehavior = FillBehavior.HoldEnd,
                EasingFunction = new QuadraticEase { EasingMode = EasingMode.EaseOut }
            };
            Storyboard.SetTargetProperty(opacityAnimation, "Opacity");
            storyboard.Children.Add(opacityAnimation);

            EventHandler<object> completed = null;
            completed = (sender, o) =>
            {
                targetUiElement.CacheMode = null;
                var typedSender = (Storyboard) sender;
                targetUiElement.Opacity = 1;
                var renderTransform = targetUiElement.RenderTransform as CompositeTransform;
                if (renderTransform != null)
                {
                    renderTransform.ScaleX = 1;
                    renderTransform.ScaleY = 1;
                }
                
                typedSender.Completed -= completed;
                typedSender.Stop();
            };
            storyboard.Completed += completed;
            storyboard.Begin();
            return storyboard;
        }

        public override Storyboard RunGoAwayBackwardTransition(NavigationTransitionArgs args)
        {
            var targetUiElement = args.TargetPage.Content;
            targetUiElement.CacheMode = new BitmapCache();
            targetUiElement.RenderTransform = new CompositeTransform();
            targetUiElement.Opacity = 1;

            var storyboard = new Storyboard { Duration = TimeSpan.FromMilliseconds(230) };
            Storyboard.SetTarget(storyboard, targetUiElement);

            var translateYAnimation = new DoubleAnimation
            {
                To = 120,
                Duration = new Duration(TimeSpan.FromMilliseconds(220)),
                AutoReverse = false,
                EnableDependentAnimation = true,
                FillBehavior = FillBehavior.HoldEnd,
                EasingFunction = new QuadraticEase { EasingMode = EasingMode.EaseIn }
            };
            Storyboard.SetTargetProperty(translateYAnimation, "(UIElement.RenderTransform).(CompositeTransform.TranslateY)");
            storyboard.Children.Add(translateYAnimation);

            var opacityAnimation = new DoubleAnimation
            {
                To = 0,
                Duration = new Duration(TimeSpan.FromMilliseconds(220)),
                AutoReverse = false,
                EnableDependentAnimation = true,
                FillBehavior = FillBehavior.HoldEnd,
                EasingFunction = new QuadraticEase { EasingMode = EasingMode.EaseIn }
            };
            Storyboard.SetTargetProperty(opacityAnimation, "Opacity");
            storyboard.Children.Add(opacityAnimation);

            EventHandler<object> completed = null;
            completed = (sender, o) =>
            {
                targetUiElement.CacheMode = null;
                var typedSender = (Storyboard) sender;
                targetUiElement.Opacity = 0;

                if (args.OnCompletedAction != null) args.OnCompletedAction();

                typedSender.Completed -= completed;
                typedSender.Stop();
            };

            storyboard.Completed += completed;
            storyboard.Begin();
            return storyboard;
        }

        public override Storyboard RunGoInBackwardTransition(NavigationTransitionArgs args)
        {
            var targetUiElement = args.TargetPage.Content;
            targetUiElement.CacheMode = new BitmapCache();
            targetUiElement.Opacity = 0;

            var storyboard = new Storyboard { Duration = TimeSpan.FromMilliseconds(230) };
            Storyboard.SetTarget(storyboard, targetUiElement);

            var opacityAnimation = new DoubleAnimation
            {
                To = 1,
                Duration = new Duration(TimeSpan.FromMilliseconds(220)),
                AutoReverse = false,
                EnableDependentAnimation = true,
                FillBehavior = FillBehavior.HoldEnd,
                EasingFunction = new QuadraticEase { EasingMode = EasingMode.EaseOut }
            };
            Storyboard.SetTargetProperty(opacityAnimation, "Opacity");
            storyboard.Children.Add(opacityAnimation);

            EventHandler<object> completed = null;
            completed = (sender, o) =>
            {
                targetUiElement.CacheMode = null;
                var typedSender = (Storyboard) sender;
                targetUiElement.Opacity = 1;

                typedSender.Completed -= completed;
                typedSender.Stop();
            };

            storyboard.Completed += completed;
            storyboard.Begin();
            return storyboard;
        }

        public override Storyboard RunGoAwayForwardTransition(NavigationTransitionArgs args)
        {
            var targetUiElement = args.TargetPage.Content;
            targetUiElement.CacheMode = new BitmapCache();
            targetUiElement.Opacity = 1;

            var storyboard = new Storyboard { Duration = TimeSpan.FromMilliseconds(230) };
            Storyboard.SetTarget(storyboard, targetUiElement);

            var opacityAnimation = new DoubleAnimation
            {
                To = 0,
                Duration = new Duration(TimeSpan.FromMilliseconds(220)),
                AutoReverse = false,
                EnableDependentAnimation = true,
                FillBehavior = FillBehavior.HoldEnd,
                EasingFunction = new QuadraticEase { EasingMode = EasingMode.EaseIn }
            };
            Storyboard.SetTargetProperty(opacityAnimation, "Opacity");
            storyboard.Children.Add(opacityAnimation);

            EventHandler<object> completed = null;
            completed = (sender, o) =>
            {
                targetUiElement.CacheMode = null;
                var typedSender = (Storyboard) sender;
                targetUiElement.Opacity = 0;

                if (args.OnCompletedAction != null) args.OnCompletedAction();

                typedSender.Completed -= completed;
                typedSender.Stop();
            };

            storyboard.Completed += completed;
            storyboard.Begin();
            return storyboard;
        }
    }
}