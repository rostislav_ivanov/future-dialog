﻿using System;

namespace MetroLab.Common.Transitions
{
    public class NavigationTransitionArgs
    {
        public IPageViewModel PreviousPageViewModel { get; set; }

        public IPageViewModel NextPageViewModel { get; set; }

        public MvvmPage TargetPage { get; set; }

        public Action OnCompletedAction { get; set; }
    }
}