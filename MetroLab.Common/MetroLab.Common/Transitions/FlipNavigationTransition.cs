﻿using System;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Media.Animation;

namespace MetroLab.Common.Transitions
{
    public class FlipNavigationTransition : MetroLabNavigationTransitionInfo
    {
        private const double PlaneMagnitude = 40;
        private const double MinScale = 0.8;

        private Storyboard RunTransition(MvvmPage page, EasingFunctionBase easingFunction, 
            double startOpacity, double endOpacity, double startRotationY, double endRotationY,
            Action onCompleted = null)
        {
            var targetUiElement = page.Content;

            var planeProjection = new PlaneProjection { CenterOfRotationX = 0, CenterOfRotationY = 0, CenterOfRotationZ = -70 };
            targetUiElement.Projection = planeProjection;            
            targetUiElement.CacheMode = new BitmapCache();
            
            planeProjection.RotationY = startRotationY;
            targetUiElement.Opacity = startOpacity;
            const int durationInMs = 140;
            var storyboard = new Storyboard { Duration = TimeSpan.FromMilliseconds(durationInMs + 10) }; 
            Storyboard.SetTarget(storyboard, targetUiElement);

            var projectAnimation = new DoubleAnimation
            {
                To = endRotationY,
                Duration = new Duration(TimeSpan.FromMilliseconds(durationInMs)),
                AutoReverse = false,
                FillBehavior = FillBehavior.HoldEnd,
                EasingFunction = easingFunction,
                EnableDependentAnimation = true
            };
            Storyboard.SetTargetProperty(projectAnimation, "(UIElement.Projection).(PlaneProjection.RotationY)");
            storyboard.Children.Add(projectAnimation);

            var opacityAnimation = new DoubleAnimation
            {
                To = endOpacity,
                Duration = new Duration(TimeSpan.FromMilliseconds(durationInMs)),
                AutoReverse = false,
                EnableDependentAnimation = true,
                FillBehavior = FillBehavior.HoldEnd,
                EasingFunction = easingFunction
            };
            Storyboard.SetTargetProperty(opacityAnimation, "Opacity");
            storyboard.Children.Add(opacityAnimation);
            
            EventHandler<object> completed = null;
            completed = (sender, o) =>
            {
                targetUiElement.CacheMode = null;
                var typedSender = (Storyboard)sender;
                ((PlaneProjection)targetUiElement.Projection).RotationY = endRotationY;
                targetUiElement.Opacity = endOpacity;
                
                if (onCompleted != null) onCompleted();

                typedSender.Completed -= completed;
                typedSender.Stop();
            };

            storyboard.Completed += completed;
            storyboard.Begin();
            return storyboard;
        }

        public override Storyboard RunGoInForwardTransition(NavigationTransitionArgs args) 
        {
            return RunTransition(args.TargetPage, 
                new QuadraticEase { EasingMode = EasingMode.EaseOut },
                startOpacity: 0, endOpacity: 1, 
                startRotationY: -PlaneMagnitude, endRotationY: 0,
                onCompleted: args.OnCompletedAction);
        }


        public override Storyboard RunGoAwayBackwardTransition(NavigationTransitionArgs args)
        {
            return RunTransition(args.TargetPage,
                new QuadraticEase { EasingMode = EasingMode.EaseIn },
                startOpacity: 1, endOpacity: 0, 
                startRotationY: 0, endRotationY: -PlaneMagnitude,
                onCompleted: args.OnCompletedAction);
        }

        public override Storyboard RunGoInBackwardTransition(NavigationTransitionArgs args)
        {
            //var targetUiElement = args.TargetPage.Content;

            //var planeProjection = targetUiElement.Projection as PlaneProjection;
            //if (planeProjection != null)
            //    planeProjection.RotationY = 0;
            //targetUiElement.Opacity = 1;
            //if (args.OnCompletedAction != null)
            //    args.OnCompletedAction();
            //return null;
            return RunTransition(args.TargetPage,
                new QuadraticEase { EasingMode = EasingMode.EaseOut },
                startOpacity: 0, endOpacity: 1,
                startRotationY: PlaneMagnitude, endRotationY: 0,
                onCompleted: args.OnCompletedAction);
        }

        public override Storyboard RunGoAwayForwardTransition(NavigationTransitionArgs args)
        {
            return RunTransition(args.TargetPage, 
                new QuadraticEase { EasingMode = EasingMode.EaseIn },
                startOpacity: 1, endOpacity: 0, 
                startRotationY: 0, endRotationY: PlaneMagnitude,
                onCompleted: args.OnCompletedAction);
        }
    }
}