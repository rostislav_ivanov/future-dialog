﻿using System;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Media.Animation;

namespace MetroLab.Common.Transitions
{
    public class Slide2NavigationTransition : MetroLabNavigationTransitionInfo
    {
        private const double TranslateMagnitude = 120;

        private Storyboard RunTransition(MvvmPage page, EasingFunctionBase easingFunction,
            double startOpacity, double endOpacity, 
            double startPosition, double endPosition, bool animateYAxis = true,
            Action onCompleted = null)
        {
            var targetUiElement = page.Content;
            targetUiElement.RenderTransform = animateYAxis
                ? new TranslateTransform {Y = startPosition}
                : new TranslateTransform {X = startPosition};
            
            targetUiElement.Opacity = startOpacity;

            var storyboard = new Storyboard { Duration = TimeSpan.FromMilliseconds(230) };
            Storyboard.SetTarget(storyboard, targetUiElement);

            var translateYAnimation = new DoubleAnimation
            {
                From = startPosition,
                To = endPosition,
                Duration = new Duration(TimeSpan.FromMilliseconds(220)),
                AutoReverse = false,
                EnableDependentAnimation = true,
                FillBehavior = FillBehavior.HoldEnd,
                EasingFunction = easingFunction
            };
            
            Storyboard.SetTargetProperty(translateYAnimation, animateYAxis ? "(UIElement.RenderTransform).(TranslateTransform.Y)" : "(UIElement.RenderTransform).(TranslateTransform.X)");
            storyboard.Children.Add(translateYAnimation);

            var opacityAnimation = new DoubleAnimation
            {
                From = startOpacity,
                To = endOpacity,
                Duration = new Duration(TimeSpan.FromMilliseconds(220)),
                AutoReverse = false,
                EnableDependentAnimation = true,
                FillBehavior = FillBehavior.HoldEnd,
                EasingFunction = new QuadraticEase { EasingMode = EasingMode.EaseIn }
            };
            Storyboard.SetTargetProperty(opacityAnimation, "Opacity");
            storyboard.Children.Add(opacityAnimation);

            EventHandler<object> completed = null;
            completed = (sender, o) =>
            {
                var typedSender = (Storyboard)sender;
                targetUiElement.Opacity = endOpacity;
                var translateTransform = (TranslateTransform) targetUiElement.RenderTransform;
                if (animateYAxis)
                    translateTransform.Y = endPosition;
                else
                    translateTransform.X = endPosition;
                if (onCompleted != null) onCompleted();

                typedSender.Completed -= completed;
                typedSender.Stop();
            };

            storyboard.Completed += completed;
            storyboard.Begin();
            return storyboard;
        }

        public override Storyboard RunGoInForwardTransition(NavigationTransitionArgs args)
        {
            return RunTransition(args.TargetPage,
                new QuadraticEase { EasingMode = EasingMode.EaseOut },
                startOpacity: 0, endOpacity: 1,
                startPosition: TranslateMagnitude, endPosition: 0,
                animateYAxis: false,
                onCompleted: args.OnCompletedAction);
        }

        public override Storyboard RunGoAwayBackwardTransition(NavigationTransitionArgs args)
        {
            return RunTransition(args.TargetPage,
                new QuadraticEase { EasingMode = EasingMode.EaseIn },
                startOpacity: 1, endOpacity: 0,
                startPosition: 0, endPosition: -TranslateMagnitude,
                onCompleted: args.OnCompletedAction);
        }

        public override Storyboard RunGoInBackwardTransition(NavigationTransitionArgs args)
        {
            return RunTransition(args.TargetPage,
                new QuadraticEase { EasingMode = EasingMode.EaseOut },
                startOpacity: 0, endOpacity: 1,
                startPosition: TranslateMagnitude, endPosition: 0, 
                animateYAxis: false,
                onCompleted: args.OnCompletedAction);
        }

        public override Storyboard RunGoAwayForwardTransition(NavigationTransitionArgs args)
        {
            return RunTransition(args.TargetPage,
                new QuadraticEase { EasingMode = EasingMode.EaseIn },
                startOpacity: 1, endOpacity: 0,
                startPosition: 0, endPosition: TranslateMagnitude,
                onCompleted: args.OnCompletedAction);
        }
    }
}