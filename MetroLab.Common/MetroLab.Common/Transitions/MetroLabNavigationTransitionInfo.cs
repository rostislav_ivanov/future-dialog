﻿using Windows.UI.Xaml.Media.Animation;

namespace MetroLab.Common.Transitions
{
    public abstract class MetroLabNavigationTransitionInfo
    {
        public abstract Storyboard RunGoInForwardTransition(NavigationTransitionArgs args);

        public abstract Storyboard RunGoAwayBackwardTransition(NavigationTransitionArgs args);

        public abstract Storyboard RunGoInBackwardTransition(NavigationTransitionArgs args);

        public abstract Storyboard RunGoAwayForwardTransition(NavigationTransitionArgs args);

    }
}
