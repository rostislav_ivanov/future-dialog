﻿using System;
using System.Globalization;
using System.IO;
using System.Threading.Tasks;
using Windows.Storage;
using Windows.Web.Http;
using Windows.Web.Http.Headers;

namespace MetroLab.Common
{
    internal class DownloadFileClient
    {
        private TaskCompletionSource<bool> _taskCompletionSource;

        public async Task<bool> DownloadFileToCache(bool isAlwaysUpdate, StorageFolder cacheFolder, string serverPath, string localFileName,
            Action<double> changeProgress = null, Action<string> changeLoading = null)
        {
            _taskCompletionSource = new TaskCompletionSource<bool>();
            Execute(isAlwaysUpdate, cacheFolder, serverPath, localFileName, changeProgress, changeLoading);
            return await _taskCompletionSource.Task;
        }

        private async void Execute(bool isAlwaysUpdate, StorageFolder cacheFolder, string requestUriString, string localFileName,
            Action<double> changeProgress = null, Action<string> changeLoading = null)
        {
            try
            {
                using (var httpClient = new HttpClient())
                {
                    if (isAlwaysUpdate)
                        httpClient.DefaultRequestHeaders.CacheControl.Add(new HttpNameValueHeaderValue("Cache-Control", "no-cache"));
                    var asyncOperation = httpClient.GetAsync(new Uri(requestUriString, UriKind.RelativeOrAbsolute),
                        HttpCompletionOption.ResponseContentRead);
                    Task<HttpResponseMessage> responseTask;
                    if (changeProgress == null)
                    {
                        responseTask = asyncOperation.AsTask();
                    }
                    else
                    {
                        string localizedDownloading = CommonLocalizedResources.GetLocalizedString("txt_downloading");
                        changeLoading(localizedDownloading);

                        Action<HttpProgress> progressAction = progress =>
                        {
                            ulong bytesReceived = progress.BytesReceived;
                            if (bytesReceived == 0 || !progress.TotalBytesToReceive.HasValue) return;

                            ulong totalBytesToRecieve = progress.TotalBytesToReceive.Value;

                            double progressValue = bytesReceived/(double) totalBytesToRecieve*100;
                            changeLoading(string.Format("{0} {1}%", localizedDownloading,
                                Math.Round(progressValue)
                                    .ToString(CultureInfo.InvariantCulture)
                                    .PadLeft(3, ' ')));
                            changeProgress(progressValue);
                        };
                        responseTask = asyncOperation.AsTask(new Progress<HttpProgress>(progressAction));
                    }

                    using (var response = await responseTask)
                    {
                        response.EnsureSuccessStatusCode();
                        using (var responseStream = await response.Content.ReadAsInputStreamAsync())
                        {
                            var file = await cacheFolder.CreateFileAsync(localFileName, CreationCollisionOption.ReplaceExisting);

                            using (var transactedWrite = await file.OpenTransactedWriteAsync())
                            {
                                var outputStream = transactedWrite.Stream;
                                var streamForWrite = outputStream.AsStreamForWrite();
                                responseStream.AsStreamForRead().CopyTo(streamForWrite);
                                await streamForWrite.FlushAsync();
                                await outputStream.FlushAsync();
                                await transactedWrite.CommitAsync();
                            }
                        }
                    }
                }
                _taskCompletionSource.TrySetResult(true);
            }
            catch (Exception ex)
            {
                _taskCompletionSource.TrySetException(ex);
            }
        }
    }
}

