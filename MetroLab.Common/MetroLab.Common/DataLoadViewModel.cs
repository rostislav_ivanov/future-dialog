﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Runtime.Serialization;
using System.Threading.Tasks;
using Windows.Networking.Connectivity;
using Windows.UI.Popups;

namespace MetroLab.Common
{
    [DataContract]
    public class DataLoadViewModel : BaseViewModel, IDataLoadViewModel
    {
        protected DataLoadViewModel()
        {
            NetworkInformation.NetworkStatusChanged += NetworkInformationOnNetworkStatusChanged;
        }

        private string _alertMessage = String.Empty;
        [DataMember]
        public string AlertMessage
        {
            get { return _alertMessage; }
            set { SetProperty(ref _alertMessage, value); }
        }

        private string _alertHeader;
        [DataMember]
        public string AlertHeader
        {
            get { return _alertHeader; }
            set { SetProperty(ref _alertHeader, value); }
        }
        
        private bool _isButtonTryAgainVisible;
        [DataMember]
        public bool IsButtonTryAgainVisible
        {
            get { return _isButtonTryAgainVisible; }
            set { SetProperty(ref _isButtonTryAgainVisible, value); }
        }

        private string _loadingMessage = CommonLocalizedResources.GetLocalizedString("Loading");
        [DataMember]
        public string LoadingMessage
        {
            get { return _loadingMessage; }
            set { SetProperty(ref _loadingMessage, value); }
        }

        private bool _isWasProblemsWithConnection;
        [DataMember]
        public bool IsWasProblemsWithConnection
        {
            get { return _isWasProblemsWithConnection; }
            set
            {
                if (value)
                    IsWorkingOffline = true;
                if (SetProperty(ref _isWasProblemsWithConnection, value))
                    OnPropertyChanged("IsLoadingProblemsMessageVisible");
            }
        }

        private bool _isWasErrorOnLoading;
        public bool IsWasErrorOnLoading
        {
            get { return _isWasErrorOnLoading; }
            set
            {
                if (SetProperty(ref _isWasErrorOnLoading, value))
                    OnPropertyChanged("IsLoadingProblemsMessageVisible");
            }
        }

        private bool _isLoadedSuccessfully;
        [IgnoreDataMember]
        public bool IsLoadedSuccessfully
        {
            get { return _isLoadedSuccessfully; }
            set { SetProperty(ref _isLoadedSuccessfully, value); }
        }
        
        private bool _isInitializeLoadingActive = true;
        /// <summary>
        /// True providing that cache is loading now, or there are no cache and loading data from server is in progress
        /// </summary>
        [IgnoreDataMember]
        public bool IsInitializeLoadingActive
        {
            get { return _isInitializeLoadingActive; }
            protected set
            {
                if (SetProperty(ref _isInitializeLoadingActive, value))
                {
                    IsLoadingActive = value || IsUpdatingActive;
                }
                if (value)
                {
                    IsWasProblemsWithConnection = false;
                    IsWasErrorOnLoading = false;
                }
            }
        }

        private string _errorMessage;
        [DataMember]
        public string ErrorMessage
        {
            get { return _errorMessage; }
            set { SetProperty(ref _errorMessage, value); }
        }

        private bool _isLoadingActive = true;

        public bool IsLoadingActive
        {
            get { return _isLoadingActive; }
            set
            {
                if (SetProperty(ref _isLoadingActive, value))
                    OnPropertyChanged("IsLoadingProblemsMessageVisible");
            }
        }
        

        private bool _isUpdatingActive;
        /// <summary>
        /// Animated dots on page's top.
        /// True if any loading is in action now.
        /// </summary>
        [IgnoreDataMember] 
        public bool IsUpdatingActive
        {
            get { return _isUpdatingActive; }
            private set
            {
                if (SetProperty(ref _isUpdatingActive, value))
                {
                    IsLoadingActive = value || IsInitializeLoadingActive;
                    OnPropertyChanged("IsLoadingProblemsMessageVisible");
                    if (value)
                    {
                        IsWasProblemsWithConnection = false;
                        IsWasErrorOnLoading = false;
                        if (!IsLoadedSuccessfully)
                            IsInitializeLoadingActive = true;
                    }
                }
            }
        }

        private int _activeUpdatingLoadingsCount;

        public void BeginDisplayLoadingWhenUpdating()
        {
            if (_activeUpdatingLoadingsCount < 0)
                _activeUpdatingLoadingsCount = 0;
            _activeUpdatingLoadingsCount++;
            IsUpdatingActive = _activeUpdatingLoadingsCount > 0;
        }

        public void EndDisplayLoadingWhenUpdating()
        {
            _activeUpdatingLoadingsCount--;
            if (_activeUpdatingLoadingsCount < 0)
                _activeUpdatingLoadingsCount = 0;
            IsUpdatingActive = _activeUpdatingLoadingsCount > 0;
        }
        
        private bool _isWorkingOffline;

        public bool IsWorkingOffline
        {
            get { return _isWorkingOffline; }
            set { SetProperty(ref _isWorkingOffline, value); }
        }

        [IgnoreDataMember]
        public string WorkingOfflineMessage { get { return CommonLocalizedResources.GetLocalizedString("txt_offline"); } }

        private string _noInternetMessage = null;
        [IgnoreDataMember]
        protected string NoInternetMessage
        {
            //To allow user to set default value back by setting _loadFailedMessage = null
            get { return _noInternetMessage ?? CommonLocalizedResources.GetLocalizedString("exceptionMsg_ServerNotRespondOrConnectionFailure"); }
            set { _noInternetMessage = value; }
        }

        private string _noInternetHeader = null;
        [IgnoreDataMember]
        protected string NoInternetHeader
        {
            get { return _noInternetHeader ?? CommonLocalizedResources.GetLocalizedString("exceptionHeader_ServerNotRespondOrConnectionFailure"); }
            set { _noInternetHeader = value; }
        }

        private string _loadFailedMessage = null;
        [IgnoreDataMember]
        protected string LoadFailedMessage
        {
            get { return _loadFailedMessage ?? CommonLocalizedResources.GetLocalizedString("exceptionMsg_LoadFailed"); }
            set { _loadFailedMessage = value; }
        }

        private string _loadFailedHeader = null;
        [IgnoreDataMember]
        protected string LoadFailedHeader
        {
            get { return _loadFailedHeader ?? CommonLocalizedResources.GetLocalizedString("exceptionHeader_LoadFailed"); }
            set { _loadFailedHeader = value; }
        }

        private string _dialogOkButtonTitle = null;
        [IgnoreDataMember]
        protected string DialogOkButtonTitle
        {
            get { return _dialogOkButtonTitle ?? CommonLocalizedResources.GetLocalizedString("Ok"); }
            set { _dialogOkButtonTitle = value; }
        }

        [IgnoreDataMember]
        public bool IsLoadingProblemsMessageVisible 
        {
            get { return (IsWasProblemsWithConnection || IsWasErrorOnLoading) && !IsLoadingActive && !IsLoadedSuccessfully; }
        }

        private List<WeakReference<ILoadedOrUpdatedSuccessfullyListener>> _loadedOrUpdatedSuccessfullyListers =
            new List<WeakReference<ILoadedOrUpdatedSuccessfullyListener>>();

        public void AddLoadedOrUpdatedSuccessfullyListener(ILoadedOrUpdatedSuccessfullyListener listener)
        {
            if (IsLoadedSuccessfully && listener != null)
                listener.OnLoadedOrUpdatedSuccessfully(this, EventArgs.Empty);
            
            if (_loadedOrUpdatedSuccessfullyListers != null)
                _loadedOrUpdatedSuccessfullyListers.Add(new WeakReference<ILoadedOrUpdatedSuccessfullyListener>(listener));
        }

        public void RemoveLoadedOrUpdatedSuccessfullyListener(ILoadedOrUpdatedSuccessfullyListener listener)
        {
            for (int i = 0; i < _loadedOrUpdatedSuccessfullyListers.Count; i++)
            {
                var item = _loadedOrUpdatedSuccessfullyListers[i];
                ILoadedOrUpdatedSuccessfullyListener itemListener;
                if (item.TryGetTarget(out itemListener) && Equals(listener, itemListener))
                {
                    _loadedOrUpdatedSuccessfullyListers.RemoveAt(i);
                    return;
                }
            }
        }

        public static bool GetIsConnectedToNetwork()
        {
            var profile = NetworkInformation.GetInternetConnectionProfile();
            if (profile != null)
            {
                var networkConnectivityLevel = profile.GetNetworkConnectivityLevel();
                if (networkConnectivityLevel == NetworkConnectivityLevel.InternetAccess)
                    return true;
            }
            return false;
        }

        public void UpdateGlobalIsWorkingOffline()
        {
            if (GetIsConnectedToNetwork())
            {
                IsWorkingOffline = false; 
                if (!IsLoadedSuccessfully) UpdateAsync();                
            }
            else
            {
                IsWorkingOffline = true;
            }
        }

        private void NetworkInformationOnNetworkStatusChanged(object sender)
        {
            UpdateGlobalIsWorkingOffline();
        }

        private void OnLoadedOrUpdatedSuccessfully(EventArgs args)
        {
            var listeners = _loadedOrUpdatedSuccessfullyListers.ToList();
            foreach (var weakReference in listeners)
            {
                ILoadedOrUpdatedSuccessfullyListener itemListener;
                if (weakReference.TryGetTarget(out itemListener) && itemListener != null)
                {
                    itemListener.OnLoadedOrUpdatedSuccessfully(this, args);
                }
            }
        }

        [OnDeserialized]
        public void OnDeserialized(StreamingContext context)
        {
            _loadedOrUpdatedSuccessfullyListers = new List<WeakReference<ILoadedOrUpdatedSuccessfullyListener>>();
        }

        public virtual void UnSubscribeFromEvents()
        {
            NetworkInformation.NetworkStatusChanged -= NetworkInformationOnNetworkStatusChanged;
        }

        protected virtual async Task<bool> LoadAsync() { return true; }

        private Task<bool> _loadingTask;
        private Task _initializeTask;
        
        protected Task<bool> TryLoadAsync()
        {
            lock (this)
            {
                return _loadingTask ?? (_loadingTask = TryLoadAsyncInner(async () => await LoadAsync()));
            }
        }
        
        protected Task<CacheState> TryLoadFromCache()
        {
            return _tryLoadFromCacheTask ?? (_tryLoadFromCacheTask = TryLoadFromCacheInner());
        }

        private Task<CacheState> _tryLoadFromCacheTask;
        private async Task<CacheState> TryLoadFromCacheInner()
        {
            try
            {
                var cacheResult = await LoadFromCacheAsync();
                _isContentActual = (cacheResult & CacheState.Actual) == CacheState.Actual;
                return cacheResult;
            }
            catch (Exception)
            {
                //if (Debugger.IsAttached) Debugger.Break();
                return CacheState.Empty;
            }
        }

        protected bool _isContentActual;

        protected virtual async Task<CacheState> LoadFromCacheAsync()
        {
            return CacheState.Empty;
        }
        
        protected async void RunLoadFailed(string alertMessage, bool isNeedToShowDialog = true,
            UICommandInvokedHandler dialogOkAction = null, string alertHeader = "", bool isButtonTryAgainVisible = false)
        {
            AlertMessage = alertMessage;
            AlertHeader = alertHeader;
            IsButtonTryAgainVisible = isButtonTryAgainVisible;

            if (isNeedToShowDialog) await ShowDialogAsync(AlertMessage, string.Empty, dialogOkAction, DialogOkButtonTitle);

            EndDisplayLoadingWhenUpdating();
        }

        protected virtual async Task<bool> TryLoadAsyncInner(Func<Task<bool>> loadAction)
        {
            try
            {
                var isLoadedSuccessfully = await loadAction();
                return isLoadedSuccessfully;
            }
            catch (WebException ex)
            {
                IsWasProblemsWithConnection = true;
                if (ex.Response == null && ((byte)ex.Status == 1 || (byte)ex.Status == 2))
                    RunLoadFailed(NoInternetMessage, false, null, NoInternetHeader);
                else RunLoadFailed(LoadFailedMessage, false, null, LoadFailedHeader, true);

                return false;
            }
            catch (HttpRequestException hrex)
            {
                IsWasProblemsWithConnection = true;
                var webException = hrex.InnerException as WebException;
                if (webException != null && webException.Response == null && ((byte)webException.Status == 1 || (byte)webException.Status == 2))
                    RunLoadFailed(NoInternetMessage, false, null, NoInternetHeader);
                else RunLoadFailed(LoadFailedMessage, false, null, LoadFailedHeader, true);

                return false;
            }
            catch (Exception e)
            {
                if (Debugger.IsAttached) Debugger.Break();
                IsWasErrorOnLoading = true;
                RunLoadFailed(LoadFailedMessage, false, null, LoadFailedHeader);
                return false;
            }
        }

        /// <summary>
        /// On first opening
        /// </summary>
        public async Task InitializeAsync()
        {
            await (_initializeTask ?? (_initializeTask = InitializeInnerAsync()));
        }

        private async Task InitializeInnerAsync()
        {
            IsInitializeLoadingActive = true;

            var cacheState = await TryLoadFromCache();
            if ((cacheState & CacheState.Actual) == CacheState.Actual)
            {
                IsInitializeLoadingActive = false;
                EndDisplayLoadingWhenUpdating();
                IsLoadedSuccessfully = true;
                OnLoadedOrUpdatedSuccessfully(EventArgs.Empty);
                return;
            }

            if ((cacheState & CacheState.Available) == CacheState.Available)
            {
                IsInitializeLoadingActive = false;
                IsLoadedSuccessfully = true;
                BeginDisplayLoadingWhenUpdating();
                OnLoadedOrUpdatedSuccessfully(EventArgs.Empty);
            }

            var isLoadedFromServerSuccessfully = await TryLoadAsync();
            if (isLoadedFromServerSuccessfully)
            {
                IsLoadedSuccessfully = true;
                OnLoadedOrUpdatedSuccessfully(EventArgs.Empty);
            }
            EndDisplayLoadingWhenUpdating();
            IsInitializeLoadingActive = false;
        }

        protected async Task LoadNextItem()
        {
            _isContentActual = false;
            IsInitializeLoadingActive = true;
            IsLoadedSuccessfully = false;

            // ToDo: Need to know is code bellow works!!!
            //if(_loadingTask != null) _loadingTask.AsAsyncOperation().Cancel();

            _loadingTask = null;
            var isLoadedFromServerSuccessfully = await TryLoadAsync();
            if (isLoadedFromServerSuccessfully)
            {
                IsLoadedSuccessfully = true;
                OnLoadedOrUpdatedSuccessfully(EventArgs.Empty);
            }
            IsInitializeLoadingActive = false;
        }

        [IgnoreDataMember]
        public virtual bool UpdatingOnNavigationEnabled { get { return false; } }

        protected virtual async Task<bool> UpdateInnerAsync() { return true; }

        public async Task UpdateAsync()
        {
            try
            {
                BeginDisplayLoadingWhenUpdating();
                IsButtonTryAgainVisible = false;
                _isContentActual = false;
                _loadingTask = null;
                var isLoadedFromServerSuccessfully = await TryLoadAsyncInner(UpdateInnerAsync);
                if (isLoadedFromServerSuccessfully)
                {
                    IsLoadedSuccessfully = true;
                    OnLoadedOrUpdatedSuccessfully(EventArgs.Empty);
                }
            }
            finally
            {
                EndDisplayLoadingWhenUpdating();
            }
        }

        public virtual async Task OnDeserializedFromStorageAsync()
        {
            NetworkInformation.NetworkStatusChanged += NetworkInformationOnNetworkStatusChanged;
        }
    }
}
