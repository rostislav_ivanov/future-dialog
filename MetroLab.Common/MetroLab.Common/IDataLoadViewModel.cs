﻿using System.Threading.Tasks;

namespace MetroLab.Common
{
    public interface IDataLoadViewModel
    {
        Task InitializeAsync();

        void AddLoadedOrUpdatedSuccessfullyListener(ILoadedOrUpdatedSuccessfullyListener listener);

        void RemoveLoadedOrUpdatedSuccessfullyListener(ILoadedOrUpdatedSuccessfullyListener listener);
        
        bool UpdatingOnNavigationEnabled { get; }

        Task UpdateAsync();
    }
}
