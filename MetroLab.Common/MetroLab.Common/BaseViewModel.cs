﻿using System.Threading;
using MetroLog;
using NotificationsExtensions.ToastContent;
using System;
using System.Runtime.Serialization;
using System.Threading.Tasks;
using System.Windows.Input;
using Windows.Foundation;
using Windows.UI.Notifications;
using Windows.UI.Popups;

namespace MetroLab.Common
{
    [DataContract]
    [KnownType(typeof(DataLoadViewModel))]
    public abstract class BaseViewModel : BindableBase
    {
        protected BaseViewModel()
        {
            Log = LogManagerFactory.DefaultLogManager.GetLogger(GetType());
        }

        [IgnoreDataMember] protected ILogger Log { get; set; }

        private Guid _guid = Guid.NewGuid();
        [DataMember]
        public Guid Guid {
            get { return _guid; }
            set { SetProperty(ref _guid, value); }
        }

        private readonly static SemaphoreSlim MessageBoxSemaphore = new SemaphoreSlim(1);

        public static async Task ShowDialogAsync(string message, string title = "", 
            UICommandInvokedHandler dialogOkAction = null, string dialogOkTitle = null)
        {
            await MessageBoxSemaphore.WaitAsync();
            await (await AppViewModel.GetMainAsync()).RunAtDispatcherAsync(
                async () =>
                {
                    try
                    {
                        var md = string.IsNullOrEmpty(title)
                            ? new MessageDialog(message)
                            : new MessageDialog(message, title);
                        md.Commands.Add(new UICommand(
                            dialogOkTitle ?? CommonLocalizedResources.GetLocalizedString("Ok"), dialogOkAction));
                        await md.ShowAsync();
                    }
                    finally
                    {
                        MessageBoxSemaphore.Release();
                    }
                });
        }

        public static async Task ShowOkCancelDialogAsync(string message, string title = "",
           UICommandInvokedHandler dialogOkAction = null, string dialogOkTitle = null, string dialogCancelTitle = null)
        {
            await MessageBoxSemaphore.WaitAsync();
            await (await AppViewModel.GetMainAsync()).RunAtDispatcherAsync(
                async () =>
                {
                    try
                    {
                        var md = string.IsNullOrEmpty(title) ? new MessageDialog(message) : new MessageDialog(message, title);
                        md.Commands.Add(new UICommand(dialogOkTitle ?? CommonLocalizedResources.GetLocalizedString("Ok"), dialogOkAction));
                        md.Commands.Add(new UICommand(dialogCancelTitle ?? CommonLocalizedResources.GetLocalizedString("Cancel"), null));
                        await md.ShowAsync();
                    }
                    finally
                    {
                        MessageBoxSemaphore.Release();
                    }
                });
        }

        public static void ShowToast(string textHeading, string textBody,
            TypedEventHandler<ToastNotification, object> onActivatedHandler = null)
        {
            var toastContent = ToastContentFactory.CreateToastText02();
            toastContent.TextHeading.Text = textHeading;
            toastContent.TextBodyWrap.Text = textBody;
            var tostNotification = toastContent.CreateNotification();
            if (onActivatedHandler != null)
                tostNotification.Activated += onActivatedHandler;

            ToastNotificationManager.CreateToastNotifier().Show(tostNotification);
        }

        public static ICommand GetCommand(ref ICommand command, Action<object> action, bool isCanExecute = true)
        {
            if (command != null) return command;

            var c = new ActionCommand { IsCanExecute = isCanExecute };
            c.ExecuteAction += action;
            command = c;

            return command;
        }
    }
}
