﻿namespace MetroLab.Common
{
    public interface IPrintHelper
    {
        void PrintTaskRequested(object printArgs);
    }
}
