﻿using Windows.Storage;

namespace MetroLab.Common
{
    public static class SettingsStorageHelper
    {
        public static T GetSetting<T>(string key, T defaultValue = default(T))
        {
            var values = ApplicationData.Current.LocalSettings.Values;
            return values.ContainsKey(key) && values[key] is T ? (T)values[key] : defaultValue;
        }

        public static void SetSetting(string key, object value)
        {
            ApplicationData.Current.LocalSettings.Values[key] = value;
        }
    }
}
