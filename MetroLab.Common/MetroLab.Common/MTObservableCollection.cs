﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Collections.Specialized;
using System.ComponentModel;
using System.Diagnostics;
using System.Linq;
using Windows.UI.Core;
using Windows.UI.Xaml;

namespace MetroLab.Common
{
    /// <summary>
    /// Multi-Threaded ObservableCollection
    /// </summary>
    /// <typeparam name="T"></typeparam>
    public class MTObservableCollection<T> : ObservableCollection<T>
    {
        private readonly Dictionary<NotifyCollectionChangedEventHandler, CoreDispatcher> _collectionChanged =
            new Dictionary<NotifyCollectionChangedEventHandler, CoreDispatcher>();

        public override event NotifyCollectionChangedEventHandler CollectionChanged
        {
            add
            {
                CoreDispatcher dispatcher = null;
                if (Window.Current != null && Window.Current.CoreWindow != null)
                    dispatcher = Window.Current.CoreWindow.Dispatcher;
                if (dispatcher == null && Debugger.IsAttached)
                    Debugger.Break();
                if (!_collectionChanged.ContainsKey(value))
                    _collectionChanged.Add(value, dispatcher);
            }
            remove { _collectionChanged.Remove(value); }
        }

        public MTObservableCollection() : base() { }

        public MTObservableCollection(IEnumerable<T> collection) : base(collection) { }

        protected async override void OnCollectionChanged(NotifyCollectionChangedEventArgs eventArgs)
        {
            OnPropertyChanged(new PropertyChangedEventArgs("Count"));
            List<KeyValuePair<NotifyCollectionChangedEventHandler, CoreDispatcher>> items;
            lock (_collectionChanged)
                items = _collectionChanged.ToList();
            foreach (var item in items)
            {
                CoreDispatcher dispatcher = item.Value;
                if (dispatcher == null)
                {
                    try
                    {
                        item.Key(this, eventArgs);
                    }
                    catch (Exception e)
                    {
                        if (Debugger.IsAttached)
                            Debugger.Break();
                    }
                    continue;
                }

                if (dispatcher.HasThreadAccess)
                    item.Key(this, eventArgs);
                else
                {
                    KeyValuePair<NotifyCollectionChangedEventHandler, CoreDispatcher> item1 = item;
                    await dispatcher.RunAsync(CoreDispatcherPriority.Normal, () => item1.Key(this, eventArgs));
                }
            }
        }

        private readonly Dictionary<PropertyChangedEventHandler, CoreDispatcher> _propertyChanged =
            new Dictionary<PropertyChangedEventHandler, CoreDispatcher>();

        /// <summary>
        /// Multicast event for property change notifications.
        /// </summary>
        protected override event PropertyChangedEventHandler PropertyChanged
        {
            add
            {
                var dispatcher = Window.Current.CoreWindow.Dispatcher;
                if (dispatcher == null && Debugger.IsAttached)
                    Debugger.Break();
                lock (_propertyChanged)
                    if (!_propertyChanged.ContainsKey(value))
                        _propertyChanged.Add(value, dispatcher);
            }
            remove
            {
                lock (_propertyChanged)
                    _propertyChanged.Remove(value);
            }
        }

        protected async override void OnPropertyChanged(PropertyChangedEventArgs eventArgs)
        {
            List<KeyValuePair<PropertyChangedEventHandler, CoreDispatcher>> items;
            lock (_propertyChanged)
                items = _propertyChanged.ToList();
            foreach (var item in items)
            {
                CoreDispatcher dispatcher = item.Value;
                if(dispatcher == null) continue;
                
                if (dispatcher.HasThreadAccess)
                    item.Key(this, eventArgs);
                else
                {
                    KeyValuePair<PropertyChangedEventHandler, CoreDispatcher> item1 = item;
                    await dispatcher.RunAsync(CoreDispatcherPriority.Normal, () => item1.Key(this, eventArgs));
                }
            }
        }
    }
}
