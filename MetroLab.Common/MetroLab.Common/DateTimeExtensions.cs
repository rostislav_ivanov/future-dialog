﻿using System;
using System.Diagnostics;
using System.Diagnostics.Contracts;
using System.Globalization;
using System.Linq;

namespace MetroLab.Common
{
    public static class DateTimeExtensions
    {
        private static readonly string[] SupportedLanguages = { "en", "ru" };

        private static string GetMostSuitableLangTwoLetters()
        {
            var currentLangTwoLetters = CultureInfo.CurrentUICulture.TwoLetterISOLanguageName.ToLower();
            return SupportedLanguages.Contains(currentLangTwoLetters) ? currentLangTwoLetters : "en";
        }
        
        [Pure]
        public static string ToUserFriendlyTimeAgoString(this DateTime d)
        {
            var rule = CommonLanguageRules.GetPluralRule(GetMostSuitableLangTwoLetters());
            Func<int, string, string> makeSentense = (number, stringVariants) =>
            {
                try
                {
                    var strings = stringVariants.Split(';').Select(x => x.Trim()).ToArray();
                    var resString = strings[rule(number, strings.Length)];
                    return string.Format("{0} {1}", number, resString);
                }
                catch (Exception)
                {
                    if (Debugger.IsAttached) 
                        Debugger.Break();
                    return number + " ??? ";
                }
            };

            var dayDiff = (int)DateTime.Now.Subtract(d).TotalDays;
            if (dayDiff / 365 >= 1)
                return CommonLocalizedResources.GetLocalizedString("txt_datestring_overayearago");

            var monthsAgo = dayDiff / 31;
            if (monthsAgo >= 1)
                return makeSentense(monthsAgo, CommonLocalizedResources.GetLocalizedString("txt_datestring_monthsago"));

            var weeksAgo = dayDiff / 7;
            if (weeksAgo >= 1)
                return makeSentense(weeksAgo, CommonLocalizedResources.GetLocalizedString("txt_datestring_weeksago"));

            if (dayDiff == 0) return CommonLocalizedResources.GetLocalizedString("txt_datestring_today");
            if (dayDiff == 1) return CommonLocalizedResources.GetLocalizedString("txt_datestring_yesterday");

            return makeSentense(dayDiff, CommonLocalizedResources.GetLocalizedString("txt_datestring_daysago"));
        }
        
        [Pure]
        public static string ToUserFriendlyString(this TimeSpan timeSpan)
        {
            var rule = CommonLanguageRules.GetPluralRule(GetMostSuitableLangTwoLetters());
            Func<int, string, string> makeSentense = (number, stringVariants) =>
            {
                try
                {
                    var strings = stringVariants.Split(';').Select(x => x.Trim()).ToArray();
                    var resString = strings[rule(number, strings.Length)];
                    return string.Format("{0} {1}", number, resString);
                }
                catch (Exception)
                {
                    if (Debugger.IsAttached)
                        Debugger.Break();
                    return "";
                }
            };
            var result = string.Empty;
            if (timeSpan.Days > 0)
                result += makeSentense(timeSpan.Days, CommonLocalizedResources.GetLocalizedString("days")) + " ";
            if (timeSpan.Hours > 0)
                result += makeSentense(timeSpan.Hours, CommonLocalizedResources.GetLocalizedString("hours")) + " ";
            if (timeSpan.Minutes > 0)
                result += makeSentense(timeSpan.Minutes, CommonLocalizedResources.GetLocalizedString("minutes")) + " ";
            if (timeSpan.Seconds > 0)
                result += makeSentense(timeSpan.Seconds, CommonLocalizedResources.GetLocalizedString("seconds")) + " ";
            return result;
        }
    }
}
