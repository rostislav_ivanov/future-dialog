﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Diagnostics.Contracts;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using Windows.Security.Cryptography.Core;
using Windows.Storage;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Media.Animation;
using Windows.UI.Xaml.Media.Imaging;
using MetroLog;

namespace MetroLab.Common
{
    public abstract class ContentLoader
    {
        public abstract class ElementCacheHelper
        {
            public abstract object Target { get; }
            public abstract void InitializeWithObject(object target);
            public abstract void RemoveContent();
            public abstract Task<bool> IsSourceEmptyAsync();
            public virtual void SetProgressValue(double value) { }
            public virtual void SetLoadingText(string value) { }
            public virtual void SetException(Exception value) { }
            public virtual bool NeedToInformProgressAndThrowException { get { return false; } }

            internal bool DoNotUseCache { get; set; }

            public abstract Task SetSourceAsync(Uri localUri);
        }

        public abstract class ElementCacheHelper<T> : ElementCacheHelper
        {
            public sealed override object Target { get { return TargetObject; } }
            public T TargetObject { get; private set; }

            public override void InitializeWithObject(object target)
            {
                TargetObject = (T)target;
            }
        }

        protected class SystemImageCacheHelper : ElementCacheHelper<Image>
        {
            internal static readonly Dictionary<Uri, WeakReference<BitmapImage>> Cache = new Dictionary<Uri, WeakReference<BitmapImage>>();

            public override void RemoveContent()
            {
                TargetObject.Source = null;
            }

            public override async Task SetSourceAsync(Uri localUri)
            {
                var image = TargetObject;
                BitmapImage source = null;
                if (!DoNotUseCache)
                {
                    lock (Cache)
                    {
                        WeakReference<BitmapImage> weakReference;
                        if (Cache.TryGetValue(localUri, out weakReference))
                            weakReference.TryGetTarget(out source);
                    }
                }
                if (source == null)
                {
                    source = new BitmapImage();
                    StorageFile file; 
                    if (localUri.OriginalString.StartsWith("ms-appdata:"))
                        file = await StorageFile.GetFileFromApplicationUriAsync(localUri);
                    else
                        file = await StorageFile.GetFileFromPathAsync(localUri.OriginalString);
                    using (var fileStream = await file.OpenReadAsync())
                    {
                        source.SetSource(fileStream);
                    }
                    lock (Cache)
                    {
                        if (!Cache.ContainsKey(localUri))
                            Cache.Add(localUri, new WeakReference<BitmapImage>(source));
                        else
                            Cache[localUri] = new WeakReference<BitmapImage>(source);
                    }
                }
                image.Source = source;
            }

            public override async Task<bool> IsSourceEmptyAsync()
            {
                return TargetObject.Source == null;
            }
        }

        private class MediaElementCacheHelper : ElementCacheHelper<MediaElement>
        {
            public override void RemoveContent()
            {
                TargetObject.Source = null;
            }

            public override async Task SetSourceAsync(Uri localUri)
            {
                var image = TargetObject;
                image.Source = localUri;
            }

            public override async Task<bool> IsSourceEmptyAsync()
            {
                return TargetObject.Source == null;
            }
        }

        private class TaskCompletionSourceCacheHelper : ElementCacheHelper<TaskCompletionSource<Uri>>
        {
            public override void RemoveContent()
            {

            }

            public override async Task SetSourceAsync(Uri localUri)
            {
                TargetObject.TrySetResult(localUri);
            }

            public override async Task<bool> IsSourceEmptyAsync()
            {
                return false;
            }
        }

        protected class TaskCompletionSourceWithCallbacks<T> : TaskCompletionSource<T>
        {
            public Action<double> ProgressValueCallback { get; set; }
            public Action<string> LoadingTextCallback { get; set; }
        }

        private class TaskCompletionSourceWithCallbacksCacheHelper : ElementCacheHelper<TaskCompletionSourceWithCallbacks<Uri>>
        {
            public override void RemoveContent()
            {

            }

            public override async Task SetSourceAsync(Uri localUri)
            {
                TargetObject.TrySetResult(localUri);
            }

            public override async Task<bool> IsSourceEmptyAsync() { return false; }

            public override void SetProgressValue(double value)
            {
                if (TargetObject.ProgressValueCallback != null)
                    TargetObject.ProgressValueCallback(value);
            }

            public override void SetLoadingText(string value)
            {
                if (TargetObject.LoadingTextCallback != null)
                    TargetObject.LoadingTextCallback(value);
            }

            public override void SetException(Exception value)
            {
                if (TargetObject.ProgressValueCallback != null)
                    TargetObject.TrySetException(value);
            }

            public override bool NeedToInformProgressAndThrowException { get { return true; } }
        }

        protected ILogger Log { get; set; }

 public static bool GetRemoveOldContentWhenLoading(DependencyObject frameworkControl) { return (bool)frameworkControl.GetValue(RemoveOldContentWhenLoadingProperty); }
        public static void SetRemoveOldContentWhenLoading(DependencyObject frameworkControl, bool value) { frameworkControl.SetValue(RemoveOldContentWhenLoadingProperty, value); }

        public static readonly DependencyProperty RemoveOldContentWhenLoadingProperty = DependencyProperty.RegisterAttached(
            "RemoveOldContentWhenLoading", typeof(bool), typeof(ContentLoader), new PropertyMetadata(true));

        private static readonly object SupportedElementsLocker = new object();
        //object, ElementCacheHelper
        private static readonly Dictionary<Type, Type> SupportedElements = new Dictionary<Type, Type>();
        public static void AddSupportedElementType(Type objectType, Type cacheHelperType)
        {
            lock (SupportedElementsLocker)
                SupportedElements.Add(objectType, cacheHelperType);
        }

        static ContentLoader()
        {
            AddSupportedElementType(typeof(Image), typeof(SystemImageCacheHelper));
            AddSupportedElementType(typeof(TaskCompletionSource<Uri>), typeof(TaskCompletionSourceCacheHelper));
            AddSupportedElementType(typeof(TaskCompletionSourceWithCallbacks<Uri>), typeof(TaskCompletionSourceWithCallbacksCacheHelper));
            AddSupportedElementType(typeof(MediaElement), typeof(MediaElementCacheHelper));
        }

        protected ContentLoader()
        {
            Log = LogManagerFactory.DefaultLogManager.GetLogger(GetType());
        }

        public static ElementCacheHelper GetHelper(object targetObject)
        {
            var type = targetObject.GetType();
            lock (SupportedElementsLocker)
            {
                if (!SupportedElements.ContainsKey(type))
                    if (Debugger.IsAttached)
                        Debugger.Break();
                var cacherHelper = (ElementCacheHelper)Activator.CreateInstance(SupportedElements[type]);
                cacherHelper.InitializeWithObject(targetObject);
                return cacherHelper;
            }
        }
        
        private static readonly HashAlgorithmProvider HashProvider = HashAlgorithmProvider.OpenAlgorithm(HashAlgorithmNames.Sha256);
        
        [Pure]
        protected static string GetCachedFileName(string serverFilePath)
        {
            using (var stream = new MemoryStream())
            {
                using (var binaryWriter = new BinaryWriter(stream))
                {
                    binaryWriter.Write(serverFilePath);
                    stream.Seek(0, SeekOrigin.Begin);
                    var sourceBuffer = stream.ToArray().AsBuffer();

                    var cryptographicHash = HashProvider.CreateHash();
                    cryptographicHash.Append(sourceBuffer);
                    var resultBuffer = cryptographicHash.GetValueAndReset();
                    var resultArray = resultBuffer.ToArray();

                    var resultFileNameWithoutExtension = Base32.Base32Encode(resultArray);

                    var regex = new Regex(@"^(([^:/?#]+):)?(//([^/?#]*))?([^?#]*)(\?([^#]*))?(#(.*))?");
                    var match = regex.Match(serverFilePath);
                    if (match.Success)
                    {
                        var query = match.Groups[5].Value;
                        if (!string.IsNullOrEmpty(query))
                        {
                            var extensionPosition = query.LastIndexOf('.');
                            if (extensionPosition > -1)
                            {
                                var extension = query.Substring(extensionPosition);
                                if (extension != null && !Path.GetInvalidPathChars().Intersect(extension.ToCharArray()).Any())
                                    return resultFileNameWithoutExtension + extension;
                            }
                        }
                    }
                    
                    return resultFileNameWithoutExtension;
                }
            }
        }
    }
}
