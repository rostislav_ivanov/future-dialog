﻿using System;

namespace MetroLab.Common
{
    public enum NavigationType
    {
        New,
        NewAndRemovePrevious,
        Back,
        Forward,
        NotShow
    }

    public class ShowViewModelPageEventArgs : EventArgs
    {
        public IPageViewModel PageViewModel { get; private set; }
        public NavigationType Direction { get; private set; }

        public ShowViewModelPageEventArgs(IPageViewModel pageViewModel, NavigationType direction)
        {
            PageViewModel = pageViewModel;
            Direction = direction;
        }
    }
}