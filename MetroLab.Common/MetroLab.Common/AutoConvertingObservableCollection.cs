﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Diagnostics;
using System.Linq;
using Windows.UI.Core;
using Windows.UI.Xaml;

namespace MetroLab.Common
{
    public class AutoConvertingObservableCollection<T, TSource> : BindableBase, IList, IList<T>, INotifyCollectionChanged        
    {
        public Func<TSource, T> Converter { get; private set; }
        public Func<T, TSource> BackConverter { get; private set; }
        
        public AutoConvertingObservableCollection(Func<TSource, T> converter, Func<T, TSource> backConverter)
        {
            if (converter == null) throw new ArgumentNullException("converter");
            if (backConverter == null) throw new ArgumentNullException("backConverter");
            Converter = converter;
            BackConverter = backConverter;
        }

        private IList<TSource> _sourceCollection;

        public IList<TSource> SourceCollection
        {
            get { return _sourceCollection; }
            set
            {               
                var oldSourceCollection = _sourceCollection as INotifyCollectionChanged;
                if (oldSourceCollection != null)
                    oldSourceCollection.CollectionChanged -= SourceCollectionOnCollectionChanged;                
                var newSourceCollection = value as INotifyCollectionChanged;
                if (newSourceCollection != null)
                    newSourceCollection.CollectionChanged += SourceCollectionOnCollectionChanged;                
                _sourceCollection = value;
                var args = new NotifyCollectionChangedEventArgs(NotifyCollectionChangedAction.Reset);
                OnCollectionChanged(args);
            }
        }

        private void SourceCollectionOnCollectionChanged(object sender, NotifyCollectionChangedEventArgs sourceArgs)
        {
            try
            {
                NotifyCollectionChangedEventArgs args;
                if (sourceArgs.Action == NotifyCollectionChangedAction.Reset)
                {
                    args = new NotifyCollectionChangedEventArgs(NotifyCollectionChangedAction.Reset);
                }
                else if (sourceArgs.Action == NotifyCollectionChangedAction.Move)
                {
                    args = new NotifyCollectionChangedEventArgs(NotifyCollectionChangedAction.Move);
                }
                else if (sourceArgs.Action == NotifyCollectionChangedAction.Add)
                {
                    args = new NotifyCollectionChangedEventArgs(NotifyCollectionChangedAction.Add,
                                                                sourceArgs.NewItems.Cast<TSource>()
                                                                          .Select(Converter)
                                                                          .ToList(), sourceArgs.NewStartingIndex);
                }
                else if (sourceArgs.Action == NotifyCollectionChangedAction.Remove)
                {
                    args = new NotifyCollectionChangedEventArgs(NotifyCollectionChangedAction.Remove,
                                                                sourceArgs.OldItems.Cast<TSource>()
                                                                          .Select(Converter)
                                                                          .ToList(), sourceArgs.OldStartingIndex);
                }
                else
                {
#if DEBUG
                    if (System.Diagnostics.Debugger.IsAttached)
                        System.Diagnostics.Debugger.Break();
#endif
                    throw new NotImplementedException();
                }
                OnCollectionChanged(args);
            }
            catch (Exception e)
            {
                if (Debugger.IsAttached)
                    Debugger.Break();
            }
        }

        public void Add(T item)
        {
            var storeItem = BackConverter(item);
            SourceCollection.Add(storeItem);
        }

        public void Insert(int index, T item)
        {
            var storeItem = BackConverter(item);
            SourceCollection.Insert(index, storeItem);
        }

        public int IndexOf(T item)
        {
            var storeItem = BackConverter(item);
            return SourceCollection.IndexOf(storeItem);
        }

        public T this[int index]
        {
            get
            {
                var x = SourceCollection[index];
                return Converter(x);
            }
            set
            {
                SourceCollection[index] = BackConverter(value);
            }
        }

        public void Clear()
        {
            SourceCollection.Clear();
        }

        public bool Contains(T item)
        {
            var storeItem = BackConverter(item);
            return SourceCollection.Contains(storeItem);
        }

        public void CopyTo(T[] array, int arrayIndex)
        {
            var arrayForSourceCollection = array.Select(BackConverter).ToArray();
            SourceCollection.CopyTo(arrayForSourceCollection, arrayIndex);
        }

        public int Count
        {
            get
            {
                var sourceCollection = SourceCollection;
                return sourceCollection == null ? 0 : sourceCollection.Count;
            }
        }

        public bool IsReadOnly
        {
            get { return true; }
        }

        public bool Remove(T item)
        {
            var storeItem = BackConverter(item);
            if (Equals(storeItem, null)) return false;
            return SourceCollection.Remove(storeItem);
        }

        public void RemoveAt(int index)
        {
            SourceCollection.RemoveAt(index);
        }

        public IEnumerator<T> GetEnumerator()
        {
            return SourceCollection.Select(Converter).GetEnumerator();
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return GetEnumerator();
        }

        private readonly Dictionary<NotifyCollectionChangedEventHandler, CoreDispatcher> _collectionChanged =
            new Dictionary<NotifyCollectionChangedEventHandler, CoreDispatcher>();

        public event NotifyCollectionChangedEventHandler CollectionChanged
        {
            add
            {
                var dispatcher = Window.Current.CoreWindow.Dispatcher;
                if (dispatcher == null && Debugger.IsAttached)
                    Debugger.Break();
                if (!_collectionChanged.ContainsKey(value))
                    _collectionChanged.Add(value, dispatcher);
            }
            remove { _collectionChanged.Remove(value); }
        }

        private async void OnCollectionChanged(NotifyCollectionChangedEventArgs args)
        {
            List<KeyValuePair<NotifyCollectionChangedEventHandler, CoreDispatcher>> items;
            lock (_collectionChanged)
                items = _collectionChanged.ToList();
            foreach (var item in items)
            {
                CoreDispatcher dispatcher = item.Value;
                if (dispatcher == null) continue;

                if (dispatcher.HasThreadAccess)
                    item.Key(this, args);
                else
                {
                    KeyValuePair<NotifyCollectionChangedEventHandler, CoreDispatcher> item1 = item;
                    await dispatcher.RunAsync(CoreDispatcherPriority.Normal, () => item1.Key(this, args));
                }
            }
            OnPropertyChanged("Count");
        }

        int IList.Add(object value)
        {
            Add((T)value);
            return Count - 1;
        }

        void IList.Clear()
        {
            Clear();
        }

        bool IList.Contains(object value)
        {
            return Contains((T)value);
        }

        int IList.IndexOf(object value)
        {
            if (value is T)
                return IndexOf((T)value);
            return -1;
        }

        void IList.Insert(int index, object value)
        {
            Insert(index, (T)value);
        }

        bool IList.IsFixedSize
        {
            get { return false; }
        }

        bool IList.IsReadOnly
        {
            get { return false; }
        }

        void IList.Remove(object value)
        {
            Remove((T)value);
        }

        void IList.RemoveAt(int index)
        {
            RemoveAt(index);
        }

        object IList.this[int index]
        {
            get
            {
                return this[index];
            }
            set
            {
                this[index] = (T)value;
            }
        }

        void ICollection.CopyTo(Array array, int index)
        {
            CopyTo((T[])array, index);
        }

        int ICollection.Count
        {
            get { return Count; }
        }

        bool ICollection.IsSynchronized
        {
            get { return false; }
        }

        object ICollection.SyncRoot
        {
            get { throw new NotImplementedException(); }
        }
    }
}
