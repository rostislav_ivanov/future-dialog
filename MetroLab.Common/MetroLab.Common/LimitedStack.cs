﻿using System;
using System.Collections.Generic;

namespace MetroLab.Common
{
    public class LimitedStack<T>
    {
        public class GotOutItemRemovedArgs : EventArgs
        {
           public T Removeditem {get; private set;}
           public GotOutItemRemovedArgs(T removeditem)
           {
               Removeditem = removeditem;
           }
        }

        public delegate void GotOutItemRemovedHandler(object sender, GotOutItemRemovedArgs args);

        public event GotOutItemRemovedHandler GotOutItemRemoved;

        private void OnGotOutItemRemoved(T removedItem)
        {
            if(GotOutItemRemoved != null)
            {
                GotOutItemRemoved(this, new GotOutItemRemovedArgs(removedItem));
            }
        }
        
        private readonly List<T> _innerList = new List<T>();

        public int Limit { get; private set; }

        public LimitedStack(int limit)
        {
            if (limit < 0)
                throw new ArgumentOutOfRangeException("limit");
            Limit = limit;
        }

        public void Clear()
        {
            _innerList.Clear();
        }

        public int Count
        {
            get { return _innerList.Count; }
        }

        public T Peek()
        {
            if(_innerList.Count == 0)
                throw new ArgumentOutOfRangeException();
            var lastItemIndex = _innerList.Count - 1;
            return _innerList[lastItemIndex];
        }

        public T Pop()
        {
            if (_innerList.Count == 0)
                throw new ArgumentOutOfRangeException();

            var lastItemIndex = _innerList.Count - 1;
            var topItem = _innerList[lastItemIndex];
            _innerList.RemoveAt(lastItemIndex);
            return topItem;
        }

        public void Push(T item)
        {
            _innerList.Add(item);
            if (_innerList.Count > Limit)
            {
                var removedItem = _innerList[0];
                _innerList.RemoveAt(0);
                OnGotOutItemRemoved(removedItem);
            }
        }
    }
}
