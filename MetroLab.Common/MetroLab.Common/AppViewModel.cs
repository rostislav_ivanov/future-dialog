﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using System.Windows.Input;
using Windows.ApplicationModel.Core;
using Windows.ApplicationModel.DataTransfer;
using Windows.Networking.Connectivity;
using Windows.UI.Core;

namespace MetroLab.Common
{
    public class AppViewModel : BindableBase
    {
        public CoreDispatcher Dispatcher { get; private set; }

        private static readonly Dictionary<CoreDispatcher, AppViewModel> AppViewModels = new Dictionary<CoreDispatcher, AppViewModel>();

        public static AppViewModel Current
        {
            get { return GetAppViewModelForWindow(CoreWindow.GetForCurrentThread() ?? CoreApplication.MainView.CoreWindow); }
        }

        public static async Task<AppViewModel> GetMainAsync()
        {
            var window = CoreApplication.MainView.CoreWindow;
            AppViewModel appViewModel = null;
            await window.Dispatcher.RunAsync(CoreDispatcherPriority.Normal, () =>
            {
                lock (AppViewModels)
                {
                    if (!AppViewModels.TryGetValue(window.Dispatcher, out appViewModel))
                    {
                        appViewModel = new AppViewModel();
                        AppViewModels.Add(window.Dispatcher, appViewModel);
                        appViewModel.Dispatcher = window.Dispatcher;
                    }
                }
            });
            return appViewModel;
        }

        public MvvmFrame MvvmFrame { get; set; }

        public event EventHandler<string> NewLogEntry;

        public virtual void RaiseNewLogEntry(string e)
        {
            EventHandler<string> handler = NewLogEntry;
            if (handler != null) handler(this, e);
        }

        private static AppViewModel GetAppViewModelForWindow(CoreWindow window)
        {
            if (window == null) return null;
            lock (AppViewModels)
            {
                AppViewModel appViewModel;
                if (!AppViewModels.TryGetValue(window.Dispatcher, out appViewModel))
                {
                    appViewModel = new AppViewModel();
                    AppViewModels.Add(window.Dispatcher, appViewModel);
                    appViewModel.Dispatcher = window.Dispatcher;
                }
                return appViewModel;
            }
        }

        public string NoInternetMessage { get { return CommonLocalizedResources.GetLocalizedString("exceptionMsg_ServerNotRespondOrConnectionFailure"); } }
        public string NoInternetHeader { get { return CommonLocalizedResources.GetLocalizedString("exceptionHeader_ServerNotRespondOrConnectionFailure"); } }

        public string LoadFailedMessage { get { return CommonLocalizedResources.GetLocalizedString("exceptionMsg_LoadFailed"); } }
        public string LoadFailedHeader { get { return CommonLocalizedResources.GetLocalizedString("exceptionHeader_LoadFailed"); } }

        private AppViewModel()
        {
            CurrentDataTransferManager = DataTransferManager.GetForCurrentView();
            CurrentDataTransferManager.DataRequested += CurrentDataTransferManagerDataRequested;
            NetworkInformation.NetworkStatusChanged += NetworkInformationOnNetworkStatusChanged;
            UpdateGlobalIsWorkingOffline();
        }
        
        async void CurrentDataTransferManagerDataRequested(DataTransferManager sender, DataRequestedEventArgs args)
        {
            var defferal = args.Request.GetDeferral();
            var mvvmPage = MvvmFrame.Content as MvvmPage;
            if (mvvmPage != null)
            {
                var pageViewModel = mvvmPage.ViewModel;
                if (pageViewModel != null)
                    await pageViewModel.RequestData(sender, args);
            }

            defferal.Complete();
        }
        
        private void NetworkInformationOnNetworkStatusChanged(object sender)
        {
            UpdateGlobalIsWorkingOffline();
        }

        public static bool GetIsConnectedToNetwork()
        {
            var profile = NetworkInformation.GetInternetConnectionProfile();
            if (profile != null)
            {
                var networkConnectivityLevel = profile.GetNetworkConnectivityLevel();
                if (networkConnectivityLevel == NetworkConnectivityLevel.InternetAccess)
                    return true;
            }
            return false;
        }

        public async Task RunAtDispatcherAsync(DispatchedHandler action)
        {
            var dispatcher = Dispatcher;
            if (dispatcher != null && !dispatcher.HasThreadAccess)
                dispatcher.RunAsync(CoreDispatcherPriority.Normal, action);
            else action();
        }

        public static bool IsWorkingOffline { get; set; }

        public static void UpdateGlobalIsWorkingOffline()
        {
            if (GetIsConnectedToNetwork())
            {
                if (!IsWorkingOffline) return;
                IsWorkingOffline = false;
                OnInternetConnected();
            }
            else
            {
                if (IsWorkingOffline) return;
                IsWorkingOffline = true;
                OnInternetDisconnected();
            }
        }

        public static event EventHandler InternetConnected;

        protected static void OnInternetConnected()
        {
            EventHandler handler = InternetConnected;
            if (handler != null) handler(null, EventArgs.Empty);            
        }

        protected static event EventHandler InternetDisconnected;

        protected static void OnInternetDisconnected()
        {
            EventHandler handler = InternetDisconnected;
            if (handler != null) handler(null, EventArgs.Empty);
        }

        public event EventHandler GoBackEvent;

        protected virtual void FireGoBackEvent()
        {
            EventHandler handler = GoBackEvent;
            if (handler != null) handler(this, EventArgs.Empty);
        }

        public Task NavigateToViewModel(IPageViewModel pageViewModel)
        {
            lock (this)
            {
                return MvvmFrame.Navigate(pageViewModel);
            }
        }

        public void ClearNavigationStack()
        {
            MvvmFrame.ClearNavigationStack();
        }
        
        public ICommand GoBackCommand
        {
            get
            { 
                var command = new ActionCommand { IsCanExecute = MvvmFrame.CanGoBack };
                command.ExecuteAction += o => GoBack();
                return command;
            }
        }

        public bool CanGoBack
        {
            get { return MvvmFrame.CanGoBack; }
        }
        
        public Task<IPageViewModel> GoBack()
        {
            return MvvmFrame.GoBack();
        }

        public Type HomePageViewModelType { get; set; }

        public IPageViewModel GoHome()
        {
            lock (this)
            {
                var startViewModel = (IPageViewModel) Activator.CreateInstance(HomePageViewModelType);
                NavigateToViewModel(startViewModel);
                return startViewModel;
            }
        }
        
        private static DataTransferManager CurrentDataTransferManager { get; set; }
        
    }
}
