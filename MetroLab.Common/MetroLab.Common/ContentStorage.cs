﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Diagnostics.Contracts;
using System.IO;
using System.Runtime.InteropServices.WindowsRuntime;
using System.Threading;
using System.Threading.Tasks;
using Windows.Security.Cryptography.Core;
using Windows.Storage;
using Windows.Storage.Streams;
using Windows.UI.Core;
using Windows.UI.Xaml;

namespace MetroLab.Common
{
    public class ContentStorage : ContentLoader
    {
        protected virtual string BinaryCacheFolderName { get { return "BinaryStorage"; } }

        private class CacheFileInfo
        {
            public ContentStorage CurrentContentStorage { get; private set; }
            public string ServerPath { get; private set; }

            public CacheFileInfo(string serverPath, ContentStorage contentStorage)
            {
                if (serverPath == null)
                    throw new ArgumentNullException("serverPath");
                ServerPath = serverPath;
                CurrentContentStorage = contentStorage;
            }

            private Uri _serverUri;
            public Uri ServerUri
            {
                get { return _serverUri ?? (_serverUri = new Uri(ServerPath)); }
            }

            private string _localFileName;
            public string LocalFileName
            {
                get { return _localFileName ?? (_localFileName = GetCachedFileName(ServerPath)); }
            }

            private Uri _localUri;
            public Uri LocalUri
            {
                get
                {
                    return _localUri ?? (_localUri = new Uri(CurrentContentStorage.GetCachedFilePath(LocalFileName)));
                }
            }

            public bool IsAlwaysUpdate { get; set; }
            public override int GetHashCode() { return ServerPath.GetHashCode(); }
            public override string ToString() { return ServerPath; }

            public override bool Equals(object obj)
            {
                var other = obj as CacheFileInfo;
                if (other == null) return false;
                return ServerPath == other.ServerPath;
            }
        }

        private static volatile ContentStorage _current = new ContentStorage();

        public static ContentStorage Current
        {
            get { return _current; }
        }
        
        public static string GetSourceUrl(DependencyObject frameworkControl) { return (string)frameworkControl.GetValue(SourceUrlProperty); }
        public static void SetSourceUrl(DependencyObject frameworkControl, string value) { frameworkControl.SetValue(SourceUrlProperty, value); }

        public static readonly DependencyProperty SourceUrlProperty = DependencyProperty.RegisterAttached(
            "SourceUrl", typeof(string), typeof(ContentStorage), new PropertyMetadata(null, Current.OnSourceUriPropertyChanged));
        
#if DEBUG
        private static TimeSpan _totalTime = TimeSpan.Zero;
#endif

        protected async void OnSourceUriPropertyChanged(DependencyObject o, DependencyPropertyChangedEventArgs e)
        {
#if DEBUG
            var start = DateTime.Now;
#endif
            var newServerPath = (string)e.NewValue;

            if (newServerPath != null && !IsServerPathCorrect(newServerPath))
                throw new InvalidDataException("SourceUri is not correct");

            var element = (FrameworkElement)o;
            if (GetRemoveOldContentWhenLoading(element))
            {
                var elementCacheHelper = GetHelper(element);
                elementCacheHelper.RemoveContent();
            }

            element.Unloaded -= ElementOnUnloaded;
            element.Unloaded += ElementOnUnloaded;

            await RemoveAllQueuedElements();
            await RemoveElement(element);
            _elementsQueue.Enqueue(element, newServerPath);
            Log.Info("Ссылка {0} добавлена в очередь на скачивание (сработало SourceUriPropertyChanged)", newServerPath);

#if DEBUG
            var timeDelta = DateTime.Now - start;
            _totalTime += timeDelta;
#endif
        }

        private readonly object UriesAndElementsLocker = new object();
        private readonly Dictionary<CacheFileInfo, List<object>> UriesAndObjects =
            new Dictionary<CacheFileInfo, List<object>>();

        private readonly object ObjectsLocker = new object();
        private readonly Dictionary<object, CacheFileInfo> Objects =
            new Dictionary<object, CacheFileInfo>();

        protected void ElementOnUnloaded(object sender, RoutedEventArgs routedEventArgs)
        {
            var element = ((FrameworkElement)sender);
            element.Loaded += ElementOnLoaded;
            element.Unloaded -= ElementOnUnloaded;
            lock (PendingUnloadedElementsQueueLocker)
                PendingUnloadedElementsQueue.Add(element);
        }

        private async void ElementOnLoaded(object sender, RoutedEventArgs routedEventArgs)
        {
            var element = ((FrameworkElement)sender);
            element.Loaded -= ElementOnLoaded;
            var helper = GetHelper(element);
            if (!(await helper.IsSourceEmptyAsync())) return;
            await RemoveAllQueuedElements();
            await RemoveElement(element);
            var sourceUrl = GetSourceUrl(element);
            _elementsQueue.Enqueue(element, sourceUrl);

            Log.Info("Ссылка {0} добавлена в очередь на скачивание (ElementOnLoaded called)", sourceUrl);
        }

        private readonly object PendingUnloadedElementsQueueLocker = new object();
        private List<object> _pendingUnloadedElementsQueue = new List<object>();
        private List<object> PendingUnloadedElementsQueue
        {
            get { return _pendingUnloadedElementsQueue; }
        }

        protected async Task RemoveAllQueuedElements()
        {
            List<object> elements;
            lock (PendingUnloadedElementsQueueLocker)
            {
                elements = PendingUnloadedElementsQueue;
                _pendingUnloadedElementsQueue = new List<object>();
            }
            if (elements == null) return;
            foreach (var element in elements)
            {
                await RemoveElement(element);
            }
        }

        private async Task RemoveElement(object element)
        {
            await Initialize();
            _elementsQueue.Remove(element);
            CacheFileInfo serverCacheFileInfo;
            lock (ObjectsLocker)
            {
                if (!Objects.ContainsKey(element))
                    return;
                serverCacheFileInfo = Objects[element];
                Objects.Remove(element);
            }
            lock (UriesAndElementsLocker)
            {
                if (UriesAndObjects.ContainsKey(serverCacheFileInfo))
                {
                    UriesAndObjects[serverCacheFileInfo].Remove(element);
                    if (UriesAndObjects[serverCacheFileInfo].Count == 0)
                    {
                        UriesAndObjects.Remove(serverCacheFileInfo);
                    }
                }
            }
        }

        private static bool IsServerPathCorrect(string serverPath)
        {
            if (serverPath == null) throw new ArgumentNullException("serverPath");
            return serverPath.StartsWith("http://") || serverPath.StartsWith("https://") || serverPath.StartsWith("lazylens://");
        }

        private Task<bool> SetCachedSource(object element, CacheFileInfo cacheFileInfo)
        {
            if (cacheFileInfo.LocalUri == null)
                throw new ArgumentNullException("localUri");

            var task = new TaskCompletionSource<bool>();

            Action<object> action = async target =>
            {
                try
                {
                    var helper = GetHelper(element);
                    if (cacheFileInfo.IsAlwaysUpdate)
                        helper.DoNotUseCache = true;
                    await helper.SetSourceAsync(cacheFileInfo.LocalUri);
                    task.TrySetResult(true);
                }
                catch (FileNotFoundException)
                {
                    ReportCacheForFileIsBroken(cacheFileInfo);
                    _elementsQueue.Enqueue(element, cacheFileInfo.ServerPath);
                    task.TrySetResult(false);
                }
                catch (Exception e)
                {
#if DEBUG
                    if (Debugger.IsAttached)
                        Debugger.Break();
                    //press F5
#endif
                    ReportCacheForFileIsBroken(cacheFileInfo);
                    _elementsQueue.Enqueue(element, cacheFileInfo.ServerPath);
                    task.TrySetResult(false);
                }
            };

            var frameworkElement = element as FrameworkElement;
            if (frameworkElement != null)
            {
                frameworkElement.Dispatcher.RunAsync(CoreDispatcherPriority.Low, () => action(element));
            }
            else
            {
                action(element);
            }

            return task.Task;
        }

        private readonly object CachedFilesLocker = new object();
        private readonly HashSet<string> CachedFiles = new HashSet<string>();

        public void ReportCachedFilesDeleted()
        {
            lock (CachedFilesLocker)
                CachedFiles.Clear();
            lock (SystemImageCacheHelper.Cache)
                SystemImageCacheHelper.Cache.Clear();
        }

        private bool IsFileCached(CacheFileInfo cacheFileInfo)
        {
            lock (CachedFilesLocker)
                return CachedFiles.Contains(cacheFileInfo.LocalFileName);
        }

        public bool IsFileCached(string serverPath)
        {
            if (serverPath == null)
                throw new ArgumentNullException("serverPath");
            //if (!IsServerPathCorrect(serverPath))
            //    throw new ArgumentException("serverPath is not correct", "serverPath");

            return Current.IsFileCached(new CacheFileInfo(serverPath, Current));
        }

        public class FileJustCachedEventArgs : EventArgs
        {
            public string ServerPath { get; private set; }

            internal FileJustCachedEventArgs(string serverPath)
            {
                ServerPath = serverPath;
            }
        }

        public event EventHandler<FileJustCachedEventArgs> FileJustCached;

        protected virtual void OnFileJustCached(FileJustCachedEventArgs e)
        {
            EventHandler<FileJustCachedEventArgs> handler = FileJustCached;
            if (handler != null) handler(this, e);
        }

        private async Task ReportFileJustCachedAsync(CacheFileInfo cacheFileInfo)
        {
            if (!CachedFiles.Contains(cacheFileInfo.LocalFileName))
            {
                CachedFiles.Add(cacheFileInfo.LocalFileName);
                OnFileJustCached(new FileJustCachedEventArgs(cacheFileInfo.ServerPath));
            }
            await SetJustCachedFileAsSourceOfAllExpectantElementsAsync(cacheFileInfo);
        }

        private async Task SetJustCachedFileAsSourceOfAllExpectantElementsAsync(CacheFileInfo cacheFileInfo)
        {
            List<object> elements = null;

            if (cacheFileInfo.LocalUri != null)
            {
                lock (UriesAndElementsLocker)
                {
                    if (UriesAndObjects.ContainsKey(cacheFileInfo))
                    {
                        elements = UriesAndObjects[cacheFileInfo];
                        UriesAndObjects.Remove(cacheFileInfo);
                    }
                }
                if (elements != null)
                    foreach (var element in elements)
                    {
                        await SetCachedSource(element, cacheFileInfo);
                        lock (ObjectsLocker)
                            Objects.Remove(element);
                    }
            }
        }

        private void ReportCacheForFileIsBroken(CacheFileInfo cacheFileInfo)
        {
            lock (CachedFilesLocker)
                CachedFiles.Remove(cacheFileInfo.LocalFileName);
        }

        private async Task<string> UpdateCachedFileAsync(Stream streamOfFileToCache, CacheFileInfo cacheFileInfo)
        {
            try
            {
                var file = await _cacheFolder.CreateFileAsync(cacheFileInfo.LocalFileName,
                    CreationCollisionOption.ReplaceExisting);

                using (var transactedWrite = await file.OpenTransactedWriteAsync())
                {
                    using (var streamForRead = streamOfFileToCache)
                    {
                        var outputStream = transactedWrite.Stream;
                        var streamForWrite = outputStream.AsStreamForWrite();
                        streamForRead.CopyTo(streamForWrite);
                        await streamForWrite.FlushAsync();
                        await outputStream.FlushAsync();
                        await transactedWrite.CommitAsync();
                    }
                }

                await ReportFileJustCachedAsync(cacheFileInfo);
                lock (SystemImageCacheHelper.Cache)
                    SystemImageCacheHelper.Cache.Remove(cacheFileInfo.LocalUri);
            }
            catch (Exception ex)
            {
                if (Debugger.IsAttached) Debugger.Break();
            }

            return cacheFileInfo.ServerPath;
        }

        private readonly SemaphoreSlim _semaphore = new SemaphoreSlim(1);

        private async Task<string> UpdateCachedFileAsync(IRandomAccessStream streamOfFileToCache, CacheFileInfo cacheFileInfo)
        {
            try
            {
                await _semaphore.WaitAsync();
                var file = await _cacheFolder.CreateFileAsync(cacheFileInfo.LocalFileName,
                    CreationCollisionOption.ReplaceExisting);

                using (var transactedWrite = await file.OpenTransactedWriteAsync())
                {
                    using (var streamForRead = streamOfFileToCache.AsStreamForRead())
                    {
                        var outputStream = transactedWrite.Stream;
                        var streamForWrite = outputStream.AsStreamForWrite();
                        streamForRead.CopyTo(streamForWrite);
                        await streamForWrite.FlushAsync();
                        await outputStream.FlushAsync();
                        await transactedWrite.CommitAsync();
                    }
                }

                await ReportFileJustCachedAsync(cacheFileInfo);
                lock (SystemImageCacheHelper.Cache)
                    SystemImageCacheHelper.Cache.Remove(cacheFileInfo.LocalUri);
            }
            catch (Exception ex)
            {
                if (Debugger.IsAttached) Debugger.Break();
            }
            finally
            {
                _semaphore.Release();
            }

            return cacheFileInfo.ServerPath;
        }

        public async Task<string> AddFileToCacheAsync(Stream streamOfFileToCache, string filePath)
        {
            var cacheFileInfo = new CacheFileInfo(filePath, this);
            if (IsFileCached(cacheFileInfo)) return cacheFileInfo.ServerPath;

            return await UpdateCachedFileAsync(streamOfFileToCache, cacheFileInfo);
        }

        public async Task<string> AddFileToCacheAsync(IRandomAccessStream streamOfFileToCache, string filePath)
        {
            var cacheFileInfo = new CacheFileInfo(filePath, this);
            if (IsFileCached(cacheFileInfo)) return cacheFileInfo.ServerPath;

            return await UpdateCachedFileAsync(streamOfFileToCache, cacheFileInfo);
        }

        public async Task<string> UpdateCachedFileAsync(Stream streamOfFileToCache, string filePath)
        {
            var cacheFileInfo = new CacheFileInfo(filePath, this);
            return await UpdateCachedFileAsync(streamOfFileToCache, cacheFileInfo);
        }

        private StorageFolder _cacheFolder;

        //<ui element, server file path>
        private KeyValuePairsQueue<object, string> _elementsQueue = new KeyValuePairsQueue<object, string>();

        private readonly object InitializeLocker = new object();

        private volatile Task _initializeTask;
        public Task Initialize()
        {
            if (_initializeTask == null)
                lock (InitializeLocker)
                {
                    if (_initializeTask == null)
                        return (_initializeTask = InitializeInner());
                }
            return _initializeTask;
        }

        private async Task InitializeInner()
        {
            _cacheFolder = await ApplicationData.Current.LocalFolder.CreateFolderAsync(
                   BinaryCacheFolderName, CreationCollisionOption.OpenIfExists);
            var files = await _cacheFolder.GetFilesAsync();
            foreach (var storageFile in files)
            {
                CachedFiles.Add(storageFile.Name);
            }

            _elementsQueue = new KeyValuePairsQueue<object, string>();
            _elementsQueue.ProcessItem += ElementsQueue_ProcessItem;
            _elementsQueue.StartWorker();
        }

        async Task ElementsQueue_ProcessItem(object sender, ProcessItemEventArgs<object, string> args)
        {
            try
            {
                var element = args.Key;
                var newServerPath = args.Value;
                if (String.IsNullOrEmpty(newServerPath)) return;

                var newServerPathCacheInfo = new CacheFileInfo(newServerPath, this);
                
                if (IsFileCached(newServerPathCacheInfo))
                {
                    await SetCachedSource(element, newServerPathCacheInfo);
                    if (!newServerPathCacheInfo.IsAlwaysUpdate)
                        return;
                }

                lock (ObjectsLocker)
                {
                    if (!Objects.ContainsKey(element))
                        Objects.Add(element, newServerPathCacheInfo);
                }
                lock (UriesAndElementsLocker)
                {
                    if (!UriesAndObjects.ContainsKey(newServerPathCacheInfo))
                    {
                        UriesAndObjects.Add(newServerPathCacheInfo, new List<object> { element });
                    }
                    else if (!UriesAndObjects[newServerPathCacheInfo].Contains(element))
                    {
                        UriesAndObjects[newServerPathCacheInfo].Add(element);
                    }
                }
            }
            catch (Exception)
            {
                if (Debugger.IsAttached)
                    Debugger.Break();
            }
        }

        /// <summary>
        /// Return Uri to cached file or first download file and then return Uri
        /// </summary>
        /// <param name="serverPath"></param>
        /// <param name="getOnlyFromCache"></param>
        /// <param name="progressValueCallback"></param>
        /// <param name="progressTextCallback"></param>
        public async Task<Uri> GetFileByServerPathAsync(string serverPath, bool getOnlyFromCache = false,
            Action<double> progressValueCallback = null, Action<string> progressTextCallback = null)
        {
            if (serverPath == null)
                throw new ArgumentNullException("serverPath");
            if (!IsServerPathCorrect(serverPath))
                throw new ArgumentException("serverPath is not correct", "serverPath");

            var cacheFileInfo = new CacheFileInfo(serverPath, this);

            if (!IsFileCached(cacheFileInfo))
            {
                if (getOnlyFromCache) return null;

                var taskCompletionSource = progressValueCallback == null
                    ? new TaskCompletionSource<Uri>()
                    : new TaskCompletionSourceWithCallbacks<Uri>
                    {
                        ProgressValueCallback = progressValueCallback,
                        LoadingTextCallback = progressTextCallback
                    };

                _elementsQueue.Enqueue(taskCompletionSource, serverPath);

                var localUri = await taskCompletionSource.Task;
                return localUri;
            }
            return cacheFileInfo.LocalUri;
        }

        public async Task<StorageFile> GetCachedFileOrNullAsync(string serverPath)
        {
            if (serverPath == null)
                throw new ArgumentNullException("serverPath");
            var localFileName = GetCachedFileName(serverPath);

            lock (CachedFilesLocker)
            {
                if (!CachedFiles.Contains(localFileName)) return null;
            }

            var cacheFolder = await ApplicationData.Current.LocalFolder.CreateFolderAsync(
                BinaryCacheFolderName, CreationCollisionOption.OpenIfExists);

            return await cacheFolder.GetFileAsync(localFileName);
        }

        private string GetCachedFilePath(string fileName)
        {
            return String.Format("ms-appdata:///local/{0}/{1}", BinaryCacheFolderName, fileName);
        }
        
        public async Task RemoveOutdatedFilesAsync(DateTime cutOffDate)
        {
            try
            {
                var cacheFolder = await ApplicationData.Current.LocalFolder.CreateFolderAsync(
                    BinaryCacheFolderName, CreationCollisionOption.OpenIfExists);
                foreach (var file in await cacheFolder.GetFilesAsync())
                {
                    if (file.DateCreated < cutOffDate)
                        await file.DeleteAsync();
                }
            }
            catch (Exception e)
            {
                Log.Error("RemoveOutdatedFilesAsync failed", e);
                if (Debugger.IsAttached)
                    Debugger.Break();
            }
        }

        public async Task DeleteFromCacheAsync(string serverPath)
        {
            if (serverPath == null)
                throw new ArgumentNullException("serverPath");
            if (!IsServerPathCorrect(serverPath))
                throw new ArgumentException("serverPath is not correct", "serverPath");
            var cacheFileInfo = new CacheFileInfo(serverPath, this);
            if (!IsFileCached(cacheFileInfo)) return;

            try
            {
                var file = await _cacheFolder.GetFileAsync(cacheFileInfo.LocalFileName);
                await file.DeleteAsync(StorageDeleteOption.Default);

                lock (CachedFilesLocker)
                    CachedFiles.Remove(cacheFileInfo.LocalFileName);
            }
            catch (Exception ex)
            {
                if (Debugger.IsAttached) Debugger.Break();
            }
        }
        
        public async Task ClearCachedFiles()
        {
            try
            {
                await ApplicationData.Current.LocalFolder.CreateFolderAsync(
                    BinaryCacheFolderName, CreationCollisionOption.ReplaceExisting);
            }
            catch (Exception e)
            {
                if (Debugger.IsAttached)
                    Debugger.Break();
            }
        }
    }
}
