﻿using System;
using Windows.System;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Documents;

namespace MetroLab.Common
{
    public class TapNavigation
    {
        public static readonly DependencyProperty NavigateUriProperty = DependencyProperty.RegisterAttached(
            "NavigateUri", typeof(string), typeof(TapNavigation), new PropertyMetadata(null, OnNavigateUriPropertyChanged));

        public static string GetNavigateUri(DependencyObject frameworkControl) { return (string)frameworkControl.GetValue(NavigateUriProperty); }
        public static void SetNavigateUri(DependencyObject frameworkControl, string value) { frameworkControl.SetValue(NavigateUriProperty, value); }

        
        private static void OnNavigateUriPropertyChanged(DependencyObject d, DependencyPropertyChangedEventArgs args)
        {
            var hyperlink = (Hyperlink)d;
            hyperlink.Click -= HyperlinkOnClick;
            if (args.NewValue != null)
                hyperlink.Click += HyperlinkOnClick;

        }

        private static void HyperlinkOnClick(Hyperlink sender, HyperlinkClickEventArgs args)
        {
            var navigationUri = GetNavigateUri(sender);
            if (string.IsNullOrEmpty(navigationUri)) return;
            Launcher.LaunchUriAsync(new Uri(navigationUri, UriKind.RelativeOrAbsolute));
        }
    }
}
