using System;

namespace MetroLab.Common
{
    [Flags]
    public enum CacheState
    {
        /// <summary>
        /// No cache for data
        /// </summary>
        Empty = 0, //0000

        /// <summary>
        /// Cache is available but may be outdated
        /// </summary>
        Available = 0x01, //0001
        
        /// <summary>
        /// Cache is in actual state
        /// </summary>
        Actual = 0x07, //0111

        /// <summary>
        /// Cache is not required
        /// </summary>
        ActualOrDispensable = 0x04, //0100
    }

    public static class CacheStateExtensions
    {
        public static CacheState Combine(this CacheState firstObjectState, CacheState secondObjectState)
        {
            return firstObjectState & secondObjectState;
        }
    }
}