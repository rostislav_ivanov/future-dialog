﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MetroLab.Common
{
    public class CollectionUtil
    {
        public static void UpdateCollection<T>(ref ObservableCollection<T> targetCollection, ObservableCollection<T> nextCollection)
        {
            if (targetCollection == null)
            {
                targetCollection = nextCollection;
                return;
            }

            int prevItemPosition = -1;
            foreach (var item in nextCollection)
            {
                var itemPosition = targetCollection.IndexOf(item);
                if (itemPosition < 0)
                {
                    prevItemPosition++;
                    if (prevItemPosition < targetCollection.Count)
                        targetCollection.Insert(prevItemPosition, item);
                    else
                        targetCollection.Add(item);
                }
                else
                {
                    prevItemPosition++;
                    for (int i = prevItemPosition; i < itemPosition; i++)
                        targetCollection.RemoveAt(prevItemPosition);
                }
            }

            while (targetCollection.Count > prevItemPosition + 1)
                targetCollection.RemoveAt(targetCollection.Count - 1);
        }
        
        public static void UpdateCollection<T>(ref MTObservableCollection<T> targetCollection, MTObservableCollection<T> nextCollection)
        {
            if (targetCollection == null)
            {
                targetCollection = nextCollection;
                return;
            }

            int prevItemPosition = -1;
            foreach (var item in nextCollection)
            {
                var itemPosition = targetCollection.IndexOf(item);
                if (itemPosition < 0)
                {
                    prevItemPosition++;
                    if (prevItemPosition < targetCollection.Count)
                        targetCollection.Insert(prevItemPosition, item);
                    else
                        targetCollection.Add(item);
                }
                else
                {
                    prevItemPosition++;
                    for (int i = prevItemPosition; i < itemPosition; i++)
                        targetCollection.RemoveAt(prevItemPosition);
                }
            }

            while (targetCollection.Count > prevItemPosition + 1)
                targetCollection.RemoveAt(targetCollection.Count - 1);
        }
    }
}
