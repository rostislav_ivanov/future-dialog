﻿using System;

namespace TheBoard.ViewModel.Facebook
{
    public class FacebookAppReturnInvalidDataException: Exception
    {
        public FacebookAppReturnInvalidDataException(string message) : base(message) { }
    }
}