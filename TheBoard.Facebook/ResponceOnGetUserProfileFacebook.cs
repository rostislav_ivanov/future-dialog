﻿using System.Runtime.Serialization;

namespace TheBoard.ViewModel.Facebook
{
    [DataContract]
    public class ResponceOnGetUserProfileFacebook
    {
        [DataMember(Name = "email")]
        public string Email { get; set; }
    }
}

