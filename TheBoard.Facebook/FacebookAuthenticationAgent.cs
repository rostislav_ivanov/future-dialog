﻿using System;
using System.IO;
using System.Runtime.InteropServices.WindowsRuntime;
using System.Runtime.Serialization.Json;
using System.Threading.Tasks;
using Windows.ApplicationModel.Activation;
using Windows.Security.Authentication.Web;
using Windows.System;
using Windows.Web.Http;

namespace TheBoard.ViewModel.Facebook
{
    public class FacebookAuthenticationAgent
    {
        public readonly static FacebookAuthenticationAgent Current = new FacebookAuthenticationAgent();

        private const string AccessTokenKey = "access_token";
        private const string ErrorCodeKey = "error_code";
        private const string ErrorDescriptionKey = "error_description";
        private const string ErrorKey = "error";
        private const string ErrorReasonKey = "error_reason";
        private const string ExpiresInKey = "expires_in";
        private const string RedirectUri = "msft-fdccf88ea8c54847b4a2cd0868803883";
        private const string FacebookConnectUriTemplate = "fbconnect://authorize?client_id={0}&scope={1}&redirect_uri={2}";

        //public const string FacebookClientId = "566235876818089"; //VPN Unlimited
        public const string FacebookClientId = "769518249832709";
        //public const string FacebookСlientSecret = "c5436b2b98b3f480b2aca007a2e266d2"; //VPN Unlimited
        public const string FacebookСlientSecret = "9d33ee258b92308856540aa1fb41cb68";

        public async Task<CredentialsFacebook> AuthenticationFromAppReceivedAsync(string queryString)
        {
            if (String.IsNullOrWhiteSpace(queryString))
            {
                throw new FacebookAppReturnInvalidDataException("");
            }

            var error = GetQueryStringValueFromUri(queryString, ErrorKey);
            var errorDescription = GetQueryStringValueFromUri(queryString, ErrorDescriptionKey);
            var errorReason = GetQueryStringValueFromUri(queryString, ErrorReasonKey);

            int errorCodeValue = 0;
            Int32.TryParse(GetQueryStringValueFromUri(queryString, ErrorCodeKey), out errorCodeValue);

            if (String.IsNullOrEmpty(error) && errorCodeValue == 0)
            {
                CredentialsFacebook credentials = new CredentialsFacebook();
                credentials.AccessToken = GetQueryStringValueFromUri(queryString, AccessTokenKey);

                long expiresInValue;
                Int64.TryParse(GetQueryStringValueFromUri(queryString, ExpiresInKey), out expiresInValue);
                credentials.ExpirationDate = expiresInValue;

                var now = DateTime.UtcNow;
                credentials.RealExpirationDate = now + TimeSpan.FromSeconds(expiresInValue);

                return credentials;
            }
            else
            {
                throw new FacebookOAuthException(String.Format("{0}: {1}", error, errorDescription));
            }
        }

        const string FacebookCallbackUrl = "https://m.facebook.com/connect/login_success.html";

        public static string GetFacebookUrl()
        {
            return "https://www.facebook.com/dialog/oauth?client_id=" + Uri.EscapeDataString(FacebookClientId) +
            "&redirect_uri=" + Uri.EscapeDataString(FacebookCallbackUrl) + "&scope=" + CredentialsFacebook.Scope +
            "&display=popup&response_type=token";
        }
        public static string GetFacebookUrlForIE()
        {
            return "https://m.facebook.com/v2.4/dialog/oauth?client_id=" + Uri.EscapeDataString(FacebookClientId) +
            "&redirect_uri=" + RedirectUri + "%3A%2F%2Fauthorize" + "&scope=" + CredentialsFacebook.Scope +
            "&display=touch&type=user_agent";
        }
        public static async Task<bool> AuthenticateWithApp()
        {
            bool result;
            try
            {
                var options = new Windows.System.LauncherOptions();
                options.FallbackUri = new Uri(GetFacebookUrlForIE());
                result = await Launcher.LaunchUriAsync(new Uri(String.Format(FacebookConnectUriTemplate, FacebookClientId,
                    CredentialsFacebook.Scope, RedirectUri + "://authorize")), options);
            }
            catch
            {
               return false;
            }
            return result;
        }

        private static string GetQueryStringValueFromUri(string uri, string key)
        {
            int questionMarkIndex = 0;
            if (uri.Contains("?"))
            {
                questionMarkIndex = uri.LastIndexOf("?", StringComparison.Ordinal);
            }
            else if (uri.Contains("#"))
            {
                questionMarkIndex = uri.LastIndexOf("#", StringComparison.Ordinal);
            }

            if (questionMarkIndex == -1)
            {
                // if no questionmark found, this is just a queryString
                questionMarkIndex = 0;
            }

            string queryString = uri.Substring(questionMarkIndex, uri.Length - questionMarkIndex);
            queryString = queryString.Replace("#", String.Empty).Replace("?", String.Empty);

            string[] keyValuePairs = queryString.Split(new[] { '&' });
            for (int i = 0; i < keyValuePairs.Length; i++)
            {
                string[] pair = keyValuePairs[i].Split(new[] { '=' });
                if (pair[0].ToLowerInvariant() == key.ToLowerInvariant())
                {
                    return Uri.UnescapeDataString(pair[1]);
                }
            }

            return String.Empty;
        }

        public async Task<CredentialsFacebook> LoginAsync()
        {
            var startUri = new Uri(GetFacebookUrl());
            var endUri = new Uri(FacebookCallbackUrl);

            WebAuthenticationBroker.AuthenticateAndContinue(startUri, endUri, null, WebAuthenticationOptions.None);
            return null;
        }
        
        private void GetKeyValues(string webAuthResultResponseData, out string accessToken, out string expiresIn)
        {
            string responseData = webAuthResultResponseData.Substring(webAuthResultResponseData.IndexOf("access_token", StringComparison.Ordinal));
            string[] keyValPairs = responseData.Split('&');
            accessToken = null;
            expiresIn = null;
            for (int i = 0; i < keyValPairs.Length; i++)
            {
                string[] splits = keyValPairs[i].Split('=');switch (splits[0])
                {
                    case "access_token":
                        accessToken = splits[1];
                        break;
                    case "expires_in":
                        expiresIn = splits[1];
                        break;
                }
            }
        }

        public async Task<CredentialsFacebook> Finalize(WebAuthenticationBrokerContinuationEventArgs args)
        {
            Exception exception = null;
            try
            {
                var result = args.WebAuthenticationResult;

                return await GetSession(result);
            }
            catch (Exception e)
            {
                exception = e;
            }

            return null;
        }

        private async Task<CredentialsFacebook> GetSession(WebAuthenticationResult result)
        {
            if (result.ResponseStatus == WebAuthenticationStatus.Success)
            {
                string accessToken;
                string expiresIn;
                GetKeyValues(result.ResponseData, out accessToken, out expiresIn);

                //string email = await GetEmail(accessToken);

                return new CredentialsFacebook
                {
                    AccessToken = accessToken,
                    ExpirationDate = Int64.Parse(expiresIn)
                   // Email = email
                };
            }
            if (result.ResponseStatus == WebAuthenticationStatus.ErrorHttp)
            {
                throw new Exception("Error http");
            }
            if (result.ResponseStatus == WebAuthenticationStatus.UserCancel)
            {
                throw new Exception("User Canceled.");
            }
            return null;
        }

        /*private static async Task<string> GetEmail(string accessToken)
        {
            const string getEmailUrl = "https://graph.facebook.com/me?fields=email&";

            var request = new HttpRequestMessage(HttpMethod.Get,
                new Uri(String.Concat(getEmailUrl, "access_token=", accessToken), UriKind.Absolute));


            var httpClient = new HttpClient();
            var responseMessage = await httpClient.SendRequestAsync(request);

            responseMessage.EnsureSuccessStatusCode();

            var dataStream = await responseMessage.Content.ReadAsBufferAsync();


            var serializer = new DataContractJsonSerializer(typeof(ResponceOnGetUserProfileFacebook));
            var responceOnGetUserProfileGoogle = (ResponceOnGetUserProfileFacebook)serializer.ReadObject(dataStream.AsStream());

            return responceOnGetUserProfileGoogle.Email;
        }*/
    }
}
