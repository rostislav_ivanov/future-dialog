﻿using System;

namespace TheBoard.ViewModel.Facebook
{
    public class FacebookOAuthException : Exception
    {
        public FacebookOAuthException(string message) : base(message) { }
    }
}
