﻿namespace TheBoard.ViewModel
{
    public interface IRemovable
    {
        void Remove();
    }
}
