﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Runtime.InteropServices.WindowsRuntime;
using System.Runtime.Serialization.Json;
using System.Threading.Tasks;
using Windows.Security.Cryptography;
using Windows.Security.Cryptography.Core;
using Windows.Storage.Streams;
using Windows.System.Profile;
using TheBoard.ViewModel.Model;
using System.Net.Http;
using System.Net;
using Windows.Networking.Sockets;
using Windows.System.UserProfile;
using Windows.Devices.Geolocation;
using Windows.Storage;
using Windows.Foundation;
using Windows.Networking.PushNotifications;
using TheBoard.ViewModel.View.Cards;
using Windows.UI.Xaml;
using System.Reflection;
using Windows.Networking.Connectivity;
using Windows.UI.Notifications;
using NotificationsExtensions.BadgeContent;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System.Text;

namespace TheBoard.ViewModel
{
    public class AppServerAgent
    {
        private readonly HttpClient _httpClient;
        private const int CountLoadingAttempts = 5;
        public static readonly string DeviceId;
        public bool IsUserLoaded;

        //private const string ApiDomain = "http://demo.bind.ee/board/api/";
        //private const string ApiDomain = "http://app.theboard.pro/api/v1/";
        //public static string ServerUrl = "https://dev-app.futuredialog.fi/";

        //public static string ServerUrl = "https://stage-app.futuredialog.fi/";
        public static string ServerUrl = "https://app.futuredialog.fi/";
        private static string ApiDomain = ServerUrl + "api/v2/";
        private const string UrlGetCustomerData = "customer";
        private const string UrlGetCardsData = "cards";
        private const string UrlGetPromotedCardsData = "cards/promoted";
        private const string UrlRegisterDevice = "customer/register_device";
        private const string UrlGetDeeplinkEmail = "customer/auth";
        private const string UrlMarkCardAsSeen = "cards/seen/";
        private const string UrlDeleteCard = "cards/promotion-delete/";

        static AppServerAgent()
        {
            var token = HardwareIdentification.GetPackageSpecificToken(null);
            var hardwareId = token.Id;
            using (var dataReader = DataReader.FromBuffer(hardwareId))
            {
                var bytes = new byte[hardwareId.Length];
                dataReader.ReadBytes(bytes);
                var buffer = CryptographicBuffer.CreateFromByteArray(bytes);

                var md5Provider = HashAlgorithmProvider.OpenAlgorithm(HashAlgorithmNames.Md5);
                var md5Hash = md5Provider.HashData(buffer).ToArray();
                //DeviceId = BitConverter.ToString(md5Hash).Replace("-", "").ToLower();
                DeviceId = "asfafsvvbvvsvsvrbfdbrdb-38";

                ApplicationData.Current.LocalSettings.Values["deviceID"] = DeviceId;
            }
        }

        private AppServerAgent()
        {
            _httpClient = new HttpClient() { BaseAddress = new Uri(ApiDomain) };
            var headers = _httpClient.DefaultRequestHeaders;
            //DEV Espoo:             yOAt7IS3zSWj6HYI5vi4dan1Hwz5PcYH
            //LIVE Espoo:            d0GjMPg7Bo6nyqPREM7bSyo-G_cPdHl2
            //headers.Add("x-board-appid", "dc3s9kLXYWRCFOVOeZyXSNiwYow2P9Y-");
            //headers.Add("x-board-appid", "aQpF_IGaIoMkPf9sXQr4BInEFp_lqXJ6");  //Holola
            //headers.Add("x-board-appid", "rIP5TnHnGvRr2yl7Q4QE0OCtXoj0TmKA");  //Holola Release
            //headers.Add("x-board-appid", "Sw1iQb8AJrv7oh0JxAZ4tdHcvYWGYu13");  //Tamperere3

            //Medialab:              df9lFPDzz6MD448oBnfb4wS8YEKV5ZeJ

            //DEV Yleisurheilu SUL:  yf2fQ5xBnOf7vUOt1tdMdVB3LD_nPciR
            //LIVE Yleisurheilu SUL: dc3s9kLXYWRCFOVOeZyXSNiwYow2P9Y-

            //DEV Lahti(Porukka):    bL7PmoBrmdmJUTmOgX4RTq3e8VjEcZrR
            //LIVE Lahti(Porukka):   17adc2fb962b468417eee74ef8c49e5a

            //DEV Tampere:           qbcbyjJ4FmgAoDKowGw1cQCMK6cmofqZ
            //LIVE Tampere:          rNn03Kf4SR2GU5A9Kn1l7rHevoXrfJqI

            //DEV Laurea (3AMK):     sfJZs3k5O6tUM3Sw0jkmNfU2E7iyvGx-
            //LIVE Laurea (3AMK):    BryvRRIXugPP6QGWaoNoOObBl7YhNnL9

            //DEV HalooKouvola:      2xR7QYUO3MhDu2ghxHKuV1fX_U4cTfpP
            //LIVE HalooKouvola:     g_v3M2DutNa542k4bkCGRfx6EMIs7kbo

            //DEV Puro Joensuu:      7FM0eBrgPFDiLjuXvESXD78YePV-9B00
            //LIVE Puro Joensuu:     FRxsv02_GJwI7WfxXSnpnQ_P6k73Wdy0

            //DEV Tredu:             J_glW_pLOPGGMtnvDyIx7EnM6sRcwP-6
            //LIVE Tredu:            excHYSUARq7YUIqlPizd_9JpyeYn5xQ6

            //DEV Rinki:             BYKDJnOHgOR2V_rt2-OBvyqQ6WP3Inop
            //LIVE Rinki:            ft82c7uuAjHzTT3Y4G72RZcqapRGhd4n

            //DEV #Tampere3:         t0e6JfH1n9-8mpA0cp-6Vltkv3y_VMoS
            //LIVE #Tampere3:        XmeqdxYepxrfCxJlxN-mnmCX5xq8BiG_

            //DEV Kaiku Liminka:     jyP2dDimQaG9mFgpu_AYaNSm89wP3y14
            //LIVE Kaiku Liminka:    UY1p2QIPsMmHNUtIn_G1qnGc8Y8ei7dd

            //DEV MunJäke:           RYUfaYWPuf8yGweoYrRGfr_xHV0bkRbR
            //LIVE MunJäke:          bYD_K1t5yw5iRUOK1f2iVW4sCriK6cYR

            //Current application
            headers.Add("x-board-appid", "UY1p2QIPsMmHNUtIn_G1qnGc8Y8ei7dd");

            headers.Add("x-board-appversion", GetAppVersion());
        }

        public void SetAuthorization(string customerHash)
        {
            var headers = _httpClient.DefaultRequestHeaders;
            if (headers.Contains("Authorization")) headers.Remove("Authorization");
            headers.TryAddWithoutValidation("Authorization", string.Format("CUSTOMER {0}", customerHash));

            ApplicationData.Current.LocalSettings.Values["customerHash"] = customerHash;    //ToDo
        }

        public string LoadCustomerHash()
        {
            var customerHashStored = ApplicationData.Current.LocalSettings.Values["customerHash"];

            if (customerHashStored == null) return null;

            string customerHash = (string)customerHashStored;

            var headers = _httpClient.DefaultRequestHeaders;
            if (!headers.Contains("Authorization"))
                headers.TryAddWithoutValidation("Authorization", string.Format("CUSTOMER {0}", customerHash));

            return customerHash;
        }

        public KeyValuePair<string, string>? LoadAccessTokenForLogin()
        {
            var name = ApplicationData.Current.LocalSettings.Values["accessTokenName"];
            var value = ApplicationData.Current.LocalSettings.Values["accessTokenValue"];

            if (name == null || value == null) return null;

            return new KeyValuePair<string, string>((string)name, (string)value);
        }

        public void SaveAccessTokenForLogin(KeyValuePair<string, string> accessTokenPair)
        {
            ApplicationData.Current.LocalSettings.Values["accessTokenName"] = accessTokenPair.Key;
            ApplicationData.Current.LocalSettings.Values["accessTokenValue"] = accessTokenPair.Value;
        }

        private static AppServerAgent _current;
        public static AppServerAgent Current { get { return _current ?? (_current = new AppServerAgent()); } }

        public async Task<ResponseCustomer> RegisterDeviceAsync()
        {

            Func<HttpContent> getContentFunc = () => new FormUrlEncodedContent(new[] {
                new KeyValuePair<string, string>("device_id", DeviceId),
                new KeyValuePair<string, string>("device_token", DeviceId),

            });

            var result = await SendRequestAsync<ResponseCustomer>(UrlRegisterDevice,
                HttpMethod.Post, getContentFunc);
            if (result.IsSuccess && result.Data != null && !string.IsNullOrEmpty(result.Data.Hash))
                SetAuthorization(result.Data.Hash);
            return result;
        }

        public async Task<ResponseBase> MarkCardAsSeen(CardBaseViewModel card)
        {
            ResponseMarkCardAsSeen result = await SendRequestAsync<ResponseMarkCardAsSeen>(UrlMarkCardAsSeen + card.Hash,
                HttpMethod.Post, null);

            BadgeNotification badgeNotification = new BadgeNotification(new BadgeNumericNotificationContent((uint)result.Data.NewCardsCount).GetXml());
            BadgeUpdateManager.CreateBadgeUpdaterForApplication().Update(badgeNotification);

            card.IsSeenAlready = true;
            return result;
        }

        public async Task<ResponseBase> DeleteCard(string hash)
        {
            ResponseBase result = await SendRequestAsync<ResponseCards>(UrlDeleteCard + hash,
                HttpMethod.Post, null);

            return result;
        }

        public async Task<ResponseCustomer> GetCustomerDataAsync()
        {
            KeyValuePair<string, string>? accessTokenPair = LoadAccessTokenForLogin();

            if (accessTokenPair == null)
            {
                throw new Exception("User is not authorized"); //ToDo: Localization
            }

            try
            {
                IAsyncOperation<PushNotificationChannel> channelOperation = PushNotificationChannelManager.CreatePushNotificationChannelForApplicationAsync();
                await channelOperation.AsTask().ContinueWith((Task<PushNotificationChannel> channelTask) =>
                {
                    PushNotificationChannel newChannel = channelTask.Result;
                    ApplicationData.Current.LocalSettings.Values["DeviceToken"] = newChannel.Uri;
                });
            }
            catch (Exception e) { };

            string IP = FindIPAddress();

            Geolocator _geolocator = new Geolocator();

            Func<HttpContent> getContentFunc;
            if (_geolocator == null || _geolocator.LocationStatus == PositionStatus.Disabled)
            {
                getContentFunc = () => new FormUrlEncodedContent(new[] {
                (KeyValuePair<string, string>)accessTokenPair,
                new KeyValuePair<string, string>("device_token", (string)ApplicationData.Current.LocalSettings.Values["DeviceToken"]),
                new KeyValuePair<string, string>("device_type", "3"),
                new KeyValuePair<string, string>("ip_addr", IP),
                new KeyValuePair<string, string>("language", GlobalizationPreferences.Languages[0].Replace('-','_'))
                });
            }
            else
            {
                try
                {
                    Geoposition pos = await _geolocator.GetGeopositionAsync();

                    getContentFunc = () => new FormUrlEncodedContent(new[] {
                        (KeyValuePair<string, string>)accessTokenPair,
                        new KeyValuePair<string, string>("device_token", (string)ApplicationData.Current.LocalSettings.Values["DeviceToken"]),
                        new KeyValuePair<string, string>("device_type", "3"),
                        new KeyValuePair<string, string>("ip_addr", IP),
                        new KeyValuePair<string, string>("language", GlobalizationPreferences.Languages[0].Replace('-','_')),
                        new KeyValuePair<string, string>("latitude", pos.Coordinate.Point.Position.Latitude.ToString() ),
                        new KeyValuePair<string, string>("longitude", pos.Coordinate.Point.Position.Longitude.ToString() )
                        });
                }
                catch (Exception e)
                {
                    getContentFunc = () => new FormUrlEncodedContent(new[] {
                        (KeyValuePair<string, string>)accessTokenPair,
                        new KeyValuePair<string, string>("device_token", (string)ApplicationData.Current.LocalSettings.Values["DeviceToken"]),
                        new KeyValuePair<string, string>("device_type", "3"),
                        new KeyValuePair<string, string>("ip_addr", IP),
                        new KeyValuePair<string, string>("language", GlobalizationPreferences.Languages[0].Replace('-','_'))
                        });
                }

            }

            var result = await SendRequestAsync<ResponseCustomer>(UrlGetCustomerData,
                HttpMethod.Post, getContentFunc);

            if (result.IsSuccess && result.Data != null && !string.IsNullOrEmpty(result.Data.Hash))
                SetAuthorization(result.Data.Hash);
            return result;
        }

        public async Task<ResponseCustomer> UpdateCustomerSettingsAsync(bool notifications, bool location)
        {
            Func<HttpContent> getContentFunc = () => new FormUrlEncodedContent(new[] {
                new KeyValuePair<string, string>("notifications", notifications.ToString()),
                new KeyValuePair<string, string>("location", location.ToString()),
            });

            var result = await SendRequestAsync<ResponseCustomer>(UrlGetCustomerData,
                HttpMethod.Post, getContentFunc);
            if (result.IsSuccess && result.Data != null && !string.IsNullOrEmpty(result.Data.Hash))
                SetAuthorization(result.Data.Hash);
            return result;
        }

        public async Task<ResponseCardsAfterAnswer> SaveAnswerAsync(RequestSaveAnswers request)
        {
            Func<HttpContent> getContentFunc = () => {
                using (var stream = new MemoryStream())
                {
                    var serializer = new DataContractJsonSerializer(typeof(RequestSaveAnswers));
                    serializer.WriteObject(stream, request);
                    stream.Position = 0;

                    using (var reader = new StreamReader(stream))
                        return new StringContent(reader.ReadToEnd(), System.Text.Encoding.UTF8, "application/json");
                }
            };


            return await SendRequestAsync<ResponseCardsAfterAnswer>(UrlGetCardsData,
                HttpMethod.Post, getContentFunc);
        }

        public async Task<ResponseCards> GetCardsDataAsync(bool isNotSavedCards)
        {
            return await SendRequestAsync<ResponseCards>(isNotSavedCards ? UrlGetCardsData : UrlGetPromotedCardsData, HttpMethod.Get);
        }

        private async Task<T> SendRequestAsync<T>(string url, HttpMethod httpMethod,
            Func<HttpContent> getContentFunc = null) where T : ResponseBase, new()
        {
            T result;
            var attempt = 1;

            while (true)
            {
                try
                {
                    if (httpMethod.Method == "GET") url += "?cache=" + Guid.NewGuid();
                    var requestMessage = new HttpRequestMessage(httpMethod,
                        string.Format("{0}{1}", ApiDomain, url));

                    if (getContentFunc != null)
                    {
                        var httpContent = getContentFunc();
#if DEBUG
                        Debug.WriteLine(await httpContent.ReadAsStringAsync());
#endif
                        if (httpContent != null) requestMessage.Content = httpContent;
                    }

                    var responseMessage = await _httpClient.SendAsync(requestMessage, HttpCompletionOption.ResponseContentRead);

                    if (responseMessage.StatusCode == HttpStatusCode.NotFound)
                        throw new NoInternetException();

                    var byteArray = await responseMessage.Content.ReadAsByteArrayAsync();
                    var answer = Encoding.UTF8.GetString(byteArray, 0, byteArray.Length);

                    if (string.IsNullOrEmpty(answer)) responseMessage.EnsureSuccessStatusCode();

                    result = (T)JsonConvert.DeserializeObject<T>(answer);

#if DEBUG
                    Debug.WriteLine(answer);
#endif
                    break;
                }
                catch (NoInternetException ex)
                {
                    //if (Debugger.IsAttached) Debugger.Break();

                    if (attempt < CountLoadingAttempts)
                    {
                        attempt++;
                        continue;
                    }

                    result = new T { Error = "Ei yhteyttä verkkoon. Tarkista verkkoyhteytesi ja yritä uudelleen.", IsSuccess = false };
                    break;
                }
                catch (Exception ex)
                {
                    //if (Debugger.IsAttached) Debugger.Break();
#if DEBUG
                    result = new T { Error = ex.ToString(), IsSuccess = false };
#else
                    //ToDo
                    result = new T { Error = "Something went wrong", IsSuccess = false };
#endif
                    break;
                }
            }

            return result;
        }

        public async Task<ResponseBase> AskForEmailDeeplink(string name, string email)
        {
            Func<HttpContent> getContentFunc;
            getContentFunc = () =>
            new StringContent("{\"name\":\"" + name + "\", \"email\":\"" + email + "\"}", System.Text.Encoding.UTF8, "application/json");
            var result = await SendRequestAsync<ResponseBase>(UrlGetDeeplinkEmail,
                HttpMethod.Post, getContentFunc);

            return result;
        }

        public static string GetAppVersion()
        {
            dynamic type = Application.Current.GetType();
            var version = (new AssemblyName(type.Assembly.FullName)).Version;
            return String.Format("{0}.{1}", version.Major, version.Minor);
        }

        public static string FindIPAddress()
        {
            List<string> ipAddresses = new List<string>();

            try
            {
                var hostnames = NetworkInformation.GetHostNames();
                foreach (var hn in hostnames)
                {
                    if (hn.IPInformation != null)
                    {
                        string ipAddress = hn.DisplayName;
                        ipAddresses.Add(ipAddress);
                    }
                }
            }
            catch (Exception e)
            {
                if (Debugger.IsAttached) Debugger.Break();
                return null;
            }

            return ipAddresses.Count == 0 ? null : ipAddresses[0];
        }

    }
}