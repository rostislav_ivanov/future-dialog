﻿using System;
using System;
using System.Collections.Generic;
using System.IO;
using System.Net;
using System.Runtime.InteropServices.WindowsRuntime;
using System.Runtime.Serialization;
using System.Runtime.Serialization.Json;
using System.Text;
using System.Threading.Tasks;
using Windows.ApplicationModel.Store;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.Networking.PushNotifications;
using Windows.Storage;

namespace TheBoard.ViewModel
{
    public sealed class ChannelAndWebResponse
    {
        public PushNotificationChannel Channel { get; set; }
        public String WebResponse { get; set; }
    }

    [DataContract]
    internal class UrlData
    {
        [DataMember]
        public String Url;
        [DataMember]
        public String ChannelUri;
        [DataMember]
        public bool IsAppId;
        [DataMember]
        public DateTime Renewed;
    }

    public class PushNotifications
    {
        public PushNotifications()
        {

        }

        public IAsyncOperation<ChannelAndWebResponse> OpenChannelAndUploadAsync(String url)
        {
            IAsyncOperation<PushNotificationChannel> channelOperation = PushNotificationChannelManager.CreatePushNotificationChannelForApplicationAsync();
            return ExecuteChannelOperation(channelOperation, url, MAIN_APP_TILE_KEY, true);
        }

        private IAsyncOperation<ChannelAndWebResponse> ExecuteChannelOperation(IAsyncOperation<PushNotificationChannel> channelOperation, String url, String itemId, bool isPrimaryTile)
        {
            return channelOperation.AsTask().ContinueWith<ChannelAndWebResponse>((Task<PushNotificationChannel> channelTask) =>
            {
                PushNotificationChannel newChannel = channelTask.Result;
                String webResponse = "URI already uploaded";

                // Upload the channel URI if the client hasn't recorded sending the same uri to the server
                UrlData dataForItem = TryGetUrlData(itemId);

                if (dataForItem == null || newChannel.Uri != dataForItem.ChannelUri)
                {
                    HttpWebRequest webRequest = (HttpWebRequest)HttpWebRequest.Create(url);
                    webRequest.Method = "POST";
                    webRequest.ContentType = "application/x-www-form-urlencoded";
                    byte[] channelUriInBytes = Encoding.UTF8.GetBytes("ChannelUri=" + WebUtility.UrlEncode(newChannel.Uri) + "&ItemId=" + WebUtility.UrlEncode(itemId));

                    Task<Stream> requestTask = webRequest.GetRequestStreamAsync();
                    using (Stream requestStream = requestTask.Result)
                    {
                        requestStream.Write(channelUriInBytes, 0, channelUriInBytes.Length);
                    }

                    Task<WebResponse> responseTask = webRequest.GetResponseAsync();
                    using (StreamReader requestReader = new StreamReader(responseTask.Result.GetResponseStream()))
                    {
                        webResponse = requestReader.ReadToEnd();
                    }
                }

                // Only update the data on the client if uploading the channel URI succeeds.
                // If it fails, you may considered setting another AC task, trying again, etc.
                // OpenChannelAndUploadAsync will throw an exception if upload fails
                UpdateUrl(url, newChannel.Uri, itemId, isPrimaryTile);
                return new ChannelAndWebResponse { Channel = newChannel, WebResponse = webResponse };
            }).AsAsyncOperation();
        }

    }
}
