﻿using System.Runtime.Serialization;

namespace TheBoard.ViewModel.View.CardContent
{
    [DataContract]
    public class CardContentRangeViewModel : CardContentBaseViewModel
    {
        private int _min;
        [DataMember]
        public int Min {
            get { return _min; }
            set {
                SetProperty(ref _min, value);
                if(_сardRangeValue == 0)
                    CardRangeValue = value;
            }
        }

        private int _max;
        [DataMember]
        public int Max {
            get { return _max; }
            set { SetProperty(ref _max, value); }
        }

        private string _lowestText;
        [DataMember]
        public string LowestText {
            get { return _lowestText; }
            set { SetProperty(ref _lowestText, value); }
        }

        private string _middleText;
        [DataMember]
        public string MiddleText
        {
            get { return _middleText; }
            set { SetProperty(ref _middleText, value); }
        }

        private string _highestText;
        [DataMember]
        public string HighestText {
            get { return _highestText; }
            set { SetProperty(ref _highestText, value); }
        }
        
        private int _сardRangeValue = 5;
        [DataMember]
        public int CardRangeValue
        {
            get { return _сardRangeValue; }
            set { SetProperty(ref _сardRangeValue, value); }
        }
    }
}
