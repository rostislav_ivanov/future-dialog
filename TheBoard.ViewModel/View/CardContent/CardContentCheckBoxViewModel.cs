﻿using System.Runtime.Serialization;

namespace TheBoard.ViewModel.View.CardContent
{
    [DataContract]
    public class CardContentCheckBoxViewModel : CardContentBaseViewModel
    {
        private bool _isChecked;
        [DataMember]
        public bool IsChecked {
            get { return _isChecked; }
            set { SetProperty(ref _isChecked, value); }
        }
    }
}
