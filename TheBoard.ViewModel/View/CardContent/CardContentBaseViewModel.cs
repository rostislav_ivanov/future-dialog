﻿using System.Runtime.Serialization;
using MetroLab.Common;

namespace TheBoard.ViewModel.View.CardContent
{
    [KnownType(typeof(CardContentCheckBoxViewModel))]
    [KnownType(typeof(CardContentRadioViewModel))]
    [KnownType(typeof(CardContentRangeViewModel))]
    [DataContract]
    public class CardContentBaseViewModel : BaseViewModel
    {
        private string _hash;
        [DataMember]
        public string Hash {
            get { return _hash; }
            set { SetProperty(ref _hash, value); }
        }

        private string _title;
        [DataMember]
        public string Title {
            get { return _title; }
            set { SetProperty(ref _title, value); }
        }

        private int _priority;
        [IgnoreDataMember]
        public int Priority {
            get { return _priority; }
            set { SetProperty(ref _priority, value); }
        }
    }
}
