﻿using System.Collections.Generic;
using System.Reflection;
using System.Runtime.Serialization.Json;
using System.Threading.Tasks;
using MetroLab.Common;
using System.Windows.Input;
using Windows.UI.Core;
using Windows.UI.Popups;
using Windows.UI.Xaml;
using TheBoard.ViewModel.Adapters;
using TheBoard.ViewModel.Model;
using TheBoard.ViewModel.View.Cards;
using System.Runtime.Serialization;
using Windows.Storage;
using Windows.UI.Notifications;
using MyToolkit.Messaging;

namespace TheBoard.ViewModel.View
{
    [DataContract]
    public class MainPageViewModel : BaseViewModel
    {
        public MainPageViewModel(bool isNotSavedCards)
        {
            _cards = new MTObservableCollection<CardBaseViewModel>(); 
            _cardsForView = new TakeLastNItemsCollection<CardBaseViewModel>(_cards, 2);

            Window.Current.Dispatcher.RunAsync(CoreDispatcherPriority.Normal, 
                async () => await LoadDataAsync(isNotSavedCards));
        }

        private readonly MTObservableCollection<CardBaseViewModel> _cards;
        [DataMember]
        public MTObservableCollection<CardBaseViewModel> Cards { get { return _cards; } }


        private readonly MTObservableCollection<CardBaseViewModel> _cardsForView;

        public MTObservableCollection<CardBaseViewModel> CardsForView
        {
            get { return _cardsForView; }
        }

        private bool _isLoadingEnabled;
        public bool IsLoadingEnabled {
            get { return _isLoadingEnabled; }
            set { SetProperty(ref _isLoadingEnabled, value); }
        }

        private bool _isNotSavedCards;
        public bool IsNotSavedCards
        {
            get { return _isNotSavedCards; }
            set { SetProperty(ref _isNotSavedCards, value); }
        }

        private bool _ifUserAnswerAllCards;
        public bool IfUserAnswerAllCards
        {
            get { return _ifUserAnswerAllCards; }
            set { SetProperty(ref _ifUserAnswerAllCards, value); }
        }

        public async Task LoadDataAsync(bool isNotSavedCards)
        {
            IsNotSavedCards = isNotSavedCards;
            IsLoadingEnabled = true;

            _cards.Clear();
            _cardsForView.Clear();

            var agent = AppServerAgent.Current;
            
            if (!agent.IsUserLoaded)
            {
                var customerResponse = await agent.GetCustomerDataAsync();
                //var responce = await agent.RegisterDeviceAsync();
                if (customerResponse.IsSuccess)
                {
                    agent.IsUserLoaded = true;
                }
                else
                {
                    await ShowDialogAsync(customerResponse.Error, null, null, "Sulje");
                    IsLoadingEnabled = false;
                    return;
                }
            }

            var responseCards = await agent.GetCardsDataAsync(isNotSavedCards);

            if (responseCards.IsSuccess)
            {
                if (responseCards.Cards.Count > 0)
                {
                    var adapter = new CardToCardViewModelAdapter();
                    responseCards.Cards.Reverse();
                    foreach (var card in responseCards.Cards)
                    {
                        var cardViewModel = adapter.Create(card, isNotSavedCards);

                        if (cardViewModel == null) continue;

                        ICommand closeCardCommand = null;
                        GetCommand(ref closeCardCommand, o => CloseCard(cardViewModel));
                        cardViewModel.CloseCommand = closeCardCommand;

                        ICommand saveAnswerCommand = null;
                        GetCommand(ref saveAnswerCommand, async o =>
                        {
                            await SaveAnswerAsync(cardViewModel);
                            Messenger.Default.Send(new ToolTipMessage(ToolTipMessageType.FirstCard));
                        });
                        cardViewModel.SaveAnswerCommand = saveAnswerCommand;

                        ICommand crossButtonCommand = null;
                        GetCommand(ref crossButtonCommand, async o =>
                            {
                                cardViewModel.IsClosed = true;

                                if (ApplicationData.Current.LocalSettings.Values["CloseButtonFirstClickDialog"] == null)
                                {
                                    ApplicationData.Current.LocalSettings.Values["CloseButtonFirstClickDialog"] = "no";
                                    await ShowOkCancelDialogAsync(Localization.LocalizedResources.GetString("dialog_skip_"), null,
                                            a => SaveAnswerAsync(cardViewModel), Localization.LocalizedResources.GetString("button_yes_"),
                                            Localization.LocalizedResources.GetString("button_no_"));
                                }
                                else
                                {
                                    await SaveAnswerAsync(cardViewModel);
                                }
                            });
                        cardViewModel.CrossButtonCommand = crossButtonCommand;

                        if (card.Type == CardType.Single)
                        {
                            ICommand noCommand = null;
                            GetCommand(ref noCommand, async o =>
                            {
                                ((CardSingleViewModel)cardViewModel).AnswerOnSingleQuestion = false;
                                await SaveAnswerAsync(cardViewModel);
                                Messenger.Default.Send(new ToolTipMessage(ToolTipMessageType.FirstCard));
                            });
                            cardViewModel.NoCommand = noCommand;
                        }

                        if (card.Type == CardType.Text)
                        {
                            ICommand noCommand = null;
                            GetCommand(ref noCommand, async o =>
                            {
                                ((CardTextViewModel)cardViewModel).AnswerOnSingleQuestion = false;
                                ((CardTextViewModel)cardViewModel).VideoUrl = null;
                                await SaveAnswerAsync(cardViewModel);
                                Messenger.Default.Send(new ToolTipMessage(ToolTipMessageType.FirstCard));
                            });
                            cardViewModel.NoCommand = noCommand;
                        }
                        Cards.Add(cardViewModel);
                    }

                    if (Cards.Count > 0) Cards[Cards.Count - 1].IsOnTop = true;
                      
                    if (Cards.Count == 1)
                        Cards[0].IsOnlyOneLeftCard = true;
                }
            }
            else await ShowDialogAsync(responseCards.Error, null, null, "Sulje");

            IsLoadingEnabled = false;
        }

        private async void CloseCard(CardBaseViewModel card)
        {
            card.IsFullScreen = false;
            string errorMessage;
            var answerContent = card.TryGetAnswer(out errorMessage);

            if (card is CardTextViewModel)
            {
                ((CardTextViewModel)card).PauseVideo();
            }

            if (!IsNotSavedCards)
            {
                errorMessage = null;
            }

            await(await AppViewModel.GetMainAsync()).RunAtDispatcherAsync(() =>
            { 
                if (!string.IsNullOrWhiteSpace(errorMessage) || !card.IsAnswered)
                {
                    Cards.Move(Cards.IndexOf(card), 0);
                    Cards[0].IsOnTop = false;
                    Cards[Cards.Count - 1].IsOnTop = true;
                    return;
                }

                Cards.Remove(card);
                if (Cards.Count > 0)
                    Cards[Cards.Count - 1].IsOnTop = true;
                else
                    IfUserAnswerAllCards = true;

                if (Cards.Count == 1)
                    Cards[0].IsOnlyOneLeftCard = true;
            });
        }

        private async Task SaveAnswerAsync(CardBaseViewModel card)
        {
            if (IsNotSavedCards)
            {
                string errorMessage;
                var answerContent = card.TryGetAnswer(out errorMessage);
                if (!string.IsNullOrWhiteSpace(errorMessage))
                {
                    Cards.Move(Cards.IndexOf(card), 0);
                    Cards[0].IsOnTop = false;
                    Cards[Cards.Count - 1].IsOnTop = true;
                    return;
                }

                var agent = AppServerAgent.Current;
                var saveAnswerResponse = await agent.SaveAnswerAsync(
                    new RequestSaveAnswers { Content = answerContent });

                if (saveAnswerResponse.IsSuccess)
                {
                    if (saveAnswerResponse.TargetedCards.Cards.Count > 0)
                    {
                        var adapter = new CardToCardViewModelAdapter();
                        saveAnswerResponse.TargetedCards.Cards.Reverse();
                        foreach (var cardTemp in saveAnswerResponse.TargetedCards.Cards)
                        {
                            var cardViewModel = adapter.Create(cardTemp, IsNotSavedCards);

                            if (cardViewModel == null) continue;

                            ICommand closeCardCommand = null;
                            GetCommand(ref closeCardCommand, o => CloseCard(cardViewModel));
                            cardViewModel.CloseCommand = closeCardCommand;

                            ICommand saveAnswerCommand = null;
                            GetCommand(ref saveAnswerCommand, async o =>
                            {
                                await SaveAnswerAsync(cardViewModel);
                                Messenger.Default.Send(new ToolTipMessage(ToolTipMessageType.FirstCard));
                            });
                            cardViewModel.SaveAnswerCommand = saveAnswerCommand;

                            ICommand crossButtonCommand = null;
                            GetCommand(ref crossButtonCommand, async o =>
                            {
                                cardViewModel.IsClosed = true;

                                if (ApplicationData.Current.LocalSettings.Values["CloseButtonFirstClickDialog"] == null)
                                {
                                    ApplicationData.Current.LocalSettings.Values["CloseButtonFirstClickDialog"] = "no";
                                    await ShowOkCancelDialogAsync(Localization.LocalizedResources.GetString("dialog_skip_"), null,
                                            a => SaveAnswerAsync(cardViewModel), Localization.LocalizedResources.GetString("button_yes_"),
                                            Localization.LocalizedResources.GetString("button_no_"));
                                }
                                else
                                {
                                    await SaveAnswerAsync(cardViewModel);
                                }
                            });
                            cardViewModel.CrossButtonCommand = crossButtonCommand;

                            if (cardTemp.Type == CardType.Single)
                            {
                                ICommand noCommand = null;
                                GetCommand(ref noCommand, async o =>
                                {
                                    ((CardSingleViewModel)cardViewModel).AnswerOnSingleQuestion = false;
                                    await SaveAnswerAsync(cardViewModel);
                                    Messenger.Default.Send(new ToolTipMessage(ToolTipMessageType.FirstCard));
                                });
                                cardViewModel.NoCommand = noCommand;
                            }

                            if (cardTemp.Type == CardType.Text)
                            {
                                ICommand noCommand = null;
                                GetCommand(ref noCommand, async o =>
                                {
                                    ((CardTextViewModel)cardViewModel).AnswerOnSingleQuestion = false;
                                    ((CardTextViewModel)cardViewModel).VideoUrl = null;
                                    await SaveAnswerAsync(cardViewModel);
                                    Messenger.Default.Send(new ToolTipMessage(ToolTipMessageType.FirstCard));
                                });
                                cardViewModel.NoCommand = noCommand;
                            }
                            Cards.Add(cardViewModel);
                        }

                        if (Cards.Count > 0) Cards[Cards.Count - 1].IsOnTop = true;

                        if (Cards.Count == 1)
                            Cards[0].IsOnlyOneLeftCard = true;
                    }
                }
                
               card.IsAnswered = true;
                CloseCard(card);
            }
            else
            {
                //IsLoadingEnabled = true;
                var responce = AppServerAgent.Current.DeleteCard(card.Hash);

                card.IsAnswered = true;
                CloseCard(card);
            }
        }
    }
}