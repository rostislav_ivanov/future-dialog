﻿using System.Collections.Generic;
using System.Runtime.Serialization;
using System.Windows.Input;
using MetroLab.Common;
using TheBoard.ViewModel.Model;
using TheBoard.Localization;
using System;
using MyToolkit.Messaging;

namespace TheBoard.ViewModel.View.Cards
{
    [KnownType(typeof(CardTextBoxViewModel))]
    [KnownType(typeof(CardImageViewModel))]
    [KnownType(typeof(CardCheckBoxViewModel))]
    [KnownType(typeof(CardRadioViewModel))]
    [KnownType(typeof(CardRangeViewModel))]
    [KnownType(typeof(CardSingleViewModel))]
    [DataContract]
    public abstract class CardBaseViewModel : BaseViewModel, IRemovable
    {
        public abstract string CardType { get; }

        private string _nextButtonText = LocalizedResources.GetString("answer");
        [DataMember]
        public string NextButtonText
        {
            get { return _nextButtonText; }
            set { SetProperty(ref _nextButtonText, value); }
        }

        private string _noButtonText;
        [DataMember]
        public string NoButtonText
        {
            get { return _noButtonText; }
            set { SetProperty(ref _noButtonText, value); }
        }

        protected string _question;
        [DataMember]
        public string Question {
            get { return _question; }
            set { SetProperty(ref _question, value); }
        }

        protected string _description;
        [DataMember]
        public string Description
        {
            get { return _description; }
            set { SetProperty(ref _description, value); }
        }

        private string _hash;
        [DataMember]
        public string Hash {
            get { return _hash; }
            set { SetProperty(ref _hash, value); }
        }

        private bool _isOnTop;
        [DataMember]
        public bool IsOnTop {
            get { return _isOnTop; }
            set {
                SetProperty(ref _isOnTop, value);
                if(!IsSeenAlready)
                {
                    AppServerAgent.Current.MarkCardAsSeen(this);
                }

                if(value == true)
                {
                    if (CanSkip)
                    {
                        Messenger.Default.Send(new ToolTipMessage(ToolTipMessageType.Skippable));
                    }

                    if (IsPromoted)
                    {
                        Messenger.Default.Send(new ToolTipMessage(ToolTipMessageType.Promoted));
                    }
                }
            }
        }

        private bool _isSeenAlready = false;
        [DataMember]
        public bool IsSeenAlready
        {
            get { return _isSeenAlready; }
            set { SetProperty(ref _isSeenAlready, value); }
        }

        private ICommand _closeCommand;
        [IgnoreDataMember]
        public ICommand CloseCommand {
            get { return _closeCommand; }
            set { SetProperty(ref _closeCommand, value); }
        }

        private ICommand _crossButtonCommand;
        [IgnoreDataMember]
        public ICommand CrossButtonCommand
        {
            get { return _crossButtonCommand; }
            set { SetProperty(ref _crossButtonCommand, value); }
        }

        private ICommand _saveAnswerCommand;
        [IgnoreDataMember]
        public ICommand SaveAnswerCommand {
            get { return _saveAnswerCommand; }
            set { SetProperty(ref _saveAnswerCommand, value); }
        }

        private ICommand _noCommand;
        [IgnoreDataMember]
        public ICommand NoCommand
        {
            get { return _noCommand; }
            set { SetProperty(ref _noCommand, value); }
        }

        public void Remove()
        {
            var closeCommand = CloseCommand;
            if (closeCommand != null)
                closeCommand.Execute(null);
        }

        public abstract List<CardContentAnswer> TryGetAnswer(out string errorMessage);

        private bool _isAnswered = false;
        [DataMember]
        public bool IsAnswered
        {
            get { return _isAnswered; }
            set { SetProperty(ref _isAnswered, value); }
        }

        private bool _isNeedToExpand = false;
        [DataMember]
        public bool IsNeedToExpand
        {
            get { return _isNeedToExpand; }
            set { SetProperty(ref _isNeedToExpand, value); }
        }

        private bool _isFullScreen = false;
        [DataMember]
        public bool IsFullScreen
        {
            get { return _isFullScreen; }
            set {
                SetProperty(ref _isFullScreen, value);
                OnPropertyChanged("ButtonWidth");
            }
        }

        private bool _canSkip = false;
        [DataMember]
        public bool CanSkip
        {
            get { return _canSkip; }
            set { SetProperty(ref _canSkip, value); }
        }

        private bool _isPromoted;
        [DataMember]
        public bool IsPromoted
        {
            get { return _isPromoted; }
            set { SetProperty(ref _isPromoted, value); }
        }

        protected Uri _imageUrl;
        [DataMember]
        public Uri ImageUrl
        {
            get { return _imageUrl; }
            set { SetProperty(ref _imageUrl, value); }
        }

        //it used only in videocards but I need to here to place video above question & description
        private Uri _videoUrl;
        [DataMember]
        public Uri VideoUrl
        {
            get { return _videoUrl; }
            set { SetProperty(ref _videoUrl, value); }
        }

        public bool CloseButtonAboveVideoOrImage
        {
            get { return _canSkip && (VideoUrl != null || ImageUrl != null); }
        }

        private bool _isClosed;
        [DataMember]
        public bool IsClosed
        {
            get { return _isClosed; }
            set { SetProperty(ref _isClosed, value); }
        }

        public int ButtonWidth
        {
            get { return string.IsNullOrEmpty(NoButtonText) ? 480 : IsFullScreen ? 201 : 182;  }
        }

        private bool _isOnlyOneLeftCard;
        [DataMember]
        public bool IsOnlyOneLeftCard
        {
            get { return _isOnlyOneLeftCard; }
            set { SetProperty(ref _isOnlyOneLeftCard, value); }
        }

        public virtual bool HasAnswer
        {
            get
            {
                return true;
            }
        }

    }
}
