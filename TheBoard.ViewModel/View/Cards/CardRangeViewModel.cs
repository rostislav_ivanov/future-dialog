﻿using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using TheBoard.ViewModel.Model;
using TheBoard.ViewModel.View.CardContent;

namespace TheBoard.ViewModel.View.Cards
{
    [DataContract]
    public class CardRangeViewModel : CardBaseViewModel
    {
        public override string CardType { get { return TheBoard.ViewModel.Model.CardType.Range; } }

        private List<CardContentRangeViewModel> _content;
        [DataMember]
        public List<CardContentRangeViewModel> Content {
            get { return _content; }
            set { SetProperty(ref _content, value); }
        }

        public override List<CardContentAnswer> TryGetAnswer(out string errorMessage)
        {
            errorMessage = null;

            return Content.Select(a => new CardContentAnswer {
                CardHash = Hash, CardContentHash = null, Data = IsClosed ? null : a.CardRangeValue.ToString() }).ToList();
        }
    }
}
