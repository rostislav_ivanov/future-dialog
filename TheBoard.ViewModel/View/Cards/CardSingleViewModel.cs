﻿using System;
using System.Collections.Generic;
using System.Runtime.Serialization;
using TheBoard.ViewModel.Model;

namespace TheBoard.ViewModel.View.Cards
{
    [DataContract]
    public class CardSingleViewModel : CardBaseViewModel
    {
        public override string CardType { get { return Model.CardType.Single; } }

        private bool _answerOnSingleQuestion = true;
        [DataMember]
        public bool AnswerOnSingleQuestion
        {
            get { return _answerOnSingleQuestion; }
            set { SetProperty(ref _answerOnSingleQuestion, value); }
        }

        public override List<CardContentAnswer> TryGetAnswer(out string errorMessage)
        {
            errorMessage = null;

            return new List<CardContentAnswer> { new CardContentAnswer {
                CardHash = Hash, Data = IsClosed ? "" : AnswerOnSingleQuestion ? "1":"0" } };
        }
    }
}
