﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Runtime.InteropServices.WindowsRuntime;
using System.Runtime.Serialization;
using System.Threading.Tasks;
using System.Windows.Input;
using Windows.Storage;
using Windows.Storage.FileProperties;
using Windows.Storage.Streams;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Media.Imaging;
using TheBoard.ViewModel.Model;
using Windows.UI.Core;

namespace TheBoard.ViewModel.View.Cards
{
    [DataContract]
    public class CardImageViewModel : CardBaseViewModel
    {
        public override string CardType { get { return Model.CardType.Image; } }

        private bool _isEnableTextInput;
        [DataMember]
        public bool IsEnableTextInput
        {
            get { return _isEnableTextInput & ChoosedPhoto != null; }
            set
            {
                SetProperty(ref _isEnableTextInput, value);
            }
        }

        private int _lettersLimit = 240;
        [DataMember]
        public int LettersLimit
        {
            get { return _lettersLimit; }
            set
            {
                SetProperty(ref _lettersLimit, value);
            }
        }

        private string _choosedPhotoString;
        private ImageSource _choosedPhoto;
        [IgnoreDataMember]
        public ImageSource ChoosedPhoto {
            get { return _choosedPhoto; }
            set
            {
                SetProperty(ref _choosedPhoto, value);
                OnPropertyChanged("IsEnableTextInput");
            }
        }

        public async void GetImage(bool isChooseMode)
        {
            try
            {
                StorageFile file = null;
/*#if WINDOWS_APP
                var taskCompletionSource = new TaskCompletionSource<StorageFile>();
                await mainAppViewModel.RunAtDispatcherAsync(async () =>
                    taskCompletionSource.SetResult(IsPhotoChooseMode.Value
                        ? await ((IApp) Application.Current).PickSingleFileAsync()
                        : await ((IApp) Application.Current).CapturePhotoFileAsync()));
                file = await taskCompletionSource.Task;
#else*/
                Window.Current.Dispatcher.RunAsync(CoreDispatcherPriority.Normal,
                   async () =>
                   {
                       file = await ((IApp)Application.Current).PickSingleFileAsync();

                       if (file == null) return;

                       using (var stream = await file.OpenAsync(Windows.Storage.FileAccessMode.Read))
                       {
                           var bitmapImage = new BitmapImage();
                           bitmapImage.SetSource(stream);
                           ChoosedPhoto = bitmapImage;

                           stream.Seek(0);
                           var bytes = new byte[stream.Size];
                           var buffer = bytes.AsBuffer();
                           await stream.ReadAsync(buffer, (uint)bytes.Length, InputStreamOptions.ReadAhead);
                           _choosedPhotoString = Convert.ToBase64String(bytes);
                           OnPropertyChanged("HasAnswer");
                       }

                   });

                /*using (var stream = await file.GetThumbnailAsync(ThumbnailMode.PicturesView, 
                    300, ThumbnailOptions.ResizeThumbnail))
                {
                    var bitmapImage = new BitmapImage();
                    bitmapImage.SetSource(stream);
                    ChoosedPhoto = bitmapImage;

                    stream.Seek(0);
                    var bytes = new byte[stream.Size];
                    var buffer = bytes.AsBuffer();
                    await stream.ReadAsync(buffer, (uint)bytes.Length, InputStreamOptions.ReadAhead);
                    _choosedPhotoString = Convert.ToBase64String(bytes);
                }*/
            }
            catch (Exception ex)
            {
                if (Debugger.IsAttached) Debugger.Break();
            }          
        }

        public override List<CardContentAnswer> TryGetAnswer(out string errorMessage)
        {
            errorMessage = null;

            if (!HasAnswer)
            {
                errorMessage = "ToDo: you must choose an image";
                return null;
            }

            return new List<CardContentAnswer> { new CardContentAnswer {
                CardHash = Hash, Data = IsClosed ? "" :_choosedPhotoString } };
        }

        public override bool HasAnswer
        {
            get
            {
                return IsClosed || !string.IsNullOrWhiteSpace(_choosedPhotoString);
            }
        }
    }
}
