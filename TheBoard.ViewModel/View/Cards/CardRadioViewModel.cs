﻿using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using TheBoard.ViewModel.Model;
using TheBoard.ViewModel.View.CardContent;

namespace TheBoard.ViewModel.View.Cards
{
    [DataContract]
    public class CardRadioViewModel : CardBaseViewModel
    {
        public override string CardType { get { return Model.CardType.Radio; } }

        private List<CardContentRadioViewModel> _content;
        [DataMember]
        public List<CardContentRadioViewModel> Content {
            get { return _content; }
            set { SetProperty(ref _content, value); }
        }

        public override List<CardContentAnswer> TryGetAnswer(out string errorMessage)
        {
            errorMessage = null;
            var checkedItems = Content.Where(i => i.IsChecked).ToList();

            if (!IsClosed && checkedItems.Count == 0)
            {
                errorMessage = "ToDo: you must check the answer";
                return null;
            }

            if (IsClosed)
            {
                return new List<CardContentAnswer>(1) { new CardContentAnswer()
                        { CardHash = Hash, CardContentHash = null, Data = null } };
            }
            else
                return checkedItems.Select(a => new CardContentAnswer
                {
                    CardHash = Hash,
                    CardContentHash = a.Hash,
                    Data = null
                }).ToList();
        }

        public override bool HasAnswer
        {
            get
            {
                var checkedItems = Content.Where(i => i.IsChecked).ToList();

                return IsClosed || checkedItems.Count != 0 || Content.Count == 0;
            }
        }
    }
}
