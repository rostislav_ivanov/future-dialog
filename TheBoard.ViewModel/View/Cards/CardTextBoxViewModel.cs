﻿using System.Collections.Generic;
using System.Runtime.Serialization;
using TheBoard.ViewModel.Model;

namespace TheBoard.ViewModel.View.Cards
{
    [DataContract]
    public class CardTextBoxViewModel : CardBaseViewModel
    {
        public override string CardType { get { return Model.CardType.TextBox; } }

        private string _text;
        [DataMember]
        public string Text {
            get { return _text; }
            set {
                SetProperty(ref _text, value);
                OnPropertyChanged("HasAnswer");
            }
        }

        private int _lettersLimit = 240;
        [DataMember]
        public int LettersLimit
        {
            get { return _lettersLimit; }
            set
            {
                SetProperty(ref _lettersLimit, value);
            }
        }

        public override List<CardContentAnswer> TryGetAnswer(out string errorMessage)
        {
            errorMessage = null;

            if (!IsClosed && string.IsNullOrWhiteSpace(Text))
            {
                errorMessage = "ToDo: you must enter your answer";
                return null;
            }

            return new List<CardContentAnswer> { new CardContentAnswer {
                CardHash = Hash, Data = Text } };
        }

        public override bool HasAnswer
        {
            get
            {
                return IsClosed || !string.IsNullOrWhiteSpace(Text);
            }
        }
    }
}
