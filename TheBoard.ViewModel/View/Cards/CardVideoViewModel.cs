﻿using System;
using System.Collections.Generic;
using System.Runtime.Serialization;
using TheBoard.ViewModel.Model;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;

namespace TheBoard.ViewModel.View.Cards
{
    [DataContract]
    public class CardTextViewModel : CardSingleViewModel
    {
        public override string CardType { get { return Model.CardType.Text; } }

        public void PauseVideo()
        {
            var tmpUrl = VideoUrl;
            VideoUrl = null;
            VideoUrl = tmpUrl;
        }

    }
}
