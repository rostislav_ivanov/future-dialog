﻿using MetroLab.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;
using TheBoard.Localization;

namespace TheBoard.ViewModel.View
{
    [DataContract]
    public class EmailLoginViewModel : BaseViewModel
    {
        private string _name;
        [DataMember]
        public string Name
        {
            get { return _name; }
            set
            {
                SetProperty(ref _name, value);
                if (!IsLoginButtonActive && !IsLoadingEnabled && !String.IsNullOrEmpty(_name) && 
                    !String.IsNullOrEmpty(_email) && _email.Contains("@"))
                {
                    IsLoginButtonActive = true;
                    OnPropertyChanged("IsLoginButtonActive");
                }
            }
        }

        private string _email;
        [DataMember]
        public string Email
        {
            get { return _email; }
            set
            {
                SetProperty(ref _email, value);
                if (!IsLoginButtonActive && !IsLoadingEnabled && !String.IsNullOrEmpty(_name) &&
                    !String.IsNullOrEmpty(_email) && _email.Contains("@"))
                {
                    IsLoginButtonActive = true;
                    OnPropertyChanged("IsLoginButtonActive");
                }
            }
        }

        private bool _isLoadingEnabled;
        [DataMember]
        public bool IsLoadingEnabled
        {
            get { return _isLoadingEnabled; }
            set
            {
                SetProperty(ref _isLoadingEnabled, value);
            }
        }

        private bool _isLoginButtonActive;
        [DataMember]
        public bool IsLoginButtonActive
        {
            get { return _isLoginButtonActive; }
            set
            {
                SetProperty(ref _isLoginButtonActive, value);
            }
        }

        private ICommand _emailRegisterCommand;
        [IgnoreDataMember]
        public ICommand EmailRegisterCommand
        {
            get { return GetCommand(ref _emailRegisterCommand, Register); }
        }

        private async void Register(object obj)
        {
            IsLoadingEnabled = true;
            IsLoginButtonActive = false;
            var agent = AppServerAgent.Current;
            var result = await agent.AskForEmailDeeplink(Name, Email);

            IsLoadingEnabled = false;
            if (result.IsSuccess)
            {
                await ShowDialogAsync(LocalizedResources.GetString("_message_for_deeplink"), null, null, "Sulje");
            }
            else
            {
                await ShowDialogAsync(result.Error, null, null, "Sulje");
            }
        }

    }
}
