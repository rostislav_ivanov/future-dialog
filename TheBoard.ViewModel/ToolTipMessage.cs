﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TheBoard.ViewModel
{
    public enum ToolTipMessageType { Skippable, Promoted , FirstCard };

    public sealed class ToolTipMessage
    {
        public ToolTipMessageType ToolTipMessageType_;

        public ToolTipMessage(ToolTipMessageType toolTipMessageType)
        {
            ToolTipMessageType_ = toolTipMessageType;
        }
    }
}
