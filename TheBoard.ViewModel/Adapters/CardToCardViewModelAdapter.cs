﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Runtime.Serialization.Json;
using System.Text;
using System.Text.RegularExpressions;
using TheBoard.Localization;
using TheBoard.ViewModel.Model;
using TheBoard.ViewModel.View.CardContent;
using TheBoard.ViewModel.View.Cards;

namespace TheBoard.ViewModel.Adapters
{
    public class CardToCardViewModelAdapter
    {
        private const string YoutubeLinkRegex = "(?:.+?)?(?:\\/v\\/|watch\\/|\\?v=|\\&v=|youtu\\.be\\/|\\/v=|^youtu\\.be\\/)([a-zA-Z0-9_-]{11})+";

        public CardBaseViewModel Create(Card input, bool isNotSavedCards)
        {
            CardBaseViewModel output;
            Uri imageUrl = null;
            CardContent tmpCardContent = null;
            CardContent tmpJsonCardContent = null;
            JsonContentItem jsonContentItem = null;

            foreach (var item in input.Content)
            {
                if(item.Category == "file")
                {
                    tmpCardContent = item;
                }
                else if(item.Category == "json")
                {
                    tmpJsonCardContent = item;
                }
            }

            if (tmpCardContent != null && !string.IsNullOrEmpty(tmpCardContent.Title))
            {
                if (tmpCardContent.Title.StartsWith("http://") || tmpCardContent.Title.StartsWith("https://"))
                {
                    imageUrl = new Uri(tmpCardContent.Title);
                }
                else
                {
                    imageUrl = new Uri(AppServerAgent.ServerUrl + "uploads/" + tmpCardContent.Title);
                }
            }
            input.Content.Remove(tmpCardContent);

            if (tmpJsonCardContent != null && !string.IsNullOrEmpty(tmpJsonCardContent.Title))
            {
                jsonContentItem = JsonConvert.DeserializeObject<JsonContentItem>(tmpJsonCardContent.Title);
            }
            input.Content.Remove(tmpJsonCardContent);

            switch (input.Type)
            {
                case CardType.TextBox:
                {
                    output = new CardTextBoxViewModel();    

                    if(jsonContentItem != null && jsonContentItem.TextAreaLimit > 0)
                    {
                        ((CardTextBoxViewModel)output).LettersLimit = jsonContentItem.TextAreaLimit;
                    }
                    break;
                }
                case CardType.Image:
                {
                    output = new CardImageViewModel();

                    if (jsonContentItem != null && jsonContentItem.TextAreaLimit > 0)
                    {
                        ((CardImageViewModel)output).IsEnableTextInput = jsonContentItem.IsEnableTextInput;
                        ((CardImageViewModel)output).LettersLimit = jsonContentItem.TextAreaLimit;
                    }
                    break;
                }
                case CardType.CheckBox:
                {
                    output = new CardCheckBoxViewModel {
                        Content = GetCardContent<CardContentCheckBoxViewModel>(input.Content)
                    };       
                    break;
                }
                case CardType.Radio:
                {
                    output = new CardRadioViewModel { 
                        Content = GetCardContent<CardContentRadioViewModel>(input.Content)
                    };       
                    break;
                }
                case CardType.Range:
                {
                    var card = new CardRangeViewModel();
                    if (jsonContentItem != null)
                    {
                        card.Content = new List<CardContentRangeViewModel>();

                        card.Content.Add(new CardContentRangeViewModel());

                        card.Content[0].LowestText = jsonContentItem.LowestText;
                        card.Content[0].MiddleText = jsonContentItem.MiddleText;
                        card.Content[0].HighestText = jsonContentItem.HighestText;

                        var parts = jsonContentItem.RangeText.Split(new[] { '-' }, StringSplitOptions.RemoveEmptyEntries);
                        if (parts.Length > 1)
                        {
                            double min, max;
                            if (double.TryParse(parts[0], NumberStyles.Number, CultureInfo.InvariantCulture, out min))
                                card.Content[0].Min = (int)min;

                            if (double.TryParse(parts[1], NumberStyles.Number, CultureInfo.InvariantCulture, out max))
                                card.Content[0].Max = (int)max;
                        }
                    }
                    output = card;
                    break;
                }
                case CardType.Single:
                {
                    CardSingleViewModel tmpCard = new CardSingleViewModel();

                    if (isNotSavedCards && jsonContentItem != null)
                    {
                        tmpCard.NextButtonText = jsonContentItem.YesButtonText;
                        tmpCard.NoButtonText = jsonContentItem.NoButtonText;
                    }
                    output = tmpCard;
                    break;
                }
                case CardType.Text:
                {
                    CardTextViewModel tmpCard = new CardTextViewModel();

                    var content = GetCardContent<CardContentBaseViewModel>(input.Content);

                    string url = content.Count > 0 ? content[0].Title : null;
                    if (!string.IsNullOrEmpty(url))
                    {
                        //string html = @"<!DOCTYPE html><style> body{margin:0; padding:0;} iframe{width:100%;height:480px;}@media screen and (max-width:300px) { iframe{width:100%;height:180px;}}  </style><iframe style=""padding:0px;margin-bottom:-20px;""   src=""http://www.youtube.com/embed/" + "QWWyI7eYqpI" + @"?rel=0"" frameborder=""0"" allowfullscreen></iframe>";

                        var regex = new Regex(YoutubeLinkRegex, RegexOptions.None);
                        foreach (Match match in regex.Matches(url))
                        {
                            foreach (var groupdata in match.Groups.Cast<Group>().Where(groupdata => !groupdata.ToString().StartsWith("http://") && !groupdata.ToString().StartsWith("https://") && !groupdata.ToString().StartsWith("youtu") && !groupdata.ToString().StartsWith("www.")))
                            {
                                tmpCard.VideoUrl = new Uri("http://www.youtube.com/embed/" + groupdata);
                                imageUrl = null;
                                break;
                            }
                        }

                    }

                    output = tmpCard;
                    break;
                }
                default: 
                {
                        return null;
                }
                //default: throw new NotSupportedException();
            }

            output.Question = input.Question;
            output.Description = input.Description;
            output.Hash = input.Hash;
            output.CanSkip = input.CanSkip;
            output.IsPromoted = input.IsPromoted;
            if (imageUrl != null)
            {
                output.ImageUrl = imageUrl;
            }

            if (isNotSavedCards && jsonContentItem != null && output.CardType != CardType.Single && !string.IsNullOrEmpty(jsonContentItem.OkButtonText))
            {
                output.NextButtonText = jsonContentItem.OkButtonText;
            }

            if (!isNotSavedCards)
            {
                output.NextButtonText = LocalizedResources.GetString("delete").ToUpper();
                output.IsSeenAlready = true;
            }
            return output;
        }

        private List<T> GetCardContent<T>(List<CardContent> input)
            where T : CardContentBaseViewModel, new()
        {

            return input.Where(d => d.Category != "empty").Select(i => new T
            {
                Hash = i.Hash,
                Title = i.Title,
                Priority = i.Priority ?? 0
            }).OrderBy(v => v.Priority).ToList();
        }
    }
}
