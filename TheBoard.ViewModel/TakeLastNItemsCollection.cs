﻿using System;
using System.Collections.ObjectModel;
using System.Collections.Specialized;
using System.Diagnostics;
using System.Linq;
using MetroLab.Common;

namespace TheBoard
{
    public class TakeLastNItemsCollection<T> : MTObservableCollection<T>
    {
        private readonly ObservableCollection<T> _sourceCollection;

        private int _n;

        public TakeLastNItemsCollection(ObservableCollection<T> sourceCollection, int n)
        {
            if (sourceCollection == null) throw new ArgumentNullException("sourceCollection");
            if (n < 1) throw new ArgumentOutOfRangeException("sourceCollection");
            _sourceCollection = sourceCollection;
            _n = n;
            _sourceCollection.CollectionChanged += SourceCollectionOnCollectionChanged;
        }

        private void SourceCollectionOnCollectionChanged(object sender, NotifyCollectionChangedEventArgs args)
        {
            var desiredItems = _sourceCollection.Reverse().Take(_n).Reverse().ToList();

            for (int index = 0; index < desiredItems.Count; index++)
            {
                var desiredItem = desiredItems[index];
                var actualPosition = this.IndexOf(desiredItem);
                if (actualPosition == index)
                    continue;

                if (actualPosition > index && desiredItems.Count != 2)
                {
                    for (int i = index; i < actualPosition; i++)
                        this.RemoveAt(index);

                    Debug.Assert(this.IndexOf(desiredItem) == index);
                    continue;
                }
                
                this.Insert(index, desiredItem);
            }

            while (this.Count > desiredItems.Count)
            {
                this.RemoveAt(this.Count - 1);
            }
        }
    }
}
