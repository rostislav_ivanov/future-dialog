﻿using System.Threading.Tasks;
using Windows.Storage;

namespace TheBoard.ViewModel
{
    public interface IApp
    {
        Task<StorageFile> PickSingleFileAsync();
    }
}
