﻿using System.Runtime.Serialization;

namespace TheBoard.ViewModel.Model
{
    [DataContract]
    public class ResponseBase
    {
        [DataMember(Name = "success")] public bool IsSuccess { get; set; }
        [DataMember(Name = "error")] public string Error { get; set; }
    }
}
