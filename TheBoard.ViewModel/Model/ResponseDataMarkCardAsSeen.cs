﻿using System.Runtime.Serialization;

namespace TheBoard.ViewModel.Model
{
    [DataContract]
    public class ResponseDataMarkCardAsSeen
    {
        [DataMember(Name = "new_cards_count")]
        public int NewCardsCount { get; set; }
    }
}