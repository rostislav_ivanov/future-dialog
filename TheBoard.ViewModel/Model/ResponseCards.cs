﻿using System.Collections.Generic;
using System.Runtime.Serialization;

namespace TheBoard.ViewModel.Model
{
    [DataContract]
    public class ResponseCards : ResponseBase
    {
        [DataMember(Name = "data")] public List<Card> Cards { get; set; }
    }
}
