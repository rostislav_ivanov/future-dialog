﻿using System.Collections.Generic;
using System.Runtime.Serialization;

namespace TheBoard.ViewModel.Model
{
    [DataContract]
    public class RequestSaveAnswers
    {
        [DataMember(Name = "content")] public List<CardContentAnswer> Content { get; set; }
    }
}
