﻿using System.Collections.Generic;
using System.Runtime.Serialization;

namespace TheBoard.ViewModel.Model
{
    [DataContract]
    public class Issue
    {
        [DataMember(Name = "email")]
        public string Email { get; set; }
        [DataMember(Name = "reason")]
        public string Reason { get; set; }
        [DataMember(Name = "message")]
        public string Message { get; set; }
        [DataMember(Name = "debug")]
        public byte[] Debug { get; set; }
    }
}
