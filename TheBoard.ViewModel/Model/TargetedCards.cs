﻿using System.Collections.Generic;
using System.Runtime.Serialization;

namespace TheBoard.ViewModel.Model
{
    [DataContract]
    public class TargetedCards
    {
        [DataMember(Name = "targeted_cards")]
        public List<Card> Cards { get; set; }
    }
}