﻿using System.Runtime.Serialization;

namespace TheBoard.ViewModel.Model
{
    [DataContract]
    public class ResponseMarkCardAsSeen : ResponseBase
    {
        [DataMember(Name = "data")]
        public ResponseDataMarkCardAsSeen Data { get; set; }
    }
}
