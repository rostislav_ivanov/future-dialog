﻿using System.Runtime.Serialization;

namespace TheBoard.ViewModel.Model
{
    [DataContract]
    public class CustomerData
    {
        [DataMember(Name = "customer_hash")] public string Hash { get; set; }
        [DataMember(Name = "status")] public int Status { get; set; }
        [DataMember(Name = "cards_unanswered")] public int CardsUnanswered { get; set; }
    }
}
