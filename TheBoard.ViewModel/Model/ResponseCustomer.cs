﻿using System.Runtime.Serialization;

namespace TheBoard.ViewModel.Model
{
    [DataContract]
    public class ResponseCustomer : ResponseBase
    {
        [DataMember(Name = "data")] public CustomerData Data { get; set; }
    }
}
