﻿using System.Runtime.Serialization;

namespace TheBoard.ViewModel.Model
{
    [DataContract]
    public class JsonContentItem
    {
        [DataMember(Name = "yesLabel")]     public string YesButtonText { get; set; }
        [DataMember(Name = "noLabel")]      public string NoButtonText { get; set; }

        [DataMember(Name = "okLabel")]      public string OkButtonText { get; set; }

        [DataMember(Name = "range")]        public string RangeText { get; set; }
        [DataMember(Name = "lowestText")]   public string LowestText { get; set; }
        [DataMember(Name = "middleText")]   public string MiddleText { get; set; }
        [DataMember(Name = "highestText")]  public string HighestText { get; set; }

        [DataMember(Name = "enableTextInput")]   public bool IsEnableTextInput { get; set; }
        [DataMember(Name = "inputLimit")]   public int TextAreaLimit { get; set; }

    }
}
