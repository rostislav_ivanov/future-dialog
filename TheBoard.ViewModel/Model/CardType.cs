﻿namespace TheBoard.ViewModel.Model
{
    public static class CardType
    {
        public const string TextBox = "basic";
        public const string Image = "image";
        public const string CheckBox = "checkbox";
        public const string Radio = "radio";
        public const string Range = "range";
        public const string Single = "single";
        public const string Text = "text";
    }
}

