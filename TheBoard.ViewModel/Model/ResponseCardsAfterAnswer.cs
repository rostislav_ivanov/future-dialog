﻿using System.Collections.Generic;
using System.Runtime.Serialization;

namespace TheBoard.ViewModel.Model
{
    [DataContract]
    public class ResponseCardsAfterAnswer : ResponseBase
    {
        [DataMember(Name = "data")]
        public TargetedCards TargetedCards        {
            get; set;
        }
    }
}
