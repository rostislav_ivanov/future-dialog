﻿using System.Collections.Generic;
using System.Runtime.Serialization;

namespace TheBoard.ViewModel.Model
{
    [DataContract]
    public class Card
    {
        [DataMember(Name = "hash")] public string Hash { get; set; }
        [DataMember(Name = "question")] public string Question { get; set; }
        [DataMember(Name = "description")] public string Description { get; set; }
        [DataMember(Name = "created_at")] public string CreatedAt { get; set; }
        [DataMember(Name = "can_skip")] public bool CanSkip { get; set; }
        
        [DataMember(Name = "promoted")] public bool IsPromoted { get; set; }
        //[DataMember(Name = "instant")] public int Instant { get; set; }
        //[DataMember(Name = "ongoing")] public string Ongoing { get; set; }
        //[DataMember(Name = "type_id")] public int TypeId { get; set; }

        [DataMember(Name = "type")] public string Type { get; set; }
        [DataMember(Name = "targeted")] public bool Targeted { get; set; }
        
        [DataMember(Name = "content")] public List<CardContent> Content { get; set; }
    }
}
