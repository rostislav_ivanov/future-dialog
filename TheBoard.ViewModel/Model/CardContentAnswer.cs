﻿using System.Runtime.Serialization;

namespace TheBoard.ViewModel.Model
{
    [DataContract]
    public class CardContentAnswer
    {
        [DataMember(Name = "card_hash")] public string CardHash { get; set; }
        [DataMember(Name = "content_hash")] public string CardContentHash { get; set; }
        [DataMember(Name = "data")] public string Data { get; set; }
    }
}
