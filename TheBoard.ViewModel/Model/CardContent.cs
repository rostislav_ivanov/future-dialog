﻿using System.Runtime.Serialization;

namespace TheBoard.ViewModel.Model
{
    [DataContract]
    public class CardContent
    {
        [DataMember(Name = "content_hash")] public string Hash { get; set; }
        [DataMember(Name = "title")] public string Title { get; set; }
        [DataMember(Name = "priority")] public int? Priority { get; set; }
        [DataMember(Name = "category")] public string Category { get; set; }
    }
}
