﻿using System.Linq;
using System;
using System.Threading.Tasks;
using Windows.ApplicationModel.Background;
using Windows.UI.Notifications;
using Windows.Devices.Geolocation;
using Windows.Devices.Geolocation.Geofencing;


namespace TheBoard.ViewModel
{
    public static class BackgroundAgentsHelper
    {
        private const string TaskEntryPoint = "TheBoard.BackgroundAgent.SendNewLocation";
        private const string GeolocationChangesTask = "GeolocationChangesTask";

        public static async Task RegisterUpdateGeolocationAgent()
        {
            foreach (var task in BackgroundTaskRegistration.AllTasks)
            {
                var taskName = task.Value.Name;
                if (taskName == GeolocationChangesTask)
                    return;
            }

            try
            {
                await BackgroundExecutionManager.RequestAccessAsync();

                Geolocator locator = new Geolocator();
                if (locator.LocationStatus == PositionStatus.Disabled)
                {
                    return;
                }
                locator.DesiredAccuracyInMeters = 10;
                locator.DesiredAccuracy = PositionAccuracy.High;

                Geoposition currentPosition = await locator.GetGeopositionAsync(TimeSpan.FromMinutes(1), TimeSpan.FromSeconds(30));
                Geocircle fenceCircle = new Geocircle(currentPosition.Coordinate.Point.Position, 50);
                //currentPosition.Coordinate.Point.Position.Longitude + " " + currentPosition.Coordinate.Point.Position.Latitude
                Geofence newFence = new Geofence(currentPosition.Coordinate.Point.Position.GetHashCode().ToString(), fenceCircle, MonitoredGeofenceStates.Exited, false, TimeSpan.FromSeconds(1), DateTimeOffset.Now, TimeSpan.FromDays(30));

                GeofenceMonitor.Current.Geofences.Add(newFence);

                var builder = new BackgroundTaskBuilder { Name = GeolocationChangesTask, TaskEntryPoint = TaskEntryPoint };
                builder.SetTrigger(new LocationTrigger(LocationTriggerType.Geofence));
                builder.AddCondition(new SystemCondition(SystemConditionType.InternetAvailable));
                builder.Register();
            }
            catch(Exception e)
            { }
        }

    }
}
