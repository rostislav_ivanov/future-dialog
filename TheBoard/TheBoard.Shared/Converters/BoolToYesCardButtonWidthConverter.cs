﻿using System;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Data;

namespace TheBoard.Converters
{
    public class BoolToYesCardButtonWidthConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, string language)
        {
            return value == null ? 480 : 182;
        }

        public object ConvertBack(object value, Type targetType, object parameter, string language)
        {
            throw new NotSupportedException();
        }
    }
}
