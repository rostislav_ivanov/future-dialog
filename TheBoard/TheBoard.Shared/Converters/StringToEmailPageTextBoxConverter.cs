﻿using System;
using Windows.UI;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Media;

namespace TheBoard.Converters
{
    public sealed class StringToEmailPageTextBoxConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, string language)
        {
            return String.IsNullOrEmpty((string)value) ? Application.Current.Resources["LoginInactiveGreyBrush"] as SolidColorBrush : 
                   Application.Current.Resources["WhiteBrush"] as SolidColorBrush; 
        }

        public object ConvertBack(object value, Type targetType, object parameter, string language)
        {
            throw new NotSupportedException();
        }
    }
}
