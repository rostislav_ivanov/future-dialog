﻿using System;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Data;

namespace TheBoard.Converters
{
    public class StringToSymbolIcon : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, string language)
        {
            return (Symbol)value;
        }

        public object ConvertBack(object value, Type targetType, object parameter, string language)
        {
            throw new NotSupportedException();
        }
    }
}
