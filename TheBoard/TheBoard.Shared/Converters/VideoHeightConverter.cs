﻿using System;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Data;

namespace TheBoard.Converters
{
    public sealed class VideoHeightConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, string language)
        {
            return (value == null) ? 0 : 260;
        }

        public object ConvertBack(object value, Type targetType, object parameter, string language)
        {
            throw new NotSupportedException();
        }
    }
}
