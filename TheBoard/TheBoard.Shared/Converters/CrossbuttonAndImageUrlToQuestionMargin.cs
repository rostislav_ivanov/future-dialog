﻿using System;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Data;

namespace TheBoard.Converters
{
    public class CrossbuttonAndImageUrlToQuestionMargin : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, string language)
        {
            return (bool)value ? new Thickness(12, 12, 12, 0) : new Thickness(12, 43, 43, 0);
        }

        public object ConvertBack(object value, Type targetType, object parameter, string language)
        {
            throw new NotSupportedException();
        }
    }
}
