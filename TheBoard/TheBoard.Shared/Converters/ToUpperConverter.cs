﻿using System;
using Windows.UI.Xaml.Data;

namespace TheBoard.Converters
{
    public class ToUpperConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, string language)
        {
            if (String.IsNullOrEmpty(value as string)) return null;
            return ((string) value).ToUpper();
        }

        public object ConvertBack(object value, Type targetType, object parameter, string language)
        {
            throw new NotSupportedException();
        }
    }
}
