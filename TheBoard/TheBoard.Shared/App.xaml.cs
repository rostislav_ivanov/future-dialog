﻿using System;
using System.Diagnostics;
using System.Threading.Tasks;
using Windows.ApplicationModel;
using Windows.ApplicationModel.Activation;
using Windows.Storage;
using Windows.Storage.Pickers;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Media.Animation;
using Windows.UI.Xaml.Navigation;
using MetroLab.Common;
using TheBoard.View;
using TheBoard.ViewModel;
using Windows.UI.Core;
using Windows.UI;
using Windows.UI.ViewManagement;
using System.Collections.Generic;
using TheBoard.ViewModel.View;

namespace TheBoard
{
    public sealed partial class App : IApp
    {
#if WINDOWS_PHONE_APP
        private TransitionCollection transitions;
        private TaskCompletionSource<StorageFile> _fileOpenOrSavePickerTaskCompletionSource;
#endif

        private string DeeplinkPrefix = "tampere3dialog://magic/";

        public App()
        {
            InitializeComponent();

            UnhandledException += CurrentUnhandledExceptionAsync;
            Suspending += OnSuspending;
        }

        public async Task<StorageFile> PickSingleFileAsync()
        {
            var picker = new FileOpenPicker { ViewMode = PickerViewMode.Thumbnail,
                SuggestedStartLocation = PickerLocationId.PicturesLibrary,
                FileTypeFilter = { ".bmp", ".png", ".jpeg", ".jpg" }
            };

#if WINDOWS_APP
            return await picker.PickSingleFileAsync();
#else 
            _fileOpenOrSavePickerTaskCompletionSource = new TaskCompletionSource<StorageFile>();
            await (await AppViewModel.GetMainAsync()).RunAtDispatcherAsync(picker.PickSingleFileAndContinue);
            return await _fileOpenOrSavePickerTaskCompletionSource.Task;
#endif
        }

        private Frame CreateRootFrame()
        {
            Frame rootFrame = Window.Current.Content as Frame;

            // Do not repeat app initialization when the Window already has content,
            // just ensure that the window is active
            if (rootFrame == null)
            {
                // Create a Frame to act as the navigation context and navigate to the first page
                rootFrame = new Frame();

                // TODO: change this value to a cache size that is appropriate for your application
                rootFrame.CacheSize = 1;

                // Place the frame in the current Window
                Window.Current.Content = rootFrame;
            }

            if (rootFrame.Content == null)
            {
                // Removes the turnstile navigation for startup.
                if (rootFrame.ContentTransitions != null)
                {
                    this.transitions = new TransitionCollection();
                    foreach (var c in rootFrame.ContentTransitions)
                    {
                        this.transitions.Add(c);
                    }
                }

                rootFrame.ContentTransitions = null;
                rootFrame.Navigated += this.RootFrame_FirstNavigated;
            }

            return rootFrame;
        }

        protected override void OnLaunched(LaunchActivatedEventArgs e)
        {
            /*Window.Current.Dispatcher.RunAsync(CoreDispatcherPriority.Low, async () =>
            {
                ApplicationView.GetForCurrentView().SetDesiredBoundsMode(ApplicationViewBoundsMode.UseCoreWindow);
            });*/

            Frame rootFrame = CreateRootFrame();

            var agent = AppServerAgent.Current;

            agent.SaveAccessTokenForLogin(new KeyValuePair<string, string>("device_id", AppServerAgent.DeviceId));

            //if (agent.LoadCustomerHash() != null)
            {
                if (!rootFrame.Navigate(typeof(StartPage), e.Arguments))
                {
                    throw new Exception("Failed to create initial page");
                }
            }

            
            if (MainPage.Current != null && MainPage.Current.DataContext != null)
            {
                MainPageViewModel viewmodel = ((MainPageViewModel)MainPage.Current.DataContext);

                Window.Current.Dispatcher.RunAsync(CoreDispatcherPriority.Normal,
                    async () => await viewmodel.LoadDataAsync(viewmodel.IsNotSavedCards));
            }

            //ToDo: for apps with Login using Facebook or email
            /*else
            {
                if (!rootFrame.Navigate(typeof(LoginPage), e.Arguments))
                {
                    throw new Exception("Failed to create initial page");
                }
            }*/

            Window.Current.Dispatcher.RunAsync(CoreDispatcherPriority.Normal,
               async () => await BackgroundAgentsHelper.RegisterUpdateGeolocationAgent());


            //ToDo: only for apps with registration through device 
            //Window.Current.Dispatcher.RunAsync(CoreDispatcherPriority.Normal,
            // async () => await AppServerAgent.Current.GetCustomerDataAsync());
            // Ensure the current window is active
            Window.Current.Activate();
        }

        protected override async void OnActivated(IActivatedEventArgs args)
        {
            Frame rootFrame = CreateRootFrame();

            var continuationEventArgs = args as IContinuationActivatedEventArgs;
            if (continuationEventArgs != null)
            {
                var continuationManager = new ContinuationManager();
                continuationManager.Continue(continuationEventArgs);
            }

            //Login user with the deeplink
            var protocolArgs = args as ProtocolActivatedEventArgs;
            if (protocolArgs != null && protocolArgs.Uri != null)
            {
                if (protocolArgs.Uri.OriginalString.StartsWith(DeeplinkPrefix))
                {
                    var agent = AppServerAgent.Current;
                    if (agent.LoadCustomerHash() == null)
                    {
                        //"email_code" - this string go to request !
                        agent.SaveAccessTokenForLogin(new KeyValuePair<string, string>("email_code", protocolArgs.Uri.OriginalString.Replace(DeeplinkPrefix, "")));
                        rootFrame.Navigate(typeof(StartPage), args);
                    }
                    else
                    {
                        //ToDo: if user come to app from deeplink but he is already signed in
                        rootFrame.Navigate(typeof(StartPage), args);
                    }
                }
                else if (protocolArgs.Uri.OriginalString.StartsWith(""))
                {
                    LoginPage.Current.ContinueAppAuthentication(Uri.UnescapeDataString(protocolArgs.Uri.OriginalString));
                }
            }

#if WINDOWS_PHONE_APP
            await Window.Current.Dispatcher.RunAsync(CoreDispatcherPriority.Normal,
               async () =>
               {
                   if (args.Kind == ActivationKind.PickFileContinuation && _fileOpenOrSavePickerTaskCompletionSource != null)
                   {
                       var fileOpenPickerArgs = args as FileOpenPickerContinuationEventArgs;

                       if (fileOpenPickerArgs != null)
                       {
                           var pickedFile = fileOpenPickerArgs.Files.Count > 0 ? fileOpenPickerArgs.Files[0] : null;
                           _fileOpenOrSavePickerTaskCompletionSource.TrySetResult(pickedFile);
                       }

                       _fileOpenOrSavePickerTaskCompletionSource = null;
                   }
                   else if (args.Kind == ActivationKind.PickSaveFileContinuation && _fileOpenOrSavePickerTaskCompletionSource != null)
                   {
                       var fileSavePickerArgs = args as FileSavePickerContinuationEventArgs;

                       if (fileSavePickerArgs != null)
                       {
                           var pickedFile = fileSavePickerArgs.File;
                           _fileOpenOrSavePickerTaskCompletionSource.TrySetResult(pickedFile);
                       }

                       _fileOpenOrSavePickerTaskCompletionSource = null;
                   }
               });
#endif
            Window.Current.Activate();
        }

#if WINDOWS_PHONE_APP
        /// <summary>
        /// Restores the content transitions after the app has launched.
        /// </summary>
        /// <param name="sender">The object where the handler is attached.</param>
        /// <param name="e">Details about the navigation event.</param>
        private void RootFrame_FirstNavigated(object sender, NavigationEventArgs e)
        {
            var rootFrame = sender as Frame;
            rootFrame.ContentTransitions = this.transitions ?? new TransitionCollection() { new NavigationThemeTransition() };
            rootFrame.Navigated -= this.RootFrame_FirstNavigated;
        }
#endif

        /// <summary>
        /// Invoked when application execution is being suspended.  Application state is saved
        /// without knowing whether the application will be terminated or resumed with the contents
        /// of memory still intact.
        /// </summary>
        /// <param name="sender">The source of the suspend request.</param>
        /// <param name="e">Details about the suspend request.</param>
        private void OnSuspending(object sender, SuspendingEventArgs e)
        {
            var deferral = e.SuspendingOperation.GetDeferral();

            // TODO: Save application state and stop any background activity
            deferral.Complete();
        }

        private void CurrentUnhandledExceptionAsync(object sender, UnhandledExceptionEventArgs e)
        {
            e.Handled = false;
#if DEBUG
            if (Debugger.IsAttached) Debugger.Break();
            ShowErrorMessage(e.Exception != null ? e.Exception.StackTrace : String.Empty, e.Message);
#else 
            //ShowErrorMessage(LocalizedResources.GetString("msg_GeneralUnhandledException"),
                //LocalizedResources.GetString("txt_Error"));
#endif
        }

        private async void ShowErrorMessage(string content, string title)
        {
            await BaseViewModel.ShowDialogAsync(content, title);
            Current.Exit();
        }
    }
}