﻿using System;
using Windows.Foundation;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Media.Animation;
using TheBoard.ViewModel;
using TheBoard.ViewModel.View.Cards;
using Windows.UI.Input;

namespace TheBoard.Controls
{
    [TemplateVisualState(GroupName = "Common", Name = "Normal")]
    [TemplateVisualState(GroupName = "Common", Name = "Disabled")]
    public class DraggableControl :  ContentControl
    {
        private bool _dragging;
        private Windows.UI.Xaml.Media.TranslateTransform _translateTransform;

        public DraggableControl()
        {
            DefaultStyleKey = typeof(DraggableControl);
            VisualStateManager.GoToState(this, "Normal", true);
            _translateTransform = new Windows.UI.Xaml.Media.TranslateTransform();
            this.RenderTransform = _translateTransform;
            ManipulationMode = ManipulationModes.TranslateX | ManipulationModes.TranslateY;
            IsEnabledChanged += DraggableControl_IsEnabledChanged;

            this.Loaded += DraggableControl_Loaded;
        }

        public void ResetRenderTransform()
        {
            _translateTransform = new Windows.UI.Xaml.Media.TranslateTransform();
            this.RenderTransform = _translateTransform;
        }

        private void DraggableControl_ManipulationStarted(object sender, ManipulationStartedRoutedEventArgs e)
        {
            //this.CacheMode = new BitmapCache();
        }

        private double GetDistance(double x, double y)
        {
            return Math.Sqrt(x * x + y * y);
        }

        private void AnimateToOriginalPosition()
        {
            var storyboard = new Storyboard { Duration = TimeSpan.FromMilliseconds(500) };
            Storyboard.SetTarget(storyboard, _translateTransform);

            {
                var positionXAnimation = new DoubleAnimation
                {
                    To = 0,
                    Duration = new Duration(TimeSpan.FromMilliseconds(220)),
                    AutoReverse = false,
                    EnableDependentAnimation = true,
                    FillBehavior = FillBehavior.HoldEnd,
                    EasingFunction = new QuadraticEase { EasingMode = EasingMode.EaseOut }
                };
                Storyboard.SetTargetProperty(positionXAnimation, "X");
                storyboard.Children.Add(positionXAnimation);
            }
            {
                var positionYAnimation = new DoubleAnimation
                {
                    To = 0,
                    Duration = new Duration(TimeSpan.FromMilliseconds(220)),
                    AutoReverse = false,
                    EnableDependentAnimation = true,
                    FillBehavior = FillBehavior.HoldEnd,
                    EasingFunction = new QuadraticEase { EasingMode = EasingMode.EaseOut }
                };
                Storyboard.SetTargetProperty(positionYAnimation, "Y");
                storyboard.Children.Add(positionYAnimation);
            }

            EventHandler<object> completed = null;
            completed = (s, o) =>
            {
                //this.CacheMode = null;
                var typedSender = (Storyboard)s;

                _translateTransform.X = 0;
                _translateTransform.Y = 0;
                typedSender.Completed -= completed;
                typedSender.Stop();
                IsAnimationStarted = false;
            };

            storyboard.Completed += completed;
            storyboard.Begin();
        }

        private void AnimateRemoving(Point currentTranslation)
        {
            var bounds = Window.Current.Bounds;
            double screenWidth = bounds.Width;
            double screenHeight = bounds.Height;
            var screenDiagonal = GetDistance(screenWidth, screenHeight);
            var pointDistance = GetDistance(currentTranslation.X, currentTranslation.Y);
            var normalizedPoint = new Point(currentTranslation.X / pointDistance, currentTranslation.Y / pointDistance);
            var destinationX = normalizedPoint.X * screenDiagonal;
            var destinationY = normalizedPoint.Y * screenDiagonal;

            //if (this.CacheMode == null)
            //    this.CacheMode = new BitmapCache();
            var storyboard = new Storyboard { Duration = TimeSpan.FromMilliseconds(500) };
            Storyboard.SetTarget(storyboard, _translateTransform);

            {
                var positionXAnimation = new DoubleAnimation
                {
                    To = destinationX,
                    Duration = new Duration(TimeSpan.FromMilliseconds(400)),
                    AutoReverse = false,
                    EnableDependentAnimation = true,
                    FillBehavior = FillBehavior.HoldEnd,
                    EasingFunction = new SineEase { EasingMode = EasingMode.EaseIn }
                };
                Storyboard.SetTargetProperty(positionXAnimation, "X");
                storyboard.Children.Add(positionXAnimation);
            }
            {
                var positionYAnimation = new DoubleAnimation
                {
                    To = destinationY,
                    Duration = new Duration(TimeSpan.FromMilliseconds(400)),
                    AutoReverse = false,
                    EnableDependentAnimation = true,
                    FillBehavior = FillBehavior.HoldEnd,
                    EasingFunction = new SineEase { EasingMode = EasingMode.EaseIn }
                };
                Storyboard.SetTargetProperty(positionYAnimation, "Y");
                storyboard.Children.Add(positionYAnimation);
            }

            EventHandler<object> completed = null;
            completed = (s, o) =>
            {
                //this.CacheMode = null;
                var typedSender = (Storyboard)s;

                _translateTransform.X = destinationX;
                _translateTransform.Y = destinationY;

                var item = this.DataContext as IRemovable;
                if (item != null)
                    item.Remove();
                typedSender.Completed -= completed;
                typedSender.Stop();
                IsAnimationStarted = false;
            };

            storyboard.Completed += completed;
            storyboard.Begin();
        }

        public bool IsAnimationStarted;
        private void DraggableControl_ManipulationCompleted(object sender, ManipulationCompletedRoutedEventArgs e)
        {
            if (!_dragging) return;

            IsAnimationStarted = true;
            var currentPoint = e.Cumulative.Translation;
            var shift = GetDistance(currentPoint.X, currentPoint.Y);

            var bounds = Window.Current.Bounds;
            double screenWidth = bounds.Width;

            CardBaseViewModel сardBaseViewModel = (CardBaseViewModel)this.DataContext;

            if (shift < screenWidth * 0.42 || сardBaseViewModel.IsOnlyOneLeftCard)
            {   
                AnimateToOriginalPosition();
            }
            else
            {
                AnimateRemoving(currentPoint);
            }
        }

        private void DraggableControl_Loaded(object sender, RoutedEventArgs e)
        { 
            this.AddHandler(DraggableControl.PointerPressedEvent,
                            new PointerEventHandler(DraggableControl_PointerPressed), true);
            
            AddHandler(DraggableControl.PointerReleasedEvent,
                        new PointerEventHandler(DraggableControl_PointerReleased), true);

            AddHandler(DraggableControl.ManipulationStartedEvent,
                        new ManipulationStartedEventHandler(DraggableControl_ManipulationStarted), true);
            
            AddHandler(DraggableControl.ManipulationDeltaEvent,
                        new ManipulationDeltaEventHandler(DraggableControl_ManipulationDelta), true);
            
            AddHandler(DraggableControl.ManipulationCompletedEvent,
                        new ManipulationCompletedEventHandler(DraggableControl_ManipulationCompleted), true);

            if (IsEnabled)
            {
                Current = this;
            }
            VisualStateManager.GoToState(this, IsEnabled ? "Normal" : "Disabled", true);
        }


        private void DraggableControl_ManipulationDelta(object sender, ManipulationDeltaRoutedEventArgs e)
        {
            if (!_dragging) return;

            var currentPoint = e.Cumulative.Translation;
            _translateTransform.X = currentPoint.X;
            _translateTransform.Y = currentPoint.Y;
        }

        private void DraggableControl_PointerPressed(object sender, PointerRoutedEventArgs e)
        {
            _dragging = true;
        }

        private void DraggableControl_PointerReleased(object sender, PointerRoutedEventArgs e)
        {
            _dragging = false;
        }

        public static DraggableControl Current;
        private void DraggableControl_IsEnabledChanged(object sender, DependencyPropertyChangedEventArgs args)
        {
            if (IsEnabled)
            {
                Current = this;
            }
            VisualStateManager.GoToState(this, IsEnabled ? "Normal" : "Disabled", true);
        }
    }
}
