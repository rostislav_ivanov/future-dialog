﻿using System;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using TheBoard.ViewModel.Model;
using TheBoard.ViewModel.View.Cards;

namespace TheBoard.TemplateSelectors
{
    public class CardTemplateSelector : DataTemplateSelector
    {
        public DataTemplate TextBoxTemplate { get; set; }
        public DataTemplate ImageTemplate { get; set; }
        public DataTemplate CheckBoxTemplate { get; set; }
        public DataTemplate RadioTemplate { get; set; }
        public DataTemplate RangeTemplate { get; set; }
        public DataTemplate SingleTemplate { get; set; }
        public DataTemplate TextTemplate { get; set; }

        protected override DataTemplate SelectTemplateCore(object item, DependencyObject container)
        {
            var card = item as CardBaseViewModel;
            if (card == null) return null;

            switch (card.CardType)
            {
                case CardType.TextBox: return TextBoxTemplate;
                case CardType.Image: return ImageTemplate;
                case CardType.CheckBox: return CheckBoxTemplate;
                case CardType.Radio: return RadioTemplate;
                case CardType.Range: return RangeTemplate;
                case CardType.Single: return SingleTemplate;
                case CardType.Text: return TextTemplate;

                default: throw new Exception();
            }
        }
    }
}
