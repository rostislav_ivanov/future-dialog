﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net.Http;
using System.Runtime.Serialization.Json;
using System.Text;
using System.Threading.Tasks;
using TheBoard.ViewModel;
using TheBoard.ViewModel.Model;
using Windows.Security.Cryptography.Core;
using Windows.System.UserProfile;

namespace TheBoard
{
    public class TempServerAgent
    {
        private readonly HttpClient _httpClient;
        private const int CountLoadingAttempts = 5;
        private static readonly string DeviceId;

        private const string ApiDomain = "http://app.theboard.pro/api/";
        private const string UrlGetCustomerData = "customer";
        private const string UrlGetCardsData = "cards";
        private const string UrlRegisterDevice = "customer/register_device";

        static TempServerAgent()
        {
            var token = HardwareIdentification.GetPackageSpecificToken(null);
            var hardwareId = token.Id;
            using (var dataReader = DataReader.FromBuffer(hardwareId))
            {
                var bytes = new byte[hardwareId.Length];
                dataReader.ReadBytes(bytes);
                var buffer = CryptographicBuffer.CreateFromByteArray(bytes);

                var md5Provider = HashAlgorithmProvider.OpenAlgorithm(HashAlgorithmNames.Md5);
                var md5Hash = md5Provider.HashData(buffer).ToArray();
                DeviceId = BitConverter.ToString(md5Hash).Replace("-", "").ToLower();
            }
        }

        private TempServerAgent()
        {
            _httpClient = new HttpClient();
            var headers = _httpClient.DefaultRequestHeaders;
            headers.Add("x-board-appid", "1182a315ee7e37bf3b9141ec54293e8d");
        }

        private void SetAuthorization(string customerHash)
        {
            var headers = _httpClient.DefaultRequestHeaders;
            if (headers.Contains("Authorization")) headers.Remove("Authorization");
            headers.TryAddWithoutValidation("Authorization", string.Format("CUSTOMER {0}", customerHash));
        }

        private static AppServerAgent _current;
        public static AppServerAgent Current { get { return _current ?? (_current = new AppServerAgent()); } }

        public async Task<ResponseCustomer> RegisterDeviceAsync()
        {
            Func<HttpContent> getContentFunc = () => new FormUrlEncodedContent(new[] {
                new KeyValuePair<string, string>("device_token", DeviceId),
                new KeyValuePair<string, string>("device_type", "3"),
                new KeyValuePair<string, string>("language", 
                    GlobalizationPreferences.Languages[0].Substring(0, 2).ToLower())
            });

            var result = await SendRequestAsync<ResponseCustomer>(UrlRegisterDevice,
                HttpMethod.Post, getContentFunc);
            //if (result.IsSuccess && result.Data != null && !string.IsNullOrEmpty(result.Data.Hash))
            //    SetAuthorization(result.Data.Hash);
            return result;
        }

        public async Task<ResponseCustomer> GetCustomerDataAsync()
        {
            Func<HttpContent> getContentFunc = () => new FormUrlEncodedContent(new[] {
                new KeyValuePair<string, string>("device_id", DeviceId),
                new KeyValuePair<string, string>("device_type", "3"),
                new KeyValuePair<string, string>("language", 
                    GlobalizationPreferences.Languages[0].Substring(0, 2).ToLower())
            });

            var result = await SendRequestAsync<ResponseCustomer>(UrlGetCustomerData,
                HttpMethod.Post, getContentFunc);
            if (result.IsSuccess && result.Data != null && !string.IsNullOrEmpty(result.Data.Hash)) 
                SetAuthorization(result.Data.Hash); 
            return result;
        }

        public async Task<ResponseBase> SaveAnswerAsync(RequestSaveAnswers request)
        {            
            Func<HttpContent> getContentFunc = () => {
                using (var stream = new MemoryStream())
                {
                    var serializer = new DataContractJsonSerializer(typeof(RequestSaveAnswers));
                    serializer.WriteObject(stream, request);
                    stream.Position = 0;
                    using (var reader = new StreamReader(stream))
                        return new StringContent(reader.ReadToEnd());
                }                
            };

            return await SendRequestAsync<ResponseBase>(UrlGetCardsData,
                HttpMethod.Post, getContentFunc);
        }

        public async Task<ResponseCards> GetCardsDataAsync(string customerHash)
        {
            return await SendRequestAsync<ResponseCards>(UrlGetCardsData, HttpMethod.Get);
        }

        private async Task<T> SendRequestAsync<T>(string url, HttpMethod httpMethod,
            Func<HttpContent> getContentFunc = null) where T : ResponseBase, new()
        {
            T result;
            var attempt = 1;

            while (true)
            {
                try
                {
                    var requestMessage = new HttpRequestMessage(httpMethod, 
                        string.Format("{0}{1}", ApiDomain, url));

                    if (getContentFunc != null)
                    {
                        var httpContent = getContentFunc();
                        if (httpContent != null ) requestMessage.Content = httpContent;
                    }

                    var responseMessage = await _httpClient.SendAsync(requestMessage,
                        HttpCompletionOption.ResponseContentRead);

                    var serializer = new DataContractJsonSerializer(typeof (T));
                    using (var responseStream = await responseMessage.Content.ReadAsStreamAsync())
                    {
                        if (responseMessage.StatusCode == HttpStatusCode.NotFound)
                            throw new NoInternetException();

                        if (responseStream.Length == 0) responseMessage.EnsureSuccessStatusCode();
                        result = (T) serializer.ReadObject(responseStream);
                    }
                    
                    break;
                }
                catch (NoInternetException ex)
                {
                    if (Debugger.IsAttached) Debugger.Break();

                    if (attempt < CountLoadingAttempts)
                    {
                        attempt++;
                        continue;
                    }

                    result = new T { Error = "No internet", IsSuccess = false }; 
                    break;
                }
                catch (Exception ex)
                {
                    if (Debugger.IsAttached) Debugger.Break();
#if DEBUG
                    result = new T { Error = ex.ToString(), IsSuccess = false };
#else
                    result = new T { Error = "Something went wrong", IsSuccess = false };
#endif
                    break;
                }                
            }

            return result;
        }
    }
}
