﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using TheBoard.View;
using TheBoard.ViewModel.View.Cards;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Navigation;

namespace TheBoard.Controls
{
    public sealed partial class RadioButtonList : UserControl
    {
        public RadioButtonList()
        {
            this.InitializeComponent();
        }

        private void RadioButton_Checked(object sender, RoutedEventArgs e)
        {
            if (((CardBaseViewModel)this.DataContext).IsNeedToExpand)
            {
                MainPage.Current.Expand();
            }

            ((CardBaseViewModel)this.DataContext).OnPropertyChanged("HasAnswer");
        }
    }
}
