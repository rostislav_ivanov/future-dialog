﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using System.Windows.Input;
using TheBoard.View;
using TheBoard.ViewModel.View.Cards;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.UI.Input;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Navigation;

namespace TheBoard.Controls
{
    public sealed partial class ImageUploadControl : UserControl
    {
        public ImageUploadControl()
        {
            this.InitializeComponent();
        }

        private void Button_Tapped(object sender, TappedRoutedEventArgs e)
        {
            e.Handled = true;
            if (((CardBaseViewModel)this.DataContext).IsNeedToExpand)
            {
                MainPage.Current.Expand();
            }
            ((CardImageViewModel)this.DataContext).GetImage(true);
        }

        private void Button_Tapped_1(object sender, TappedRoutedEventArgs e)
        {
            e.Handled = true;
            if (((CardBaseViewModel)this.DataContext).IsNeedToExpand)
            {
                MainPage.Current.Expand();
            }
            ((CardImageViewModel)this.DataContext).GetImage(false);
        }
    }
}
