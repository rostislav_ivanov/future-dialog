﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using TheBoard.View;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.Phone.UI.Input;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Navigation;

namespace TheBoard.Controls
{
    public sealed partial class FullScreenWebView : UserControl
    {
        public static FullScreenWebView VideoOnCard;

        public FullScreenWebView()
        {
            this.InitializeComponent();

            (this.Content as FrameworkElement).DataContext = this;
        }

        public static readonly DependencyProperty VideoUrlProperty =
            DependencyProperty.Register("VideoUrl", typeof(string), typeof(FullScreenWebView),
            new PropertyMetadata(null, OnOrOffPropertyChnaged));

        public static void OnOrOffPropertyChnaged(DependencyObject d, DependencyPropertyChangedEventArgs e)
        {
            if (d == null || e == null || e.NewValue == null)
                return;

            FullScreenWebView fullScreenWebView = (FullScreenWebView)d;

            fullScreenWebView.webView.Source = new Uri((string)e.NewValue);
        }

        public string VideoUrl
        {
            get { return (string)GetValue(VideoUrlProperty); }
            set {
                SetValue(VideoUrlProperty, value);
                
            }
        }

        public WebView WebViewForFullscreen; 

        private void WebView_ContainsFullScreenElementChanged(WebView sender, object args)
        {
            if (sender.ActualWidth < 1)
                return;

            if (LayoutRoot.Children.Count == 1)
            {
                VideoOnCard = this;
                WebViewForFullscreen = sender;
                this.LayoutRoot.Children.Remove(sender);

                Frame rootFrame = Window.Current.Content as Frame;
                rootFrame.Navigate(typeof(FullscreenVideoPage), this);
            }
            else if(LayoutRoot.Children.Count == 0)
            {
                LoadVideoOnCard();
            }
        }

        public void LoadVideoOnCard()
        {
            WebView webview = FullscreenVideoPage.Current.GetFullScreenVideo();
            this.LayoutRoot.Children.Add(webview);
        }

        public void LoadVideoFromFullscreen(WebView webViewForFullscreen)
        {
            this.LayoutRoot.Children.Add(webViewForFullscreen);
        }
    }


}
