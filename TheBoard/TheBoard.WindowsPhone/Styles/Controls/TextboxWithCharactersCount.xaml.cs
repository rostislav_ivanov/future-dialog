﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using TheBoard.View;
using TheBoard.ViewModel.View.Cards;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.UI.Input;
using Windows.UI.ViewManagement;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Navigation;

namespace TheBoard.Controls
{
    public sealed partial class TextboxWithCharactersCount : UserControl
    {
        public TextboxWithCharactersCount()
        {
            this.InitializeComponent();
        }

        private void TextBox_KeyDown(object sender, KeyRoutedEventArgs e)
        {
            TextBox textbox = (TextBox)sender;

            string textWithoutReturn = textbox.Text.Replace("\r\n", "\0");

            CountOfCharacters.Text = textWithoutReturn.Length.ToString();
        }

        private void AreaTextBox_GotFocus(object sender, RoutedEventArgs e)
        {
            if (((CardBaseViewModel)this.DataContext).IsNeedToExpand)
            {
                MainPage.Current.Expand();
            }
        }

        private void AreaTextBox_PointerExited(object sender, PointerRoutedEventArgs e)
        {
            DraggableControl card = DraggableControl.Current;

            if (card.IsAnimationStarted)
            {
                ButtonToUnfocusTextBoxAndHideKeyboard.Focus(FocusState.Programmatic);
            }
        }
    }
}
