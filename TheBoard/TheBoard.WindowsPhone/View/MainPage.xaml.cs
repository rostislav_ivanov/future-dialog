﻿using MetroLab.Common;
using System;
using TheBoard.Controls;
using TheBoard.Localization;
using TheBoard.ViewModel;
using TheBoard.ViewModel.View;
using TheBoard.ViewModel.View.Cards;
using Windows.Phone.UI.Input;
using Windows.UI.Popups;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Media.Animation;
using Windows.UI.Xaml.Navigation;

namespace TheBoard.View
{
    public sealed partial class MainPage 
    {
        public bool _isFullScreen;
        public DraggableControl fullScreenCard;
        public ScrollViewer scrollViewer;
        public static MainPage Current { get; private set; }

        public MainPage()
        {
            InitializeComponent();
            
            TxtBlockFinishHeader.Text = LocalizedResources.GetString("mark_thank_you_").ToUpper();
            TxtBlockFinishMessage.Text = LocalizedResources.GetString("stack_empty_thank_you_description_");

            //Porukka
            //TxtBlockEmpty.Text = "Kohta tulee lisää!";

            TxtBlockEmpty.Text =  LocalizedResources.GetString("stack_empty_");

            Current = this;
        }

        public void ScrollToCardBegin()
        { 
            scrollViewer.ChangeView(0, 0, (float)1.0);
        }

        public void WebView_Loaded(object sender, RoutedEventArgs e)
        {
            ((WebView)sender).Navigate(new Uri((string)((WebView)sender).Tag));
        }

        protected override async void OnNavigatedTo(NavigationEventArgs e)
        {
            bool parameter = (bool)e.Parameter;

                this.DataContext = new MainPageViewModel(parameter);

            base.OnNavigatedTo(e);
        }

        protected override void OnNavigatedFrom(NavigationEventArgs e)
        {
            var cards = ((MainPageViewModel)this.DataContext).Cards;
            if (cards != null && cards.Count > 0)
            {
                CardBaseViewModel card = cards[cards.Count - 1];
                if(card is CardTextViewModel)
                    ((CardTextViewModel)card).PauseVideo();
            }
            base.OnNavigatedFrom(e);
        }

        private void Card_SizeChanged(object sender, SizeChangedEventArgs e)
        {
            Grid cardContentControl = (Grid)((ContentControl)sender).Parent;
            double cardRealHeight = cardContentControl.ActualHeight;
            double cardMaximumHeight = ((ScrollViewer)cardContentControl.Parent).ActualHeight;

            CardBaseViewModel _cardBaseViewModel = (CardBaseViewModel)cardContentControl.DataContext;

            if (!_cardBaseViewModel.IsFullScreen)
            {
                _cardBaseViewModel.IsNeedToExpand = cardRealHeight > cardMaximumHeight ? true : false;
            }
        }

        public void Expand()
        {
            DraggableControl card = DraggableControl.Current;

            if (card.IsAnimationStarted) return;

            CardBaseViewModel _cardBaseViewModel = null;
            _cardBaseViewModel = (CardBaseViewModel)card.DataContext;

            if (_cardBaseViewModel.IsFullScreen || !_cardBaseViewModel.IsNeedToExpand)
                return;

            StartPage.Current.HideOrShowAppBar(true);

            scrollViewer = (ScrollViewer)((Grid)card.Content).Children[0];

            card.RenderTransform = new CompositeTransform();

            ((Storyboard)card.Resources["ExpandAnimation"]).Begin();

            _cardBaseViewModel.IsNeedToExpand = false;
            _cardBaseViewModel.IsFullScreen = true;

            _isFullScreen = true;
            fullScreenCard = card;
        }

        private void ExpandButton_Click(object sender, RoutedEventArgs e)
        {
            Expand();
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            if (_isFullScreen)
            {
                scrollViewer.ChangeView(0, 0, (float)1.0);

                fullScreenCard.Margin = new Thickness(19, 29, 19, 15);
                fullScreenCard.ResetRenderTransform();

                StartPage.Current.HideOrShowAppBar(false);

                CardBaseViewModel _cardBaseViewModel = (CardBaseViewModel)((DraggableControl)fullScreenCard).DataContext;

                _cardBaseViewModel.IsNeedToExpand = true;
                _cardBaseViewModel.IsFullScreen = false;

                _isFullScreen = false;

            }
        }
    }
}
