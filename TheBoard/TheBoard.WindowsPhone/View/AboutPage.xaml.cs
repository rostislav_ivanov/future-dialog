﻿using System;
using TheBoard.Localization;
using Windows.UI.Xaml.Controls;

namespace TheBoard.View
{
    public sealed partial class AboutPage
    {
        public AboutPage()
        {
            InitializeComponent();

            TxtBlockAppTitle.Text = LocalizedResources.GetString("app_name").ToUpper();
            TxtBlockAboutHeader.Text = LocalizedResources.GetString("about_header");
            TxtBlockAboutMessage.Text = LocalizedResources.GetString("about_message");

            var aboutLink1 = LocalizedResources.GetString("about_link1");
            BtnAboutLink1.Content = aboutLink1;
            BtnAboutLink1.NavigateUri = new Uri(string.Format("http://{0}", aboutLink1));

            /*var aboutLink2 = LocalizedResources.GetString("about_link2");
            BtnAboutLink2.Content = aboutLink2;
            BtnAboutLink2.NavigateUri = new Uri(string.Format("http://{0}", aboutLink2));*/
        }
    }
}
