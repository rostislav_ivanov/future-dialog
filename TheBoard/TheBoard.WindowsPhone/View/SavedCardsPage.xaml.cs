﻿using System;
using TheBoard.Localization;

namespace TheBoard.View
{
    public sealed partial class SavedCardsPage
    {
        public SavedCardsPage()
        {
            InitializeComponent();

            TxtBlockAppTitle.Text = LocalizedResources.GetString("app_name").ToUpper();
            TxtBlockSavedCards.Text = LocalizedResources.GetString("tallennetut_kortit");
            TxtBlockEmpty.Text = LocalizedResources.GetString("empty_message");
        }
    }
}
