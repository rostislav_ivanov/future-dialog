﻿using MetroLab.Common;
using MyToolkit.Messaging;
using System;
using TheBoard.Controls;
using TheBoard.Localization;
using TheBoard.ViewModel;
using TheBoard.ViewModel.View.Cards;
using Windows.Graphics.Display;
using Windows.Phone.UI.Input;
using Windows.Storage;
using Windows.UI;
using Windows.UI.Popups;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Media;

namespace TheBoard.View
{
    public sealed partial class StartPage
    {
        private bool isNeedToShowCloseButtonToolTip;
        private bool isNeedToShowPromotedToolTip;
        private bool isNeedToShowFirstCardToolTip;
        public static StartPage Current { get; private set; }

        public StartPage()
        {
            InitializeComponent();

            HardwareButtons.BackPressed += HardwareButtons_BackPressed;

            Current = this;

            DisplayInformation.AutoRotationPreferences = DisplayOrientations.Portrait;

            FirstInfoCardHeaderTextBlock.Text = LocalizedResources.GetString("first_info_card_header_");
            FirstInfoCardTextBlock.Text = LocalizedResources.GetString("first_info_card_text_");
            ToolTip1HomeTextBlock.Text = LocalizedResources.GetString("home_");
            ToolTip1FavoritesTextBlock.Text = LocalizedResources.GetString("favorites_");
            ToolTip1SettingsTextBlock.Text = LocalizedResources.GetString("settings_");
            ToolTip1CenterTextBlock.Text = LocalizedResources.GetString("mark_hand_description_");
            ToolTipFirstCardGridTextBlock.Text = LocalizedResources.GetString("mark_thank_you_description_");
            ToolTipFirstCardHeaderGridTextBlock.Text = LocalizedResources.GetString("mark_thank_you_").ToUpper();

            if (ApplicationData.Current.LocalSettings.Values["FirstLaunch"] == null)
            {
                FirstCard.Visibility = Visibility.Visible;
            }
            else
            {
                MainFrame.Visibility = Visibility.Visible;
                TopAppBar.Visibility = Visibility.Visible;

                if ((string)ApplicationData.Current.LocalSettings.Values["FirstLaunch"] == "FirstToolTip")
                {
                    FirstToolTipGrid.Visibility = Visibility.Visible;
                }
            }

            if (ApplicationData.Current.LocalSettings.Values["ToolTipSkipable"] == null ||
                ApplicationData.Current.LocalSettings.Values["ToolTipPromoted"] == null)
            {
                ToolTipCloseButtonTextBlock.Text = LocalizedResources.GetString("mark_skip_description_");
                ToolTipPromotedTextBlock.Text = LocalizedResources.GetString("mark_promo_description_");
                ToolTip2FavoritesTextBlock.Text = LocalizedResources.GetString("favorites_");
                Messenger.Default.Register<ToolTipMessage>(this, HandleStatusMessage);
            }
        }

        private void HandleStatusMessage(ToolTipMessage msg)
        {
            if (msg.ToolTipMessageType_ == ToolTipMessageType.FirstCard && ApplicationData.Current.LocalSettings.Values["ToolTipFirstCard"] == null)
            {
                if (FirstCard.Visibility == Visibility.Collapsed && FirstToolTipGrid.Visibility == Visibility.Collapsed &&
                        ToolTipCloseButtonGrid.Visibility == Visibility.Collapsed && ToolTipPromotedGrid.Visibility == Visibility.Collapsed)
                {
                    ToolTipFirstCardGrid.Visibility = Visibility.Visible;
                }
                else
                    isNeedToShowFirstCardToolTip = true;
            }

            if (msg.ToolTipMessageType_ == ToolTipMessageType.Skippable && ApplicationData.Current.LocalSettings.Values["ToolTipSkipable"] == null)
            {
                if (FirstCard.Visibility == Visibility.Collapsed && FirstToolTipGrid.Visibility == Visibility.Collapsed &&
                        ToolTipPromotedGrid.Visibility == Visibility.Collapsed && ToolTipFirstCardGrid.Visibility == Visibility.Collapsed)
                {
                    ToolTipCloseButtonGrid.Visibility = Visibility.Visible;
                }
                else
                    isNeedToShowCloseButtonToolTip = true;
            }

            if (msg.ToolTipMessageType_ == ToolTipMessageType.Promoted && ApplicationData.Current.LocalSettings.Values["ToolTipPromoted"] == null)
            {
                if (FirstCard.Visibility == Visibility.Collapsed && FirstToolTipGrid.Visibility == Visibility.Collapsed &&
                        ToolTipCloseButtonGrid.Visibility == Visibility.Collapsed && ToolTipFirstCardGrid.Visibility == Visibility.Collapsed)
                {
                    ToolTipPromotedGrid.Visibility = Visibility.Visible;
                }
                else
                    isNeedToShowPromotedToolTip = true; 
            }
        }

        private void HardwareButtons_BackPressed(object sender, BackPressedEventArgs e)
        {
            if (e.Handled)
                return;

            if (MainFrame.Content is MainPage && MainPage.Current._isFullScreen)
            {
                e.Handled = true;

                MainPage.Current.ScrollToCardBegin();

                MainPage.Current.fullScreenCard.Margin = new Thickness(19, 29, 19, 15);
                MainPage.Current.fullScreenCard.ResetRenderTransform();

                StartPage.Current.HideOrShowAppBar(false);

                CardBaseViewModel _cardBaseViewModel = (CardBaseViewModel)((DraggableControl)MainPage.Current.fullScreenCard).DataContext;

                _cardBaseViewModel.IsNeedToExpand = true;
                _cardBaseViewModel.IsFullScreen = false;

                MainPage.Current._isFullScreen = false;
            }
            else 
            {
                var frame = Window.Current.Content as Frame;
                if (frame.Content is FullscreenVideoPage && FullScreenWebView.VideoOnCard != null)
                {
                    e.Handled = true;

                    FullScreenWebView.VideoOnCard.LoadVideoOnCard();
                }
                else
                {
                    Application.Current.Exit();
                }
            }

        }

        private void HomeButton_Click(object sender, RoutedEventArgs e)
        {
            MainFrame.Navigate(typeof(MainPage), true);

            this.FavoriteToggleButton.IsChecked = false;
            this.SettingsToggleButton.IsChecked = false;
            this.FavoriteToggleButton.IsHitTestVisible = true;
            this.SettingsToggleButton.IsHitTestVisible = true;

            this.HomeToggleButton.IsHitTestVisible = false;
        }

        private void FavoriteButton_Click(object sender, RoutedEventArgs e)
        {
            MainFrame.Navigate(typeof(MainPage), false);

            this.HomeToggleButton.IsChecked = false;
            this.SettingsToggleButton.IsChecked = false;
            this.HomeToggleButton.IsHitTestVisible = true;
            this.SettingsToggleButton.IsHitTestVisible = true;

            this.FavoriteToggleButton.IsHitTestVisible = false;
        }

        private async void SettingsButton_Click(object sender, RoutedEventArgs e)
        {
            MainFrame.Navigate(typeof(SettingsPage));

            this.HomeToggleButton.IsChecked = false;
            this.FavoriteToggleButton.IsChecked = false;
            this.FavoriteToggleButton.IsHitTestVisible = true;
            this.HomeToggleButton.IsHitTestVisible = true;

            this.SettingsToggleButton.IsHitTestVisible = false;
        }

        private void FirstCardOKButton_Click(object sender, RoutedEventArgs e)
        {
            FirstCard.Visibility = Visibility.Collapsed;
            MainFrame.Visibility = Visibility.Visible;
            TopAppBar.Visibility = Visibility.Visible;
            ApplicationData.Current.LocalSettings.Values["FirstLaunch"] = "FirstToolTip";
            FirstToolTipGrid.Visibility = Visibility.Visible;
        }

        private void FirstToolTipGrid_Tapped(object sender, Windows.UI.Xaml.Input.TappedRoutedEventArgs e)
        {
            ApplicationData.Current.LocalSettings.Values["FirstLaunch"] = "no";
            FirstToolTipGrid.Visibility = Visibility.Collapsed;

            if (isNeedToShowCloseButtonToolTip)
            {
                ToolTipCloseButtonGrid.Visibility = Visibility.Visible;
            }
        }

        public void HideOrShowAppBar(bool hideOrShow)
        {
            if (hideOrShow)
            {
                TopAppBar.Visibility = Visibility.Collapsed;
                MainFrame.Margin = new Thickness(0,0,0,0);
            }
            else
            {
                TopAppBar.Visibility = Visibility.Visible;
                MainFrame.Margin = new Thickness(0, 0, 0, 55);
            }
        }

        private void ToolTipCloseButtonGrid_Tapped(object sender, Windows.UI.Xaml.Input.TappedRoutedEventArgs e)
        {
            ToolTipCloseButtonGrid.Visibility = Visibility.Collapsed;
            ApplicationData.Current.LocalSettings.Values["ToolTipSkipable"] = "seen";
            isNeedToShowCloseButtonToolTip = false;

            if (ApplicationData.Current.LocalSettings.Values["ToolTipPromoted"] != null && ApplicationData.Current.LocalSettings.Values["ToolTipFirstCard"] != null)
                Messenger.Default.Deregister<ToolTipMessage>(this, HandleStatusMessage);

            if(isNeedToShowFirstCardToolTip)
            {
                ToolTipFirstCardGrid.Visibility = Visibility.Visible;
            }
            else if(isNeedToShowPromotedToolTip)
            {
                ToolTipPromotedGrid.Visibility = Visibility.Visible;
            }
        }

        private void ToolTipPromotedGrid_Tapped(object sender, Windows.UI.Xaml.Input.TappedRoutedEventArgs e)
        {
            ToolTipPromotedGrid.Visibility = Visibility.Collapsed;
            ApplicationData.Current.LocalSettings.Values["ToolTipPromoted"] = "seen";
            isNeedToShowPromotedToolTip = false;

            if (ApplicationData.Current.LocalSettings.Values["ToolTipSkipable"] != null && ApplicationData.Current.LocalSettings.Values["ToolTipFirstCard"] != null)
                Messenger.Default.Deregister<ToolTipMessage>(this, HandleStatusMessage);

            if (isNeedToShowFirstCardToolTip)
            {
                ToolTipFirstCardGrid.Visibility = Visibility.Visible;
            }
            else if (isNeedToShowCloseButtonToolTip)
            {
                ToolTipCloseButtonGrid.Visibility = Visibility.Visible;
            }
        }

        private async void LayoutRoot_Loaded(object sender, RoutedEventArgs e)
        {
            if (MainPage.Current == null)
            {
                MainFrame.Navigate(typeof(MainPage), true);
            }
            //MainPage.Current._isFromVideoFullScreen = false;
        }

        private void ToolTipFirstCardGrid_Tapped(object sender, Windows.UI.Xaml.Input.TappedRoutedEventArgs e)
        {
            ToolTipFirstCardGrid.Visibility = Visibility.Collapsed;
            ApplicationData.Current.LocalSettings.Values["ToolTipFirstCard"] = "seen";
            isNeedToShowFirstCardToolTip = false;

            if (ApplicationData.Current.LocalSettings.Values["ToolTipPromoted"] != null && ApplicationData.Current.LocalSettings.Values["ToolTipSkipable"] != null)
                Messenger.Default.Deregister<ToolTipMessage>(this, HandleStatusMessage);

            if (isNeedToShowPromotedToolTip)
            {
                ToolTipPromotedGrid.Visibility = Visibility.Visible;
            }
            else if (isNeedToShowCloseButtonToolTip)
            {
                ToolTipCloseButtonGrid.Visibility = Visibility.Visible;
            }
        }
    }
}
