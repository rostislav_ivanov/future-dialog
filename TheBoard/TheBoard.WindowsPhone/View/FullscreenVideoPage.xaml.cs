﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using TheBoard.Controls;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.Graphics.Display;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Navigation;

namespace TheBoard.View
{
    public sealed partial class FullscreenVideoPage : Page
    {
        public static FullscreenVideoPage Current { get; private set; }

        public FullscreenVideoPage()
        {
            this.InitializeComponent();
            Current = this;
        }

        FullScreenWebView controlToRetrunBackFullscreenVideo;
             
        protected override void OnNavigatedTo(NavigationEventArgs e)
        {
            if(e.Parameter != null)
            {
                controlToRetrunBackFullscreenVideo = (FullScreenWebView)e.Parameter;
                
                this.LayoutRoot.Children.Add((WebView)(controlToRetrunBackFullscreenVideo.WebViewForFullscreen));
                DisplayInformation.AutoRotationPreferences = DisplayOrientations.Portrait | DisplayOrientations.Landscape;
            }
        }

        protected override void OnNavigatedFrom(NavigationEventArgs e)
        {
            if (this.LayoutRoot.Children.Count > 0)
            {
                DisplayInformation.AutoRotationPreferences = DisplayOrientations.Portrait;

                WebView _fullscreenWebView = (WebView)this.LayoutRoot.Children[0];
                this.LayoutRoot.Children.Remove(_fullscreenWebView);

                controlToRetrunBackFullscreenVideo.LoadVideoFromFullscreen(_fullscreenWebView);
            }
            base.OnNavigatedFrom(e);
        }

        public WebView GetFullScreenVideo()
        {
            DisplayInformation.AutoRotationPreferences = DisplayOrientations.Portrait;

            WebView _fullscreenWebView = (WebView)this.LayoutRoot.Children[0];
            this.LayoutRoot.Children.Remove(_fullscreenWebView);

            var frame = Window.Current.Content as Frame;

            frame.GoBack();

            return _fullscreenWebView;
        }
    }
}
