﻿using MetroLab.Common;
using System;
using TheBoard.Localization;
using TheBoard.ViewModel;
using Windows.Devices.Geolocation;
using Windows.Storage;
using Windows.UI.ViewManagement;

namespace TheBoard.View
{
    public sealed partial class SettingsPage
    {
        public SettingsPage()
        {
            InitializeComponent();
            
            TxtBlockSettings.Text = LocalizedResources.GetString("asetukset");
            TxtBlockTrackingDescription.Text = LocalizedResources.GetString("settings_location_description_");
            TxtBlockNotificationsDescription.Text = LocalizedResources.GetString("settings_notification_description_");
            
            ToggleSwitchTracking.OnContent = ToggleSwitchTracking.OffContent =
                LocalizedResources.GetString("Tracking");
            ToggleSwitchNotifications.OnContent = ToggleSwitchNotifications.OffContent =
                LocalizedResources.GetString("Notifications");

            if (ApplicationData.Current.LocalSettings.Values["notifications"] == null)
            {
                ApplicationData.Current.LocalSettings.Values["notifications"] = true;
                this.ToggleSwitchNotifications.IsOn = true;
            }
            else
            {
                this.ToggleSwitchNotifications.IsOn = (bool)ApplicationData.Current.LocalSettings.Values["notifications"];
            }

            CheckAccess();

        }

        private async void CheckAccess()
        {
            Geolocator locator = new Geolocator();

            try
            {
                await locator.GetGeopositionAsync();

                if (ApplicationData.Current.LocalSettings.Values["location"] == null)
                {
                    ApplicationData.Current.LocalSettings.Values["location"] = true;
                    this.ToggleSwitchTracking.IsOn = true;
                }
                else
                {
                    this.ToggleSwitchTracking.IsOn = (bool)ApplicationData.Current.LocalSettings.Values["location"];
                }

                return;
            }
            catch(Exception e)
            {
                if (ApplicationData.Current.LocalSettings.Values["location"] == null || (bool)ApplicationData.Current.LocalSettings.Values["location"] == true)
                {
                    ApplicationData.Current.LocalSettings.Values["location"] = false;
                }
                this.ToggleSwitchTracking.IsOn = false;
            }
        }

        private void ToggleSwitchNotifications_Toggled(object sender, Windows.UI.Xaml.RoutedEventArgs e)
        {
            SaveSettings();
        }

        private async void ToggleSwitchTracking_Toggled(object sender, Windows.UI.Xaml.RoutedEventArgs e)
        {
            if (ToggleSwitchTracking.IsOn)
            {
                Geolocator locator = new Geolocator();

                try
                {
                    await locator.GetGeopositionAsync();

                    SaveSettings();
                }
                catch (Exception error)
                {
                    ToggleSwitchTracking.IsOn = false;
                    await BaseViewModel.ShowOkCancelDialogAsync(Localization.LocalizedResources.GetString("location_permission_phone_settings_message"), "", 
                                                        o => Windows.System.Launcher.LaunchUriAsync(new Uri("ms-settings-location:")),
                                                        Localization.LocalizedResources.GetString("settings_"),
                                                        Localization.LocalizedResources.GetString("button_no_"));
                }
            }
            else
            {
                SaveSettings();
            }
        }

        private async void SaveSettings()
        {
            var result = await AppServerAgent.Current.UpdateCustomerSettingsAsync(this.ToggleSwitchNotifications.IsOn, this.ToggleSwitchTracking.IsOn);
            if (result.IsSuccess)
            {
                ApplicationData.Current.LocalSettings.Values["notifications"] = this.ToggleSwitchNotifications.IsOn;
                ApplicationData.Current.LocalSettings.Values["location"] = this.ToggleSwitchTracking.IsOn;
            }
        }
    }
}
