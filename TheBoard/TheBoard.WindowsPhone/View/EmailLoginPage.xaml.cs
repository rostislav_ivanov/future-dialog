﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using TheBoard.ViewModel;
using TheBoard.ViewModel.View;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.Phone.UI.Input;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Navigation;

// The Blank Page item template is documented at http://go.microsoft.com/fwlink/?LinkID=390556

namespace TheBoard.View
{
    /// <summary>
    /// An empty page that can be used on its own or navigated to within a Frame.
    /// </summary>
    public sealed partial class EmailLoginPage : Page
    {
        EmailLoginViewModel dataContext = new EmailLoginViewModel();
        public EmailLoginPage()
        {
            this.DataContext = dataContext;
            this.InitializeComponent();
            HardwareButtons.BackPressed += HardwareButtons_BackPressed;
        }

        private void HardwareButtons_BackPressed(object sender, BackPressedEventArgs e)
        {
            if (e.Handled)
                return;

            e.Handled = true;

            var frame = Window.Current.Content as Frame;

            if (frame != null && frame.CanGoBack)
            {
                frame.GoBack();
            }
            else
            {
                Application.Current.Exit();
            }
        }

        private void NameTextBox_KeyUp(object sender, KeyRoutedEventArgs e)
        {
            if (e.Key == Windows.System.VirtualKey.Enter)
            {
                this.EmailTextBox.Focus(FocusState.Keyboard);
            }
        }

        private void EmailTextBox_KeyUp(object sender, KeyRoutedEventArgs e)
        {
            if (e.Key == Windows.System.VirtualKey.Enter)
            {
                Windows.UI.ViewManagement.InputPane.GetForCurrentView().TryHide();

                this.RegisterButton.IsEnabled = true;
                this.RegisterButton.Focus(FocusState.Pointer);
            }
        }

        private void NameTextBox_GotFocus(object sender, RoutedEventArgs e)
        {
            this.LayoutRoot.RowDefinitions[0].Height = new GridLength(40);
        }

        private void EmailTextBox_LostFocus(object sender, RoutedEventArgs e)
        {
            this.LayoutRoot.RowDefinitions[0].Height = new GridLength(200);
        }

        private void LayoutRoot_Loaded(object sender, RoutedEventArgs e)
        {
            this.LayoutRoot.RowDefinitions[0].Height = new GridLength(200);
            RegisterButton.IsEnabled = false;
        }

        private void NameTextBox_TextChanged(object sender, TextChangedEventArgs e)
        {
            if (!String.IsNullOrEmpty(NameTextBox.Text) &&
                    !String.IsNullOrEmpty(EmailTextBox.Text) && EmailTextBox.Text.Contains("@"))
            {
                RegisterButton.IsEnabled = true;
            }
            else
                RegisterButton.IsEnabled = false;
        }
    }
}
