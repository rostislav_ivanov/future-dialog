﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using System.Threading.Tasks;
using TheBoard.ViewModel;
using TheBoard.ViewModel.Facebook;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.Graphics.Display;
using Windows.UI;
using Windows.UI.Popups;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Navigation;

// The Blank Page item template is documented at http://go.microsoft.com/fwlink/?LinkID=390556

namespace TheBoard.View
{
    /// <summary>
    /// An empty page that can be used on its own or navigated to within a Frame.
    /// </summary>
    public sealed partial class LoginPage : IWebAuthenticationContinuable
    {
        public static LoginPage Current { get; private set; }

        public LoginPage()
        {
            var statusBar = Windows.UI.ViewManagement.StatusBar.GetForCurrentView();
            statusBar.BackgroundColor = Colors.Red;
            statusBar.BackgroundOpacity = 0;

            this.InitializeComponent();

            Current = this;

            DisplayInformation.AutoRotationPreferences = DisplayOrientations.Portrait;
        }

        public async void ContinueWebAuthentication(Windows.ApplicationModel.Activation.WebAuthenticationBrokerContinuationEventArgs args)
        {
            //ToDo: doesn't invoke
            CredentialsFacebook credentialsFacebook = await FacebookAuthenticationAgent.Current.Finalize(args);
        }

        public async void ContinueAppAuthentication(string queryString)
        {
            try
            {
                CredentialsFacebook credentialsFacebook = await FacebookAuthenticationAgent.Current.AuthenticationFromAppReceivedAsync(queryString);

                AppServerAgent.Current.SaveAccessTokenForLogin(new KeyValuePair<string, string>("fb_access_token", credentialsFacebook.AccessToken));

                Frame rootFrame = Window.Current.Content as Frame;
                rootFrame.Navigate(typeof(StartPage));
            }
            catch(Exception e)
            {
                var md = new MessageDialog(e.Message, "Error");
                await md.ShowAsync();
            }
       
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            Frame.Navigate(typeof(EmailLoginPage));
        }

        private async void FacebookButtonClick(object sender, RoutedEventArgs e)
        {
            await FacebookAuthenticationAgent.AuthenticateWithApp(); //unfortunately it always return true
        }
    }
}
